<?php

namespace app\source\salary;

use app\models\Employees;
use app\models\Projects;
use app\models\Routes;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class EmployeeSalaryManager
 * @package app\source\salary
 */
class EmployeeSalaryManager extends Component
{
    /** @var Employees */
    private $employee;

    /**
     * @inheritdoc
     */
    public function __construct($employee, array $config = [])
    {
        $this->employee = $employee;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->employee == null){
            throw new InvalidConfigException('$employee must be required');
        }
    }

    /**
     * @param Projects $project
     * @return double
     */
    public function getProjectSalary($project)
    {

    }

    /**
     * @param Projects $project
     * @return double
     */
    public function getProjectPay($project)
    {
        $routes = $this->getRoutes($project->id);
        $factCountAddress = array_sum(ArrayHelper::getColumn($routes, 'fact_count_adress'));
        $completedCountAddress = array_sum(ArrayHelper::getColumn($routes, 'completed_count'));

        $honestyPercent = round($completedCountAddress*100/$factCountAddress, 2);

        return $honestyPercent;
    }

    /**
     * @param Projects $project
     * @return double
     */
    public function getProjectHonestyPercent($project)
    {
        $routes = $this->getRoutes($project->id);
        $factCountAddress = array_sum(ArrayHelper::getColumn($routes, 'fact_count_adress'));
        $completedCountAddress = array_sum(ArrayHelper::getColumn($routes, 'completed_count'));

        $honestyPercent = round($completedCountAddress*100/$factCountAddress, 2);

        return $honestyPercent;
    }

    /**
     * @param Projects $project
     * @return double
     */
    public function getProjectHours($project)
    {

    }

    /**
     * @param Projects $project
     * @return double
     */
    public function getProjectFines($project)
    {

    }

    /**
     * @param int $projectId
     * @return Routes[]
     */
    private function getRoutes($projectId)
    {
        return Routes::find()->where(['project_id' => $projectId, 'employees_id' => $this->employee->id])->all();
    }
}