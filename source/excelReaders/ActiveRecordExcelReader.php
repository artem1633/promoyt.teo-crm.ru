<?php

namespace app\source\excelReaders;

use app\models\AddressList;
use app\models\ProjectAddress;
use app\services\YandexStorage;
use Yii;
use yii\base\Component;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class ActiveRecordExcelReader
 * @package app\source\excelReaders
 */
class ActiveRecordExcelReader extends Component
{

    private static function addToAddressList($data)
    {
        $model = new AddressList();
        $model->attributes =  $data;
        $coords = YandexStorage::getCoordByAddress($data['town'].', '.$data['region'].', ул.'.$data['street'].', '.$data['house'].' '.$data['housing']);
        $model->coord_x = $coords['x'];
        $model->coord_y = $coords['y'];
        return $model->save(false);
    }

    private static function addAddressToProject($address,$projectId,$rowAddress)
    {
        $model = new ProjectAddress();
        $model->project_id = $projectId;
        $model->address_id = $address['id'];
        $model->entrance = $rowAddress['entrance'];
        $model->floor = $rowAddress['floor'];
        $model->apartament = $rowAddress['apartament'];
        $model->porter = $rowAddress['porter'];
//        $coords = YandexStorage::getCoordByAddress($rowAddress['town'].', '.$rowAddress['region'].', ул.'.$rowAddress['street'].', '.$rowAddress['house'].' '.$rowAddress['housing']);
        $model->coord_x = $address['coord_x'];
        $model->coord_y = $address['coord_y'];
        return $model->save(false);
    }

    private static function issetAddressInProject($addressId,$projectId)
    {
        return ProjectAddress::find()->where(['project_id'=>$projectId])->andWhere(['address_id'=>$addressId])->one();
    }

    /**Проверяет существует ли адрес в базе
     * @param $town
     * @param $region
     * @param $street
     * @param $house
     * @param $housing
     * @return bool
     */
    private static function issetAddress($town,$region,$street,$house,$housing)
    {
        $res = AddressList::find();
        if ($town == null) {
            $res->andWhere(['is','town', new \yii\db\Expression('null')]);
        } else {
            $res->andWhere(['town'=>trim($town)]);
        }
        if ($region == null) {
            $res->andWhere(['is','region', new \yii\db\Expression('null')]);
        } else {
            $res->andWhere(['region'=>trim($region)]);
        }
        if ($street == null) {
            $res->andWhere(['is','street', new \yii\db\Expression('null')]);
        } else {
            $res->andWhere(['street'=>trim($street)]);
        }
        if ($house == null) {
            $res->andWhere(['is','house', new \yii\db\Expression('null')]);
        } else {
            $res->andWhere(['house'=>trim($house)]);
        }
        if ($housing == null) {
            $res->andWhere(['is','housing', new \yii\db\Expression('null')]);
        } else {
            $res->andWhere(['housing'=>trim($housing)]);
        }

        $model = $res->one();

//        var_dump([$town,$region,$street,$house,$housing]);
//
//        if($model){
//            var_dump(true);
//        }

        if($model != null){
            if($model->coord_x == null || $model->coord_y == null){
                $coords = YandexStorage::getCoordByAddress($town.', '.$region.', ул.'.$street.', '.$house.' '.$housing);
                $model->coord_x = $coords['x'];
                $model->coord_y = $coords['y'];
                $model->save(false);
            }
        }

        return $res->asArray()->one();
    }

    /**
     * Читает и записыват данные из excel-файла
     * Возвращает кол-во созданых и пропущенных записей
     * @param string $class Класс модели
     * @param string $file Путь до excel-файла
     * @param array $attributesMap Соответствия атрибутов и колонок excel-файла
     * @return array
     */
    public static function importToMainAddress($class, $file )
    {
        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet();
        $skipCount =0;
        $savedCount = 0;
        foreach ($sheet->getRowIterator() as $row){
                $cells = $row->getCellIterator();

                $rowAddress=[];

                foreach ($cells as $cell){
                    if ($cell->getColumn() == "A") {
                        $rowAddress['town'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "B") {
                        $rowAddress['region'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "C") {
                        $rowAddress['street'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "D") {
                        $rowAddress['house'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "E") {
                        $rowAddress['housing'] = $cell->getValue();
                        yii::info($rowAddress['housing'],'test');
                    }
                    if ($cell->getColumn() == "F") {
                        $rowAddress['entrance'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "G") {
                        $rowAddress['floor'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "H") {
                        $rowAddress['apartament'] = $cell->getValue();
                    }
                    if ($cell->getColumn() == "J") {
                        $rowAddress['porter'] = $cell->getValue();
                    }
                }
                if ($rowAddress['town']==null && $rowAddress['street']==null) {
                    break;
                }
                if (self::issetAddress($rowAddress['town'],$rowAddress['region'],$rowAddress['street'],$rowAddress['house'],$rowAddress['housing'])) {
                    $skipCount++;
                } else {

                    if(self::addToAddressList($rowAddress)){
                        $savedCount++;
                    }
                }

        }

        return ['savedCount' => $savedCount, 'skipCount'=>$skipCount];
    }

    public static function importToProjectAddress($file, $projectId )
    {
        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet();
        $skipCount =0;
        $savedCount = 0;
        $savedProjectCount = 0;
        foreach ($sheet->getRowIterator() as $row){
            $cells = $row->getCellIterator();

            $rowAddress=[];

            foreach ($cells as $cell){
                if ($cell->getColumn() == "A") {
                    $rowAddress['town'] = $cell->getValue();
                }
                if ($cell->getColumn() == "B") {
                    $rowAddress['region'] = $cell->getValue();
                }
                if ($cell->getColumn() == "C") {
                    $rowAddress['street'] = $cell->getValue();
                }
                if ($cell->getColumn() == "D") {
                    $rowAddress['house'] = $cell->getValue();
                }
                if ($cell->getColumn() == "E") {
                    $rowAddress['housing'] = $cell->getValue();
                    yii::info($rowAddress['housing'],'test');
                }
                if ($cell->getColumn() == "F") {
                    $rowAddress['entrance'] = $cell->getValue();
                }
                if ($cell->getColumn() == "G") {
                    $rowAddress['floor'] = $cell->getValue();
                }
                if ($cell->getColumn() == "H") {
                    $rowAddress['apartament'] = $cell->getValue();
                }
                if ($cell->getColumn() == "J") {
                    $rowAddress['porter'] = $cell->getValue();
                }
            }
            if ($rowAddress['town']==null && $rowAddress['street']==null) {
                break;
            }
            $address = self::issetAddress($rowAddress['town'],$rowAddress['region'],$rowAddress['street'],$rowAddress['house'],$rowAddress['housing']);

            if ($address) {

                if (self::issetAddressInProject($address['id'],$projectId)) {
                    $skipCount++;
                } else {
                    self::addAddressToProject($address,$projectId,$rowAddress);
                    $savedProjectCount++;
                }

            } else {

                if(self::addToAddressList($rowAddress)){
                    $savedCount++;
                    $address = self::issetAddress($rowAddress['town'],$rowAddress['region'],$rowAddress['street'],$rowAddress['house'],$rowAddress['housing']);
                    self::addAddressToProject($address,$projectId,$rowAddress);
                    $savedProjectCount++;
                }
            }

        }

        return ['savedCount' => $savedCount, 'skipCount'=>$skipCount,'savedProjectCount'=>$savedProjectCount];
    }
}