<?php

use yii\db\Migration;

/**
 * Class m190317_130811_add_columns_to_routes_address_list
 */
class m190317_130811_add_columns_to_routes_address_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes_address_list', 'completed_edition', $this->string()->comment('Подтвержденный тираж'));
        $this->addColumn('routes_address_list', 'comment', $this->text()->comment('Комментарий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes_address_list', 'completed_edition');
        $this->dropColumn('routes_address_list', 'comment');
    }
}
