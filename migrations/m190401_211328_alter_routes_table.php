<?php

use yii\db\Migration;

/**
 * Class m190403_211328_alter_routes_table
 */
class m190401_211328_alter_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes','plane_start_date',$this->date()->comment('Планируемое начало'));
        $this->addColumn('routes','plane_end_date',$this->date()->comment('Планируемое окончание'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190403_211328_alter_routes_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_211328_alter_routes_table cannot be reverted.\n";

        return false;
    }
    */
}
