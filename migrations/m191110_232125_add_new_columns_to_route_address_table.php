<?php

use yii\db\Migration;

/**
 * Handles adding new to table `{{%route_address}}`.
 */
class m191110_232125_add_new_columns_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address', 'fact_n', $this->integer()->comment('Факт Н'));
        $this->addColumn('route_address', 'fact_porch', $this->integer()->comment('Факт Подъезд'));
        $this->addColumn('route_address', 'fact_store', $this->integer()->comment('Факт Этаж'));
        $this->addColumn('route_address', 'fact_flat', $this->integer()->comment('Факт Квартира'));
        $this->addColumn('route_address', 'fact_enter', $this->integer()->comment('Факт Вахта'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'fact_n');
        $this->dropColumn('route_address', 'fact_porch');
        $this->dropColumn('route_address', 'fact_store');
        $this->dropColumn('route_address', 'fact_flat');
        $this->dropColumn('route_address', 'fact_enter');
    }
}
