<?php

use yii\db\Migration;

/**
 * Handles adding payment to table `{{%route_address}}`.
 */
class m191027_193834_add_payment_column_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address', 'payment', $this->float()->after('confirm_work')->comment('Оплата'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'payment');
    }
}
