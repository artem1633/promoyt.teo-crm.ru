<?php

use yii\db\Migration;

/**
 * Handles adding fact_work_data to table `{{%route_address}}`.
 */
class m190527_080957_add_fact_work_date_column_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address', 'fact_work_date',
            $this->timestamp()->defaultValue(null)
                ->comment('Дата и время внесения фактически выполнненого'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'fact_work_date');
    }
}
