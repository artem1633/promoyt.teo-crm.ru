<?php

use yii\db\Migration;

/**
 * Class m190317_160546_add_columns_to_routes_table
 */
class m190317_160546_add_columns_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes', 'completed_count', $this->integer()->comment('Подтвердженное кол-во'));
        $this->addColumn('routes', 'fines_sum', $this->integer()->comment('Сумма штрафов'));
        $this->addColumn('routes', 'comment_leader', $this->text()->comment('Комментарий руководителя'));
        $this->addColumn('routes', 'comment_quality_manager', $this->text()->comment('Комментарий менеджера качества'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes', 'completed_count');
        $this->dropColumn('routes', 'fines_sum');
        $this->dropColumn('routes', 'comment_leader');
        $this->dropColumn('routes', 'comment_quality_manager');
    }
}
