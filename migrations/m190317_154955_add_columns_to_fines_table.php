<?php

use yii\db\Migration;

/**
 * Class m190317_154955_add_columns_to_fines_table
 */
class m190317_154955_add_columns_to_fines_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fines', 'type', $this->integer()->comment('Тип'));
        $this->addColumn('fines', 'interval_before', $this->integer()->comment('Интервал до'));
        $this->addColumn('fines', 'interval_after', $this->integer()->comment('Интервал после'));
        $this->addColumn('fines', 'percent', $this->integer()->comment('Процент штрафа'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fines', 'type');
        $this->dropColumn('fines', 'interval_before');
        $this->dropColumn('fines', 'interval_after');
        $this->dropColumn('fines', 'percent');
    }
}
