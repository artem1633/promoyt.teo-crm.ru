<?php

use yii\db\Migration;

/**
 * Class m191012_122841_alter_price_column_from_prices_table
 */
class m191012_122841_alter_price_column_from_prices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('prices', 'price', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('prices', 'price', $this->integer());
    }
}
