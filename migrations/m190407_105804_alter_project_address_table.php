<?php

use yii\db\Migration;

/**
 * Class m190407_105804_alter_project_address_table
 */
class m190407_105804_alter_project_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('project_address','comment',$this->string()->comment('Комментарий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190407_105804_alter_project_address_table cannot be reverted.\n";

        return false;
    }
    */
}
