<?php

use yii\db\Migration;

/**
 * Class m190330_212131_alter_address_list_table
 */
class m190330_212131_alter_address_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('address_list','porter',$this->integer()->comment('К-во вахт'));
        $this->renameColumn('address_list','korpus','housing');
        $this->dropColumn('address_list','okrug');
        $this->dropColumn('address_list','okrug_region');
        $this->dropColumn('address_list','oblast');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190330_212131_alter_address_list_table cannot be reverted.\n";

        return false;
    }
    */
}
