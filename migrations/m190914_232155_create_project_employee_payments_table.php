<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project_employee_payments}}`.
 */
class m190914_232155_create_project_employee_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project_employee_payments}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'employee_id' => $this->integer()->comment('Промоутер'),
            'amount' => $this->float()->comment('Сумма оплаты'),
            'payed_at' => $this->dateTime()->comment('Оплачено'),
        ]);

        $this->createIndex(
            'idx-project_employee_payments-project_id',
            'project_employee_payments',
            'project_id'
        );

        $this->addForeignKey(
            'fk-project_employee_payments-project_id',
            'project_employee_payments',
            'project_id',
            'projects',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-project_employee_payments-employee_id',
            'project_employee_payments',
            'employee_id'
        );

        $this->addForeignKey(
            'fk-project_employee_payments-employee_id',
            'project_employee_payments',
            'employee_id',
            'employees',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-project_employee_payments-employee_id',
            'project_employee_payments'
        );

        $this->dropIndex(
            'idx-project_employee_payments-employee_id',
            'project_employee_payments'
        );

        $this->dropForeignKey(
            'fk-project_employee_payments-project_id',
            'project_employee_payments'
        );

        $this->dropIndex(
            'idx-project_employee_payments-project_id',
            'project_employee_payments'
        );

        $this->dropTable('{{%project_employee_payments}}');
    }
}
