<?php

use yii\db\Migration;

/**
 * Handles adding time_code to table `{{%employees_move}}`.
 */
class m190807_165232_add_time_code_column_to_employees_move_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees_move', 'timecode', $this->integer()->after('dateandtime')->comment('Тайм код видео (сек.)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employees_move', 'timecode');
    }
}
