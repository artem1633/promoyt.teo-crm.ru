<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%routes_address_list}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%routes}}`
 * - `{{%address_list}}`
 */
class m190317_112542_create_junction_table_for_routes_and_address_list_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%routes_address_list}}', [
            'id' => $this->primaryKey(),
            'routes_id' => $this->integer(),
            'address_list_id' => $this->integer(),
        ]);

        // creates index for column `routes_id`
        $this->createIndex(
            '{{%idx-routes_address_list-routes_id}}',
            '{{%routes_address_list}}',
            'routes_id'
        );

        // add foreign key for table `{{%routes}}`
        $this->addForeignKey(
            '{{%fk-routes_address_list-routes_id}}',
            '{{%routes_address_list}}',
            'routes_id',
            '{{%routes}}',
            'id',
            'CASCADE'
        );

        // creates index for column `address_list_id`
        $this->createIndex(
            '{{%idx-routes_address_list-address_list_id}}',
            '{{%routes_address_list}}',
            'address_list_id'
        );

        // add foreign key for table `{{%address_list}}`
        $this->addForeignKey(
            '{{%fk-routes_address_list-address_list_id}}',
            '{{%routes_address_list}}',
            'address_list_id',
            '{{%address_list}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%routes}}`
        $this->dropForeignKey(
            '{{%fk-routes_address_list-routes_id}}',
            '{{%routes_address_list}}'
        );

        // drops index for column `routes_id`
        $this->dropIndex(
            '{{%idx-routes_address_list-routes_id}}',
            '{{%routes_address_list}}'
        );

        // drops foreign key for table `{{%address_list}}`
        $this->dropForeignKey(
            '{{%fk-routes_address_list-address_list_id}}',
            '{{%routes_address_list}}'
        );

        // drops index for column `address_list_id`
        $this->dropIndex(
            '{{%idx-routes_address_list-address_list_id}}',
            '{{%routes_address_list}}'
        );

        $this->dropTable('{{%routes_address_list}}');
    }
}
