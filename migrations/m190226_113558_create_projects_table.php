<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m190226_113558_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'client_id' => $this->integer()->comment('Заказчик')
        ],$tableOptions);
        $this->createIndex('idx-projects-client_id', '{{%projects}}', 'client_id', false);
        $this->addForeignKey("fk-projects-client_id", "{{%projects}}", "client_id", "clients", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%projects}}');
    }
}
