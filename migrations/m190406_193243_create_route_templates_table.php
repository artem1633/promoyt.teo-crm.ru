<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%route_templates}}`.
 */
class m190406_193243_create_route_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%route_templates}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя шаблона'),
            'template' => $this->text()->comment('Текст шаблона'),
        ]);
        $this->addForeignKey('fk-projects-route_template_id','projects','route_template_id','route_templates','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%route_templates}}');
    }
}
