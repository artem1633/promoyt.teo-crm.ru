<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_status}}`.
 */
class m190218_225049_create_employees_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees_status}}', [
            'id' => $this->primaryKey(),
            'employees_id' => $this->integer()->comment('Сотрудник'),
            'status' => $this->smallInteger()->comment('Статус'),
            'date_status' =>$this->date()->comment('Дата'),
        ],$tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees_status}}');
    }
}
