<?php

use yii\db\Migration;

/**
 * Class m190225_132156_alter_employees_table
 */
class m190225_132156_alter_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees','phone_add',$this->string(15)->comment('Дополнительний телефон'));
        $this->renameColumn('employees','phone2','phone_guarantor');
        $this->addColumn('employees','FIO_guarantor',$this->string()->comment('ФИО поручителя'));
        $this->addColumn('employees','phone_model',$this->string()->comment('Модель смартфона'));
        $this->addColumn('employees','phone_id',$this->string()->comment('ИД смартфона'));
        $this->addColumn('employees','deposit',$this->decimal(8,3)->comment('Депозит'));
        $this->addColumn('employees','specialization',$this->string()->comment('Специализация'));
        $this->addColumn('employees','other_data',$this->string()->comment('Прочие данные'));
        $this->dropColumn('employees','camera');
        $this->dropColumn('employees','internet');
        $this->dropColumn('employees','office');
        $this->dropColumn('employees','departure_area');
        $this->dropColumn('employees','prefers_work');


        $this->dropForeignKey("fk-employees_attach-emloyees_id", "{{%employees_attach}}");
        $this->dropIndex('idx-employees_attach-emloyees_id', '{{%employees_attach}}');
        $this->renameColumn('employees_attach','emloyees_id','employees_id');
        $this->createIndex('idx-employees_attach-emloyees_id', '{{%employees_attach}}', 'employees_id', false);
        $this->addForeignKey("fk-employees_attach-emloyees_id", "{{%employees_attach}}", "employees_id", "employees", "id");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employees','phone_add');
        $this->renameColumn('employees','phone_guarantor','phone2');
        $this->dropColumn('employees','FIO_guarantor');
        $this->dropColumn('employees','phone_model');
        $this->dropColumn('employees','phone_id');
        $this->dropColumn('employees','deposit');
        $this->addColumn('employees','camera',$this->smallInteger(1)->comment('Есть фотоапарат?'));
        $this->addColumn('employees','internet',$this->smallInteger(1)->comment('Есть интернет?'));
        $this->addColumn('employees','office',$this->smallInteger(1)->comment('Сотрудник офиса'));
        $this->addColumn('employees','departure_area',$this->smallInteger(1)->comment('Выезд в область'));
        $this->addColumn('employees','prefers_work',$this->smallInteger(1)->comment('Предпочитает работу'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_132156_alter_employees_table cannot be reverted.\n";

        return false;
    }
    */
}
