<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_children}}`.
 */
class m190225_103548_create_employees_children_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees_children}}', [
            'id' => $this->primaryKey(),
            'employees_id' => $this->integer(),
            'sex' => $this->smallInteger()->comment('Пол'),
            'name' => $this->string()->comment('Имя'),
            'birthday' => $this->date()->comment('Дата рождения'),
        ],$tableOptions);
        $this->createIndex('idx-employees_children-employees_id', '{{%employees_children}}', 'employees_id', false);
        $this->addForeignKey("fk-employees_children-employees_id", "{{%employees_children}}", "employees_id", "employees", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees_children}}');
    }
}
