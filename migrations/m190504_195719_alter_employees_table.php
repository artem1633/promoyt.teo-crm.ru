<?php

use yii\db\Migration;

/**
 * Class m190504_195719_alter_employees_table
 */
class m190504_195719_alter_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees','token',$this->string()->comment('Токен'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190504_195719_alter_employees_table cannot be reverted.\n";

        return false;
    }
    */
}
