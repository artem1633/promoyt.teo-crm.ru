<?php

use yii\db\Migration;

/**
 * Class m190228_180640_alter_routes_table
 */
class m190228_180640_alter_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes','name',$this->string()->comment('Назва маршруту'));
        $this->addColumn('projects','project_cost',$this->decimal(8,3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes','name');
        $this->dropColumn('projects','project_cost');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190228_180640_alter_routes_table cannot be reverted.\n";

        return false;
    }
    */
}
