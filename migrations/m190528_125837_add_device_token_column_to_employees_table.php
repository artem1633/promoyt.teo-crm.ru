<?php

use yii\db\Migration;

/**
 * Handles adding device_token to table `{{%employees}}`.
 */
class m190528_125837_add_device_token_column_to_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees', 'device_token', $this->text()->comment('FCM Токен смартфона'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employees', 'device_token');
    }
}
