<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects_placement}}`.
 */
class m190226_121903_create_projects_placement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects_placement}}', [
            'id' => $this->primaryKey(),
            'placement_id' => $this->integer()->comment('Метод'),
            'price_id' => $this->integer()->comment('Тип цены'),
            'price' => $this->decimal(8,2)->comment('Цена'),
            'circulation' => $this->integer()->comment('Тираж'),
            'project_id' => $this->integer()->comment('Проект')
        ]);
        $this->createIndex('idx-projects_placement-placement_id', '{{%projects_placement}}', 'placement_id', false);
        $this->addForeignKey("fk-projects_placement-placement_id", "{{%projects_placement}}", "placement_id", "placement_method", "id");
        $this->createIndex('idx-projects_placement-project_id', '{{%projects_placement}}', 'project_id', false);
        $this->addForeignKey("fk-projects_placement-project_id", "{{%projects_placement}}", "project_id", "projects", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%projects_placement}}');
    }
}
