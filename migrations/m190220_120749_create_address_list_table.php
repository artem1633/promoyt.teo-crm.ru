<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%address_list}}`.
 */
class m190220_120749_create_address_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%address_list}}', [
            'id' => $this->primaryKey(),
            'country' => $this->string()->comment('Страна'),
            'town' => $this->string()->comment('Город'),
            'village' => $this->string()->comment('Поселок'),
            'street' => $this->string()->comment('Улица'),
            'house' => $this->string()->comment('Дом/Квартира'),
            'coord_x' => $this->decimal(8,8)->comment('Долгота'),
            'coord_y' => $this->decimal(8,8)->comment('Широта'),
            'floor' => $this->integer()->comment('К-во етажей'),
            'apartament' => $this->integer()->comment('К-во квартир'),
            'entrance' => $this->integer()->comment('К-во подьездов'),
            'type' => $this->smallInteger()->comment('Тип здания')
        ],$tableOptions);
        $this->createIndex('idx-address_list-country','{{%address_list}}','country');
        $this->createIndex('idx-address_list-town','{{%address_list}}','town');
        $this->createIndex('idx-address_list-village','{{%address_list}}','village');
        $this->createIndex('idx-address_list-street','{{%address_list}}','street');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-address_list-country','{{%address_list}}');
        $this->dropIndex('idx-address_list-town','{{%address_list}}');
        $this->dropIndex('idx-address_list-village','{{%address_list}}');
        $this->dropIndex('idx-address_list-street','{{%address_list}}');
        $this->dropTable('{{%address_list}}');
    }
}
