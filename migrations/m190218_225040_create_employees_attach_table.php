<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_attach}}`.
 */
class m190218_225040_create_employees_attach_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees_attach}}', [
            'id' => $this->primaryKey(),
            'emloyees_id' => $this->integer(),
            'file' => $this->string()->comment('Файл'),
            'category_file' =>$this->smallInteger()->comment('Категория файла'),
        ],$tableOptions);
        $this->createIndex('idx-employees_attach-emloyees_id', '{{%employees_attach}}', 'emloyees_id', false);
        $this->addForeignKey("fk-employees_attach-emloyees_id", "{{%employees_attach}}", "emloyees_id", "employees", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees_attach}}');
    }
}
