<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sliders}}`.
 */
class m190218_224112_create_sliders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('sliders', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->comment('Заголовок'),
            'text' => $this->binary()->comment('Текст'),
            'fone' => $this->string(255)->comment('Фон'),
            'order' => $this->integer()->comment('Сортировка'),
            'view' => $this->boolean()->comment('Показ'),
            'view_time' => $this->integer()->comment('Время показа в секундах'),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sliders}}');
    }
}
