<?php

use yii\db\Migration;

/**
 * Handles adding new_confirm to table `{{%route_address}}`.
 */
class m191111_124458_add_new_confirm_columns_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address', 'confirm_n', $this->integer()->comment('Подтверждено Н'));
        $this->addColumn('route_address', 'confirm_porch', $this->integer()->comment('Подтверждено Подъезд'));
        $this->addColumn('route_address', 'confirm_store', $this->integer()->comment('Подтверждено Этаж'));
        $this->addColumn('route_address', 'confirm_flat', $this->integer()->comment('Подтверждено Квартира'));
        $this->addColumn('route_address', 'confirm_enter', $this->integer()->comment('Подтверждено Вахта'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'confirm_n');
        $this->dropColumn('route_address', 'confirm_porch');
        $this->dropColumn('route_address', 'confirm_store');
        $this->dropColumn('route_address', 'confirm_flat');
        $this->dropColumn('route_address', 'confirm_enter');
    }
}
