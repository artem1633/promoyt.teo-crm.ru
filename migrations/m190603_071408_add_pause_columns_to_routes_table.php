<?php

use yii\db\Migration;

/**
 * Handles adding pause to table `{{%routes}}`.
 */
class m190603_071408_add_pause_columns_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes', 'start_pause', $this->timestamp()->defaultValue(null)->comment('Дата и время начала паузы'));
        $this->addColumn('routes', 'length_of_pause', $this->integer()->defaultValue(0)->comment('Общее время паузы в минутах'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes', 'start_pause');
        $this->dropColumn('routes', 'length_of_pause');
    }
}
