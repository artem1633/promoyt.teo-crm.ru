<?php

use yii\db\Migration;

/**
 * Handles adding status_api to table `{{%route_address}}`.
 */
class m190526_123311_add_status_api_column_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('address_list', 'status_api');
        $this->addColumn('route_address', 'status_api', $this->tinyInteger(1)->defaultValue(0)->comment('Флаг добавления данных по АПИ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'status_api');
    }
}
