<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%route_address}}`.
 */
class m190226_122416_create_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        };
        $this->createTable('{{%route_address}}', [
            'id' => $this->primaryKey(),
            'address_id' => $this->integer()->comment('адрес'),
            'fine_id' => $this->integer()->comment('Штраф'),
            'route_id' => $this->integer()->comment('Маршрут'),
            'fine_percent' => $this->integer()->comment('Процент штрафа'),
            'fine_comment' => $this->integer()->comment('Комментарий штрафа'),
            'plane_work' => $this->decimal(8,3)->comment('План'),
            'fact_work' => $this->decimal(8,3)->comment('Факт'),
            'confirm_work' => $this->decimal(8,3)->comment('Подтверждено'),
        ],$tableOptions);
        $this->createIndex('idx-route_address-route_id', '{{%route_address}}', 'route_id', false);
        $this->addForeignKey("fk-route_address-route_id", "{{%route_address}}", "route_id", "routes", "id");
        $this->createIndex('idx-route_address-address_id', '{{%route_address}}', 'address_id', false);
        $this->addForeignKey("fk-route_address-address_id", "{{%route_address}}", "address_id", "address_list", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%route_address}}');
    }
}
