<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prices_projects}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%prices}}`
 * - `{{%projects}}`
 */
class m191111_133447_create_junction_table_for_prices_and_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prices_projects}}', [
            'id' => $this->primaryKey(),
            'prices_id' => $this->integer(),
            'projects_id' => $this->integer(),
        ]);

        // creates index for column `prices_id`
        $this->createIndex(
            '{{%idx-prices_projects-prices_id}}',
            '{{%prices_projects}}',
            'prices_id'
        );

        // add foreign key for table `{{%prices}}`
        $this->addForeignKey(
            '{{%fk-prices_projects-prices_id}}',
            '{{%prices_projects}}',
            'prices_id',
            '{{%prices}}',
            'id',
            'CASCADE'
        );

        // creates index for column `projects_id`
        $this->createIndex(
            '{{%idx-prices_projects-projects_id}}',
            '{{%prices_projects}}',
            'projects_id'
        );

        // add foreign key for table `{{%projects}}`
        $this->addForeignKey(
            '{{%fk-prices_projects-projects_id}}',
            '{{%prices_projects}}',
            'projects_id',
            '{{%projects}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%prices}}`
        $this->dropForeignKey(
            '{{%fk-prices_projects-prices_id}}',
            '{{%prices_projects}}'
        );

        // drops index for column `prices_id`
        $this->dropIndex(
            '{{%idx-prices_projects-prices_id}}',
            '{{%prices_projects}}'
        );

        // drops foreign key for table `{{%projects}}`
        $this->dropForeignKey(
            '{{%fk-prices_projects-projects_id}}',
            '{{%prices_projects}}'
        );

        // drops index for column `projects_id`
        $this->dropIndex(
            '{{%idx-prices_projects-projects_id}}',
            '{{%prices_projects}}'
        );

        $this->dropTable('{{%prices_projects}}');
    }
}
