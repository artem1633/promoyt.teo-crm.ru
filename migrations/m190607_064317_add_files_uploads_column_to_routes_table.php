<?php

use yii\db\Migration;

/**
 * Handles adding file_uploads to table `{{%routes}}`.
 */
class m190607_064317_add_files_uploads_column_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes', 'files_uploads', $this->smallInteger()
            ->defaultValue(0)
            ->comment('Флаг, показывающий загружены или нет файлы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes', 'files_uploads');
    }
}
