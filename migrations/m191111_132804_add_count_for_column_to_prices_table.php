<?php

use yii\db\Migration;

/**
 * Handles adding count_for to table `{{%prices}}`.
 */
class m191111_132804_add_count_for_column_to_prices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('prices', 'count_for', $this->integer()->comment('По чему считаем'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('prices', 'count_for');
    }
}
