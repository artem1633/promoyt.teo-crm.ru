<?php

use yii\db\Migration;

/**
 * Handles adding normal to table `{{%routes}}`.
 */
class m190317_090114_add_normal_column_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes', 'normal', $this->integer()->comment('Норма'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes', 'normal');
    }
}
