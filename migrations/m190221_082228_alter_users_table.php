<?php

use yii\db\Migration;

/**
 * Class m190221_082228_alter_users_table
 */
class m190221_082228_alter_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('users','password_hash','password');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('users','password','password_hash');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190221_082228_alter_users_table cannot be reverted.\n";

        return false;
    }
    */
}
