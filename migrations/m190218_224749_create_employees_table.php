<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees}}`.
 */
class m190218_224749_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'status' => $this->integer()->comment('Статус'),
            'category_id' => $this->integer()->comment('Категория'),
            'firstname' => $this->string('60')->comment('Фамилия'),
            'lastname' => $this->string('60')->comment('Имя'),
            'parentname' => $this->string('60')->comment('Отчество'),
            'birthday' => $this->date()->comment('Дата рождения'),
            'address' => $this->string('120')->comment('Адрес проживания'),
            'family_status' => $this->smallInteger(1)->comment('Семейное положение'),
            'phone' => $this->string('15')->comment('Телефон'),
            'phone2' => $this->string('15')->comment('Телефон поручителя'),
            'position_id' => $this->integer()->comment('Должность'),
            'email' => $this->string()->comment('E-mail'),
            'camera' => $this->smallInteger(1)->comment('Есть фотоапарат?'),
            'internet' => $this->smallInteger(1)->comment('Есть интернет?'),
            'office' => $this->smallInteger(1)->comment('Сотрудник офиса'),
            'departure_area' => $this->smallInteger(1)->comment('Выезд в область'),
            'prefers_work' => $this->smallInteger(1)->comment('Предпочитает работу'),
            'start_work' => $this->date()->comment('Дата приема на работу'),
            'pasport_number' => $this->string('20')->comment('Серия и номер паспорта'),
            'pasport_publish' => $this->string('20')->comment('Кем выдан'),
            'pasport_date' => $this->date()->comment('Дата выдачи'),
            'code_structure' => $this->string('20')->comment('Код подразделения'),
            'foto' => $this->string()->comment('Фотография'),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees}}');
    }
}
