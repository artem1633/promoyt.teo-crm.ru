<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%placement_method_routes_address_list}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%placement_method}}`
 * - `{{%routes_address_list}}`
 */
class m190317_145934_create_junction_table_for_placement_method_and_routes_address_list_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%placement_method_routes_address_list}}', [
            'placement_method_id' => $this->integer(),
            'routes_address_list_id' => $this->integer(),
        ]);

        // creates index for column `placement_method_id`
        $this->createIndex(
            '{{%idx-placement_method_routes_address_list-placement_method_id}}',
            '{{%placement_method_routes_address_list}}',
            'placement_method_id'
        );

        // add foreign key for table `{{%placement_method}}`
        $this->addForeignKey(
            '{{%fk-placement_method_routes_address_list-placement_method_id}}',
            '{{%placement_method_routes_address_list}}',
            'placement_method_id',
            '{{%placement_method}}',
            'id',
            'CASCADE'
        );

        // creates index for column `routes_address_list_id`
        $this->createIndex(
            '{{%idx-placement_method_routes_address_list-routes_address_list_id}}',
            '{{%placement_method_routes_address_list}}',
            'routes_address_list_id'
        );

        // add foreign key for table `{{%routes_address_list}}`
        $this->addForeignKey(
            '{{%fk-placement_method_routes_address_list-routes_address_list_id}}',
            '{{%placement_method_routes_address_list}}',
            'routes_address_list_id',
            '{{%routes_address_list}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%placement_method}}`
        $this->dropForeignKey(
            '{{%fk-placement_method_routes_address_list-placement_method_id}}',
            '{{%placement_method_routes_address_list}}'
        );

        // drops index for column `placement_method_id`
        $this->dropIndex(
            '{{%idx-placement_method_routes_address_list-placement_method_id}}',
            '{{%placement_method_routes_address_list}}'
        );

        // drops foreign key for table `{{%routes_address_list}}`
        $this->dropForeignKey(
            '{{%fk-placement_method_routes_address_list-routes_address_list_id}}',
            '{{%placement_method_routes_address_list}}'
        );

        // drops index for column `routes_address_list_id`
        $this->dropIndex(
            '{{%idx-placement_method_routes_address_list-routes_address_list_id}}',
            '{{%placement_method_routes_address_list}}'
        );

        $this->dropTable('{{%placement_method_routes_address_list}}');
    }
}
