<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_position}}`.
 */
class m190218_225135_create_employees_position_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees_position}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string('60')->comment('Должность'),
        ],$tableOptions);
        $this->createIndex('idx-employees-position_id', '{{%employees}}', 'position_id', false);
        $this->addForeignKey("fk-employees-position_id", "{{%employees}}", "position_id", "employees_position", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees_position}}');
    }
}
