<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%placement_method}}`.
 */
class m190219_131548_create_placement_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%placement_method}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название')
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%placement_method}}');
    }
}
