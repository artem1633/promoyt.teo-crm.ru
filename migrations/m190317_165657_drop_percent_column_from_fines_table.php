<?php

use yii\db\Migration;

/**
 * Handles dropping percent from table `{{%fines}}`.
 */
class m190317_165657_drop_percent_column_from_fines_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('fines', 'percent');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('fines', 'percent', $this->integer()->comment('Процент штрафа'));
    }
}
