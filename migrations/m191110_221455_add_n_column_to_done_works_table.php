<?php

use yii\db\Migration;

/**
 * Handles adding n to table `{{%done_works}}`.
 */
class m191110_221455_add_n_column_to_done_works_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('done_works', 'n', $this->string()->comment('n'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('done_works', 'n');
    }
}
