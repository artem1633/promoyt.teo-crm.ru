<?php

use yii\db\Migration;

/**
 * Handles adding status_api to table `{{%address_list}}`.
 */
class m190526_091654_add_status_api_column_to_address_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('address_list', 'status_api', $this->tinyInteger(1)->defaultValue(0)->comment('Флаг добавления данных по АПИ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('address_list', 'status_api');
    }
}
