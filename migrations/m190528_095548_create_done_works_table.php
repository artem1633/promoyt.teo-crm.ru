<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%done_works}}`.
 */
class m190528_095548_create_done_works_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%done_works}}', [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer()->comment('ID маршрута'),
            'address_id' => $this->integer()->comment('ID адреса'),
            'entrance_num' => $this->integer()->comment('Номер подъезда'),
            'floor_count' => $this->integer()->comment('Количество этажей'),
            'apartment_count' => $this->integer()->comment('Количество квартир'),
            'porter' => $this->tinyInteger(1)->comment('Наличие вахты')->defaultValue(0),
            'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()'))->comment('Дата и время добавления данных'),
        ]);

        $this->addForeignKey(
            'fk-done_work-route_id',
            'done_works',
            'route_id',
            'routes',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-done_work-address_id',
            'done_works',
            'address_id',
            'address_list',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%done_works}}');
    }
}
