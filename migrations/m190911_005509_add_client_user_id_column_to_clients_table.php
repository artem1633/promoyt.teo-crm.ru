<?php

use yii\db\Migration;

/**
 * Handles adding client_user_id to table `{{%clients}}`.
 */
class m190911_005509_add_client_user_id_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'client_user_id', $this->integer()->after('id')->comment('Клиент (Пользователь)'));

        $this->createIndex(
            'idx-clients-client_user_id',
            'clients',
            'client_user_id'
        );

        $this->addForeignKey(
            'fk-clients-client_user_id',
            'clients',
            'client_user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-clients-client_user_id',
            'clients'
        );

        $this->dropIndex(
            'idx-clients-client_user_id',
            'clients'
        );

        $this->dropColumn('clients', 'client_user_id');
    }
}
