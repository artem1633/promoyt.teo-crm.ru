<?php

use yii\db\Migration;

/**
 * Class m190317_162504_add_columns_to_projects_table
 */
class m190317_162504_add_columns_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'date_start', $this->date()->comment('Дата начала'));
        $this->addColumn('projects', 'date_end', $this->date()->comment('Дата окончания'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'date_start');
        $this->dropColumn('projects', 'date_end');
    }
}
