<?php

use yii\db\Migration;

/**
 * Handles adding employee_comment to table `{{%route_address}}`.
 */
class m191111_144313_add_employee_comment_column_to_route_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address', 'employee_comment', $this->text()->comment('Комментарий сотрудника'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address', 'employee_comment');
    }
}
