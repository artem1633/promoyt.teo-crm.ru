<?php

use yii\db\Migration;

/**
 * Class m190221_103706_alter_address_list_table
 */
class m190221_103706_alter_address_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('address_list', 'coord_x' , $this->decimal(16,8));
        $this->alterColumn('address_list', 'coord_y' , $this->decimal(16,8));
        $this->dropColumn('address_list','village');
        $this->addColumn('address_list','oblast',$this->string()->comment('Республика, край, область'));
        $this->addColumn('address_list','region',$this->string()->comment('Район'));
        $this->addColumn('address_list','okrug',$this->string()->comment('Округ (в Москве)'));
        $this->addColumn('address_list','okrug_region',$this->string()->comment('Район (В москве)'));
        $this->addColumn('address_list','korpus',$this->string()->comment('Корпус'));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('address_list', 'coord_x' , $this->decimal(16,8));
        $this->alterColumn('address_list', 'coord_y' , $this->decimal(16,8));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190221_103706_alter_address_list_table cannot be reverted.\n";

        return false;
    }
    */
}
