<?php

use yii\db\Migration;

/**
 * Class m190409_065201_alter_routes_table
 */
class m190409_065201_alter_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes','fact',$this->decimal(8,3)->comment('К-во по факту'));
        $this->addColumn('routes','fact_count_address',$this->integer()->comment('Факт. адресов'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190409_065201_alter_routes_table cannot be reverted.\n";

        return false;
    }
    */
}
