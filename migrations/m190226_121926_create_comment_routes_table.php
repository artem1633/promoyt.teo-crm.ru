<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment_routes}}`.
 */
class m190226_121926_create_comment_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%comment_routes}}', [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer()->comment('Маршрут'),
            'comment_type' => $this->integer()->comment('Тип коментария'),
            'comment_date' => $this->dateTime()->comment('Дата коментария'),
            'comment_read' => $this->integer()->comment('Прочитан'),
            'comment_confirm' => $this->integer()->comment('Подтвержден'),
            'comment_text' => $this->string()->comment('Текст коментария'),
        ],$tableOptions);
        $this->createIndex('idx-comment_routes-route_id', '{{%comment_routes}}', 'route_id', false);
        $this->addForeignKey("fk-comment_routes-route_id", "{{%comment_routes}}", "route_id", "routes", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comment_routes}}');
    }
}
