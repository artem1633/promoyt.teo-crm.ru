<?php

use yii\db\Migration;

/**
 * Handles adding price_id to table `{{%projects}}`.
 */
class m191031_191002_add_price_id_column_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'price_id', $this->integer()->comment('Цена'));

        $this->createIndex(
            'idx-projects-price_id',
            'projects',
            'price_id'
        );

        $this->addForeignKey(
            'fk-projects-price_id',
            'projects',
            'price_id',
            'prices',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-projects-price_id',
            'projects'
        );

        $this->dropIndex(
            'idx-projects-price_id',
            'projects'
        );

        $this->dropColumn('projects', 'price_id');
    }
}
