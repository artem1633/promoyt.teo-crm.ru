<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fines_routes_address_list}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%fines}}`
 * - `{{%routes_address_list}}`
 */
class m190317_144400_create_junction_table_for_fines_and_routes_address_list_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fines_routes_address_list}}', [
            'fines_id' => $this->integer(),
            'routes_address_list_id' => $this->integer(),
        ]);

        // creates index for column `fines_id`
        $this->createIndex(
            '{{%idx-fines_routes_address_list-fines_id}}',
            '{{%fines_routes_address_list}}',
            'fines_id'
        );

        // add foreign key for table `{{%fines}}`
        $this->addForeignKey(
            '{{%fk-fines_routes_address_list-fines_id}}',
            '{{%fines_routes_address_list}}',
            'fines_id',
            '{{%fines}}',
            'id',
            'CASCADE'
        );

        // creates index for column `routes_address_list_id`
        $this->createIndex(
            '{{%idx-fines_routes_address_list-routes_address_list_id}}',
            '{{%fines_routes_address_list}}',
            'routes_address_list_id'
        );

        // add foreign key for table `{{%routes_address_list}}`
        $this->addForeignKey(
            '{{%fk-fines_routes_address_list-routes_address_list_id}}',
            '{{%fines_routes_address_list}}',
            'routes_address_list_id',
            '{{%routes_address_list}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%fines}}`
        $this->dropForeignKey(
            '{{%fk-fines_routes_address_list-fines_id}}',
            '{{%fines_routes_address_list}}'
        );

        // drops index for column `fines_id`
        $this->dropIndex(
            '{{%idx-fines_routes_address_list-fines_id}}',
            '{{%fines_routes_address_list}}'
        );

        // drops foreign key for table `{{%routes_address_list}}`
        $this->dropForeignKey(
            '{{%fk-fines_routes_address_list-routes_address_list_id}}',
            '{{%fines_routes_address_list}}'
        );

        // drops index for column `routes_address_list_id`
        $this->dropIndex(
            '{{%idx-fines_routes_address_list-routes_address_list_id}}',
            '{{%fines_routes_address_list}}'
        );

        $this->dropTable('{{%fines_routes_address_list}}');
    }
}
