<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category_calculation}}`.
 */
class m190219_131711_create_category_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%category_calculation}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'indicator' => $this->integer()->comment('Показатель'),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_calculation}}');
    }
}
