<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%routes}}`.
 */
class m190226_120314_create_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%routes}}', [
            'id' => $this->primaryKey(),
            'status_route' => $this->integer()->comment('Статус маршрута'),
            'start_date' => $this->dateTime()->comment('Дата начала'),
            'fact_date' => $this->dateTime()->comment('Дата завершения'),
            'video' => $this->string()->comment('Видео файл'),
            'duplicated' => $this->smallInteger()->comment('Дубликат'),
            'category_id' => $this->integer()->comment('Категория'),
            'employees_id' => $this->integer()->comment('Сотрудник'),
            'project_id' => $this->integer()->comment('Проект'),
            'comment_employees' => $this->string()->comment('Комментарий промоутера'),
            'comment_manager' => $this->string()->comment('Комментарий менеджера'),
            'count_adress' =>$this->integer()->comment('Количество адресов'),
            'fact_count_adress' =>$this->integer()->comment('Фактически выполнено'),

        ],$tableOptions);
        $this->createIndex('idx-routes-category_id', '{{%routes}}', 'category_id', false);
        $this->addForeignKey("fk-routes-category_id", "{{%routes}}", "category_id", "category_calculation", "id");
        $this->createIndex('idx-routes-employees_id', '{{%routes}}', 'employees_id', false);
        $this->addForeignKey("fk-routes-employees_id", "{{%routes}}", "employees_id", "employees", "id");
        $this->createIndex('idx-routes-project_id', '{{%routes}}', 'project_id', false);
        $this->addForeignKey("fk-routes-project_id", "{{%routes}}", "project_id", "projects", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%routes}}');
    }
}
