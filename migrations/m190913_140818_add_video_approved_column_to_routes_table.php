<?php

use yii\db\Migration;

/**
 * Handles adding video_approved to table `{{%routes}}`.
 */
class m190913_140818_add_video_approved_column_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes', 'video_approved', $this->boolean()->defaultValue(0)->comment('Видео одобрено'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('routes', 'video_approved');
    }
}
