<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_category}}`.
 */
class m190218_225201_create_employees_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%employees_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string('60')->comment('Категория'),
        ],$tableOptions);
        $this->createIndex('idx-employees-category_id', '{{%employees}}', 'category_id', false);
        $this->addForeignKey("fk-employees-category_id", "{{%employees}}", "category_id", "employees_category", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees_category}}');
    }
}
