<?php

use yii\db\Migration;

/**
 * Handles adding path_video to table `{{%routes}}`.
 */
class m190808_065002_add_path_video_column_to_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%routes}}', 'path_video', $this->text()->comment('Путь к папке с видеофайлами маршрута'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%routes}}', 'path_video');
    }
}
