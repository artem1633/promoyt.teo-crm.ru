<?php

use yii\db\Migration;

/**
 * Class m191012_122430_alter_indicator_column_from_category_calculation_table
 */
class m191012_122430_alter_indicator_column_from_category_calculation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('category_calculation', 'indicator', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('category_calculation', 'indicator', $this->integer());
    }
}
