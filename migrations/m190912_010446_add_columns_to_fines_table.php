<?php

use yii\db\Migration;

/**
 * Class m190912_010446_add_columns_to_fines_table
 */
class m190912_010446_add_columns_to_fines_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fines', 'is_enabled', $this->boolean()->defaultValue(1)->comment('Включен'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fines', 'is_enabled');
    }
}
