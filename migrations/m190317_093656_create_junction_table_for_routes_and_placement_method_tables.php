<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%routes_placement_method}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%routes}}`
 * - `{{%placement_method}}`
 */
class m190317_093656_create_junction_table_for_routes_and_placement_method_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%routes_placement_method}}', [
            'routes_id' => $this->integer(),
            'placement_method_id' => $this->integer(),
        ]);

        // creates index for column `routes_id`
        $this->createIndex(
            '{{%idx-routes_placement_method-routes_id}}',
            '{{%routes_placement_method}}',
            'routes_id'
        );

        // add foreign key for table `{{%routes}}`
        $this->addForeignKey(
            '{{%fk-routes_placement_method-routes_id}}',
            '{{%routes_placement_method}}',
            'routes_id',
            '{{%routes}}',
            'id',
            'CASCADE'
        );

        // creates index for column `placement_method_id`
        $this->createIndex(
            '{{%idx-routes_placement_method-placement_method_id}}',
            '{{%routes_placement_method}}',
            'placement_method_id'
        );

        // add foreign key for table `{{%placement_method}}`
        $this->addForeignKey(
            '{{%fk-routes_placement_method-placement_method_id}}',
            '{{%routes_placement_method}}',
            'placement_method_id',
            '{{%placement_method}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%routes}}`
        $this->dropForeignKey(
            '{{%fk-routes_placement_method-routes_id}}',
            '{{%routes_placement_method}}'
        );

        // drops index for column `routes_id`
        $this->dropIndex(
            '{{%idx-routes_placement_method-routes_id}}',
            '{{%routes_placement_method}}'
        );

        // drops foreign key for table `{{%placement_method}}`
        $this->dropForeignKey(
            '{{%fk-routes_placement_method-placement_method_id}}',
            '{{%routes_placement_method}}'
        );

        // drops index for column `placement_method_id`
        $this->dropIndex(
            '{{%idx-routes_placement_method-placement_method_id}}',
            '{{%routes_placement_method}}'
        );

        $this->dropTable('{{%routes_placement_method}}');
    }
}
