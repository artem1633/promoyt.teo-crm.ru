<?php

namespace app\controllers;

use app\models\AddressList;
use app\models\ProjectAddress;
use app\services\Polygon;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionTest()
    {
//        $params = json_decode("{\"coords\":[[[55.801178717240994,37.6255923042297],[55.79583619822972,37.63318832015989],[55.804997804485055,37.63945396041866],[55.801178717240994,37.6255923042297]]]}", true);
////        $params = json_decode("{\"coords\":[[[55.801178717240994,37.6255923042297],[55.79583619822972,37.63318832015989],[55.804997804485055,37.63945396041866],[55.801178717240994,37.6255923042297]]]}", true);
//
//
//        $polygon = Polygon::factory($params['coords'][0]);
//        $arobj["coord"]= $polygon->getObjects(44);
//
//
//        VarDumper::dump($arobj, 10 , true);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = "main-login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Скачивание файлов логов в зависимости от типа лога
     * @param string $name Тип логов
     * Типы логов:
     * test, api, app
     * @return \yii\console\Response|NotFoundHttpException|Response
     * @throws NotFoundHttpException
     */
    public function actionGetLog($name)
    {
        $file_path = '';

        switch ($name) {
            case 'test':
                $file_path = Url::to('@app/runtime/logs/test.log');
                break;
            case 'api':
                $file_path = Url::to('@app/runtime/logs/api_error.log');
                break;
            case 'app':
                $file_path = Url::to('@app/runtime/logs/app.log');
                break;

        }

        if (is_file($file_path)){
            return Yii::$app->response->sendFile($file_path);
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    public function actionPhpInfo()
    {
        return phpinfo();
    }


}
