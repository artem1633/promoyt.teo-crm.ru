<?php

namespace app\controllers;

use app\models\AddressList;
use app\models\EmployeesMove;
use app\models\forms\ProgectCopy;
use app\models\forms\RouteAddressesForm;
use app\models\forms\UploadForm;
use app\models\ProjectAddress;
use app\models\ProjectsPlacement;
use app\models\RouteAddress;
use app\models\Routes;
use app\models\RoutesAddressList;
use app\models\search\ProjectAddressSearch;
use app\models\search\ProjectsPlacementSearch;
use app\models\search\RouteAddressSearch;
use app\models\search\RoutesSearch;
use app\models\search\ZonesSearch;
use app\models\Zones;
use app\services\Polygon;
use app\services\YandexStorage;
use app\source\excelReaders\ActiveRecordExcelReader;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use app\models\Projects;
use app\models\search\ProjectsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->placements = ProjectsPlacement::find()->where(['project_id'=>$id])->asArray()->all();
        // Основная информация
        $searchModelProjectsPlacement  = new ProjectsPlacementSearch();
        $dataProviderProjectsPlacement = $searchModelProjectsPlacement->search(Yii::$app->request->queryParams);
        $dataProviderProjectsPlacement->query->andFilterWhere(['project_id'=>$id]);
        // Карта Зоны
        $searchModelZone = new ZonesSearch();
        $dataProviderZone = $searchModelZone->search(Yii::$app->request->queryParams);
        $dataProviderZone->query->andWhere(['project_id'=>$id]);

        $searchModelRoute = new RoutesSearch();
        $dataProviderRoute = $searchModelRoute->search(Yii::$app->request->queryParams);
        $dataProviderRoute->query->andWhere(['routes.project_id'=>$id]);
        $searchModelAddress = new ProjectAddressSearch();
        $dataProviderAddress = $searchModelAddress->search(Yii::$app->request->queryParams);
        $dataProviderAddress->query->andWhere(['project_address.project_id'=>$id]);
        return $this->render('view', [
            'model' => $model,
            'searchModelProjectsPlacement' => $searchModelProjectsPlacement,
            'dataProviderProjectsPlacement' => $dataProviderProjectsPlacement,
            'searchModelZone' => $searchModelZone,
            'dataProviderZone' => $dataProviderZone,
            'searchModelRoute' => $searchModelRoute,
            'dataProviderRoute' => $dataProviderRoute,
            'searchModelAddress' => $searchModelAddress,
            'dataProviderAddress' => $dataProviderAddress,

        ]);
    }

    /**
     * Creates a new Projects model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Projects();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый Проект",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                $model->maketFile = UploadedFile::getInstance($model, 'maketFile');
                $main_path = "images/maket/";
                if (!empty($model->maketFile)) {
                    if (!file_exists(($main_path))) {
                        mkdir($main_path, 0777, true);
                    }
                    $maketFile = $main_path . time() . Yii::$app->security->generateRandomString(5) . '.' . $model->maketFile->extension;
                    $model->maketFile->saveAs($maketFile);
                    $model->maket_file = '/'.$maketFile;
                    $model->save();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый Проект",
                    'content'=>'<span class="text-success">Создание прошло успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый Проект",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionDuplicateRoute($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findRoute($id);
        $new = new Routes();
        $new->attributes = $model->attributes;
        $new->save(false);

        foreach($model->routeAddresses as $address){
            $newAddress = new RouteAddress();
            $newAddress->attributes = $address->attributes;
            $newAddress->route_id = $new->id;
            $newAddress->save(false);
        }

        return ['forceReload' => '#crud-routes-pjax', 'forceClose' => true];
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить проект",
                    'size'=>'modal-lg',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                $model->maketFile = UploadedFile::getInstance($model, 'maketFile');
                $main_path = "images/maket/";
                if (!empty($model->maketFile)) {
                    if (!file_exists(($main_path))) {
                        mkdir($main_path, 0777, true);
                    }
                    $maketFile = $main_path . time() . Yii::$app->security->generateRandomString(5) . '.' . $model->maketFile->extension;
                    $model->maketFile->saveAs($maketFile);
                    $model->maket_file = '/'.$maketFile;
                    $model->save();
                }
                return [
                    'forceReload'=>'#progectMainModal',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить проект",
                    'size'=>'modal-lg',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionCopy($project_id)
    {
        $request = Yii::$app->request;
        $model = new Projects();
        $form = new ProgectCopy();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый проект",
                    'content'=>$this->renderAjax('create-copy', [
                        'model' => $model,
                        'frm' => $form,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                $form->load($request->post());
                $model->createCopy($form,$project_id);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый проект",
                    'content'=>'<span class="text-success">Создание прошло успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый проект",
                    'content'=>$this->renderAjax('create-copy', [
                        'model' => $model,
                        'frm' => $form,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'frm' => $form,
                ]);
            }
        }

    }

    /**
     * Delete an existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    //Основная информация

    /**
     * Updates an existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdateMain($id)
//    {
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
//        if($model->load($request->post()) && $model->save()) {
//            $dataAttributes = array_map(function($value, $key) {
//                return $key.'="'.$value.'"';
//            }, array_values($_POST['Projects']), array_keys($_POST['Projects']));
//            $a = implode(' ', $dataAttributes);
//            $model->maketFile = UploadedFile::getInstance($model, 'maketFile');
//            $main_path = "images/maket/";
//            if (!empty($model->maketFile)) {
//                if (!file_exists(($main_path))) {
//                    mkdir($main_path, 0777, true);
//                }
//                $maketFile = $main_path . time() . Yii::$app->security->generateRandomString(5) . '.' . $model->maketFile->extension;
//                $model->maketFile->saveAs($maketFile);
//                $model->maket_file = '/';//.$a;
//                $model->save();
//            }
//            return $a."xdzvd";
//            //$model->maket_file = '/'.$a;
//            //$model->save();
//        }else{
//            if($request->isAjax){
//                return $this->renderAjax('_form', [
//                    'model' => $model,
//                ]);
//            }
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Creates a new Projects model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPlacementCreate()
    {
        $request = Yii::$app->request;
        $model= new ProjectsPlacement();
        $project_id = $request->get('project_id');
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить новый ",
                    'content'=>$this->renderAjax('_form-placement', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Добавить новый ",
                    'content'=>$this->renderAjax('_form-placement', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-placement', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPlacementUpdate()
    {

        $request = Yii::$app->request;
        $project_id =$request->get('project_id');
        $id = $request->get('id');
        $model = ProjectsPlacement::findOne($id);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'content'=>$this->renderAjax('_form-placement', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }else if($model->load($request->post())){
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements($id);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить ".$model->name,
                    'content'=>$this->renderAjax('_form-placement', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements($id);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-placement', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPlacementDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        ProjectsPlacement::findOne($id)->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Projects model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPlacementBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            ProjectsPlacement::findOne($pk)->delete();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    //Карта
    /**
     * Creates a new Zone model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionLoadZone($project_id)
    {
        $zones = Zones::find()->where(['project_id'=>$project_id])->all();
        $arobj=array();
        foreach ($zones as $zone) {
            $id=$zone->id;
            $arobj[$id]["name_zone"]= $zone->name;
            $arobj[$id]["num_zone"]= $zone->num_zone;
            $arobj[$id]["count_objects"]= $zone->objects_count;
            $arobj[$id]["count_entrance"]= $zone->count_entrance;
            $arobj[$id]["count_floor"]= $zone->count_floor;
            $arobj[$id]["count_apartament"]= $zone->count_apartament;
            $arobj[$id]["id_zone"]= $id;
            $arobj[$id]["coords"]= unserialize($zone->coorarr);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arobj;
    }

    public function actionExcelExport($id)
    {
        $addresses = ProjectAddress::find()->where(['project_id' => $id])->all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'Город');
        $sheet->setCellValue('C1', 'Район');
        $sheet->setCellValue('D1', 'Улица');
        $sheet->setCellValue('E1', '№ Дома');
        $sheet->setCellValue('F1', 'Строение');
        $sheet->setCellValue('G1', 'Кол-во подъездов');
        $sheet->setCellValue('H1', 'Кол-во квартир');
        $sheet->setCellValue('I1', 'Кол-во этажей');
        $sheet->setCellValue('J1', 'Кол-во вахт');
        $sheet->setCellValue('K1', 'Кол-во ТИП');
        $sheet->setCellValue('L1', 'Зона');
        $sheet->setCellValue('M1', 'Комментарий');
        $sheet->setCellValue('N1', 'X');
        $sheet->setCellValue('O1', 'Y');

        for($i = 0; $i < count($addresses); $i++)
        {
            $c = $i + 2;
            /** @var ProjectAddress $address */
            $address = $addresses[$i];
            $adr = $address->address;
            $sheet->setCellValue('A'.$c, $address->id);
            $sheet->setCellValue('B'.$c, $adr->town);
            $sheet->setCellValue('C'.$c, $adr->region);
            $sheet->setCellValue('D'.$c, $adr->street);
            $sheet->setCellValue('E'.$c, $adr->house);
            $sheet->setCellValue('F'.$c, $adr->housing);
            $sheet->setCellValue('G'.$c, $address->entrance);
            $sheet->setCellValue('H'.$c, $address->apartament);
            $sheet->setCellValue('I'.$c, $address->floor);
            $sheet->setCellValue('J'.$c, $address->porter);
            $sheet->setCellValue('K'.$c, $address->type);
            $sheet->setCellValue('L'.$c, $address->zone->name);
            $sheet->setCellValue('M'.$c, $address->comment);
            $sheet->setCellValue('N'.$c, $address->coord_x);
            $sheet->setCellValue('O'.$c, $address->coord_y);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('data.xlsx');

        Yii::$app->response->sendFile('data.xlsx');
    }

    /**
     * @param integer|null $project_id
     * @return mixed
     */
    public function actionLoadAddress($project_id = null)
    {
        $request = Yii::$app->request;
        $param = $request->post('param');
        $param=json_decode($param,true);
        $polygon = Polygon::factory($param['coords'][0]);
        $arobj["coord"]= $polygon->getObjects($project_id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arobj;
    }

    public function actionLoadAddressNoZone($project_id)
    {
        $arobj["coord"] = ProjectAddress::find()->where(['project_id' => $project_id])->andWhere(['is','zone_id', new \yii\db\Expression('null')])->asArray()->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arobj;
    }


    public function actionSaveZone($project_id,$zone_id=0)
    {
        $request = Yii::$app->request;
        $param = $request->post('param');
        $param=json_decode($param,true);
        if ($zone_id>0) {
            $zone = Zones::findOne($zone_id);
        }  else {
            $zone = new Zones();
        }

        $zone->saveZone($param,$project_id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$param];
    }

    public function actionZoneUpdate($zone_id)
    {
        $request = Yii::$app->request;
        $model = Zones::findOne($zone_id);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить зону",
                    'content'=>$this->renderAjax('_form-createzone', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-zones-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить зону",
                    'content'=>$this->renderAjax('_form-createzone', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-createzone', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionCreateZone()
    {
        $request = Yii::$app->request;
        $model = new Zones();
        if($model->load($request->post())) {
            $model->save();
        }else{
            if($request->isAjax){
                return $this->renderAjax('_form-createzone', [
                    'model' => $model,
                ]);
            }
            return $this->render('_form-createzone', [
                'model' => $model,
            ]);
        }
    }

    public function actionEditZone($id)
    {
        $request = Yii::$app->request;
        $model = Zones::findOne($id);
        if($model->load($request->post())) {
            $model->save();
        }else{
            if($request->isAjax){
                return $this->renderAjax('_form-createzone', [
                    'model' => $model,
                ]);
            }
            return $this->render('_form-createzone', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return array|Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionZoneDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('zone_id');
        Zones::findOne($id)->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-zones-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    //Адресная програма
    /**
     * @param $project_id
     * @return mixed
     */
    public function actionLoadRouteAddress($route_id)
    {
        $route = Routes::findOne($route_id);
        $routeAddres = ArrayHelper::map(RouteAddress::find()->where(['route_id'=>$route_id])->asArray()->all(),'id','address_id');
        $coords = Zones::find()->where(['id'=>$route->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        $address = $polygon->getObjects();
        foreach ($address as $key => $addr){
            if (in_array($addr['id'],$routeAddres)) {
                $address[$key]['inroute'] = true;
            } else {
                $address[$key]['inroute'] = false;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $address;
    }

    /**
     * @param $route_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSetRouteAddress($route_id)
    {
        $request = Yii::$app->request;
        $addressId = $request->post('id');
        $routeAddres = RouteAddress::find()->where(['route_id'=>$route_id])->andWhere(['address_id'=>$addressId])->one();
        if ($routeAddres) {
            $routeAddres->delete();
        } else {
            $routeAddres = new RouteAddress();
            $routeAddres->route_id = $route_id;
            $routeAddres->address_id = $addressId;
            $routeAddres->save();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$addressId,$route_id,$routeAddres->errors];
    }

    public function actionAddProjectAddress($project_id)
    {
        $request = Yii::$app->request;
        $model = new ProjectAddress();
        $model->project_id = (int)$project_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить из адресной базы",
                    'content'=>$this->renderAjax('_form-addresslist_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Добавить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                $address = AddressList::findOne($model->address_id);
                $model->entrance = $address->entrance;
                $model->floor = $address->floor;
                $model->apartament = $address->apartament;
                $model->porter = $address->porter;
                $model->type = $address->type;
                $model->coord_x = $address->coord_x;
                $model->coord_y = $address->coord_y;
                $model->save();
                return [
                    'forceReload'=>'#crud-address-pjax',
                    'title'=> "Создать новый Routes",
                    'content'=>'<span class="text-success">Добавление прошло успешно',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Добавить еще',['add-project-address','project_id'=>$project_id],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить из адресной базы",
                    'content'=>$this->renderAjax('_form-addresslist_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-addresslist_add', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionProjectAddressUpdate($projectaddress_id)
    {
        $request = Yii::$app->request;
        $model = ProjectAddress::findOne($projectaddress_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить объект",
                    'content'=>$this->renderAjax('_form-addresslist_edit', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-address-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить объект",
                    'content'=>$this->renderAjax('_form-addresslist_edit', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-addresslist_edit', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionProjectAddressDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('projectaddress_id');
        $projectAddress = ProjectAddress::findOne($id);
        $projectAddress->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-address-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionProjectAddressBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $address = ProjectAddress::findOne($pk);
            if($address->address != null){
                $address->unlink('address', $address->address);
            }
            $address->delete();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-address-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionProjectAddressClear($project_id)
    {
        $request = Yii::$app->request;
        $projectAddress = ProjectAddress::deleteAll(['project_id'=>$project_id]);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-address-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    //Маршруты
    /**
     * @param $project_id
     * @return mixed
     */
    public function actionAddRoute($project_id)
    {
        $request = Yii::$app->request;
        $model = new Routes();
        $model->project_id = (int)$project_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый маршрут",
                    'content'=>$this->renderAjax('_form-route_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                $model->addRouteAddress();
                return [
                    'forceReload'=>'#crud-routes-pjax',
                    'title'=> "Создать новый маршрут",
                    'content'=>'<span class="text-success">Создание прошло успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['add-route','project_id'=>$project_id],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый маршрут",
                    'content'=>$this->renderAjax('_form-route_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-route_add', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Рспечатка маршрутного листа
     * @param int $route_id ID Маршрута
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPrintRoute($route_id)
    {
        $model = $this->findRoute($route_id);
        $project = Projects::findOne($model->project_id);
        $this->layout='print';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $this->render('_view-print',['model'=>$model,'project'=>$project]),
            'cssFile' => 'css/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-circle {border-radius: 50%;}',
            'options' => [
                'title' => 'Teo-Promoyt',
                'subject' => 'PDF'
            ],
            'methods' => [

                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionPrintRouteAddress($route_id)
    {

        $model = $this->findRoute($route_id);
        $address = RouteAddress::find()->where(['route_id'=>$route_id])->all();
        $this->layout='print';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'content' => $this->render('_view-print_address',['model'=>$model,'address'=>$address]),
            'cssFile' => 'css/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-circle {border-radius: 50%;}',
            'options' => [
                'title' => 'Teo-Promoyt',
                'subject' => 'PDF'
            ],
            'methods' => [

                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionUpdateCoordinates($id)
    {
        $addresses = ProjectAddress::find()->where(['project_id' => $id])->andWhere(['or', ['is not', 'coord_x', null], ['is not', 'coord_y', null]])->all();

        foreach ($addresses as $address)
        {
//            if($address->coord_x == null || $address->coord_y == null){
            $addressList = $address->address;
            if($addressList != null)
            {
                if($addressList->coord_x != null || $addressList->coord_y != null){
                    $address->coord_x = $addressList->coord_x;
                    $address->coord_y = $addressList->coord_y;
                    $address->save(false);
                } else {
                    $coords = YandexStorage::getCoordByAddress($addressList->town.', '.$addressList->region.', ул.'.$addressList->street.', '.$addressList->house.' '.$addressList->housing);
                    $addressList->coord_x = $coords['x'];
                    $address->coord_x = $coords['x'];
                    $addressList->coord_y = $coords['y'];
                    $address->coord_y = $coords['y'];
                    $address->save(false);
                    $addressList->save(false);
                }
            }
        }
//        }
    }

    public function actionUpdateRoute($route_id)
    {
        $request = Yii::$app->request;
        $model = $this->findRoute($route_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Редактировать маршрут",
                    'content'=>$this->renderAjax('_form-route_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-routes-pjax',
                    'title'=> "Редактировать маршрут",
                    'content'=>'<span class="text-success">Создание прошло успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['_form-route_add'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Редактировать маршрут",
                    'content'=>$this->renderAjax('_form-route_add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update_route', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionViewRoute($route_id)
    {
        $model = $this->findRoute($route_id);
        $searchModelAddress= new RouteAddressSearch();
        $dataProviderAddress  = $searchModelAddress->search(Yii::$app->request->queryParams);
        $dataProviderAddress->query->andWhere(['route_id' => $route_id]);

        return $this->render('view_route', [
            'model' => $model,
            'dataProviderAddress' => $dataProviderAddress,
            'searchModelAddress' => $searchModelAddress,
        ]);
    }

    public function actionAddAddress($route_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new RouteAddress(['route_id' => $route_id]);
        if($model->load($request->post()) && $model->save()){
            //Добавляем запись в routes_address_list
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Добавить адрес",
                'content'=>'<span class="text-success">Создание прошло успешно</span>',
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::a('Создать еще',['add-address','route_id'=>$route_id],['class'=>'btn btn-info','role'=>'modal-remote'])
            ];
        } else {
            return [
                'title'=> "Добавить адрес",
                'content'=>$this->renderAjax('_form-routeaddress_add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];
        }
    }

    public function actionUpdateAddress($address_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = RouteAddress::findOne($address_id);


        if($model->load($request->post()) && $model->save()){

            RouteAddress::updateAll(['plane_date' => $model->plane_date], ['route_id' => $model->route]);

            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose'=>true,
            ];
        } else {
            return [
                'title'=> "Редактировать адрес",
                'content'=>$this->renderAjax('_form-routeaddress_add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];
        }
    }

    public function actionDeleteAddress($address_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = RouteAddress::findOne($address_id);
        $model->delete();

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * @param int $route_id
     * @return array|Response
     */
    public function actionDeleteRoute($route_id)
    {
        $request = Yii::$app->request;
        $this->findRoute($route_id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-routes-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionEditableRouteAddress($id, $attribute)
    {
        $model = $this->findRouteAddressList($id); // your model can be loaded here

        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            // use Yii's response format to encode output as JSON
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            // read your posted model attributes
            if ($model->load($_POST)) {
                // read or convert your posted information
                $value = $model->$attribute;
                $model->save(false);

                // return JSON encoded output in the below format
                return ['output'=>$value, 'message'=>''];

                // alternatively you can return a validation error
                // return ['output'=>'', 'message'=>'Validation error'];
            }
            // else if nothing to do always return an empty JSON encoded output
            else {
                return ['output'=>'', 'message'=>''];
            }
        }
    }

    public function actionRouteCreate()
    {
        $request = Yii::$app->request;
        $model= new Routes();
        $project_id = $request->get('project_id');
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить новый ",
                    'content'=>$this->renderAjax('_form-route', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Создать новый ",
                    'content'=>$this->renderAjax('_form-route', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-route', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionRouteUpdate()
    {

        $request = Yii::$app->request;
        $project_id =$request->get('project_id');
        $id = $request->get('id');
        $model = Routes::findOne($id);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'content'=>$this->renderAjax('_form-route', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }else if($model->load($request->post())){
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements($id);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить ".$model->name,
                    'content'=>$this->renderAjax('_form-route', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $project = $this->findModel($project_id);
                $project->placements = $model;
                $project->savePlacements($id);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form-route', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRouteDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        ProjectsPlacement::findOne($id)->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionRouteBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            ProjectsPlacement::findOne($pk)->delete();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findRoute($id)
    {
        if (($model = Routes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findRouteAddressList($id)
    {
        if (($model = RoutesAddressList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUploadExcelData($project_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new UploadForm();

        if($request->isPost){
            $model->file = UploadedFile::getInstance($model, 'file');
            $tmp_name = time() + rand(999, 99999999);

            if($model->upload($tmp_name)){
                $inputFileName = 'uploads/excel/' . $tmp_name . '.' . $model->file->extension;
                $res = ActiveRecordExcelReader::importToProjectAddress($inputFileName,$project_id);
                $savedCount = $res['savedCount'];
                $savedProjectCount = $res['savedProjectCount'];
                $skipCount = $res['skipCount'];
                return [
                    'title' => 'Импортирование данных',
                    'content' => "<span class='text text-success'>Добавлено в базу: {$savedCount}<br> Импортировано в проект: {$savedProjectCount}<br> Пропущено: {$skipCount} "/*.RUtils::numeral()->choosePlural($savedCount, ['запись', 'записи', 'записей'])*/."</span>",
                    'footer' =>  Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]),
                    'forceReload'=>'#crud-address-pjax',
                ];
            }

        } else {


            return [
                'title' => 'Импортирование данных',
                'content' => $this->renderAjax('_upload_form', ['model' => $model]),
                'footer' =>  Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::button('Загрузить',['class'=>'btn btn-info','type'=>"submit"]),
            ];
        }

    }
    public function actionTest()
    {
        EmployeesMove::deleteAll();
        $routes = Routes::find()->all();

        foreach ($routes as $route) {
            $ra = RouteAddress::find()->where(['route_id'=>$route->id])->all();
            $lastA = 0;
            $dateDiff = 0;
            $dateMove = $route->start_date;
            foreach ($ra as $a){
                if ($lastA != 0) {
                    $dateDiff +=60*15;
                    $employeeMove = new EmployeesMove();
                    $employeeMove->route_id = $route->id;
                    $employeeMove->dateandtime = date('Y-m-d H:i:s',strtotime($dateMove)+$dateDiff);
                    $employeeMove->employee_id = $route->employees_id;
                    $employeeMove->coord_x = ($a->address->coord_x + $lastA->address->coord_x)/2 ;
                    $employeeMove->coord_y = ($a->address->coord_y + $lastA->address->coord_y)/2 ;
                    $employeeMove->save();
                }
                $dateDiff +=60*15;
                $employeeMove = new EmployeesMove();
                $employeeMove->route_id = $route->id;
                $employeeMove->dateandtime = date('Y-m-d H:i:s',strtotime($dateMove)+$dateDiff);
                $employeeMove->employee_id = $route->employees_id;
                $employeeMove->coord_x = $a->address->coord_x;
                $employeeMove->coord_y = $a->address->coord_y;
                $employeeMove->save();

                $lastA =$a;
            }


        }
        //var_dump(YandexStorage::getCoordByAddress('Москва, Вешняки, ул.Кетчерская, 10'));

//
//        $zones = Zones::find()->all();
//        $arobj=array();
//        foreach ($zones as $zone) {
//            $id=$zone->id;
//            $arobj[$id]["name-zone"]= $zone->name;
//            $arobj[$id]["coords"]= unserialize($zone->coorarr);
//            var_dump($arobj[$id]["coords"][0]);
//        }

    }
}
