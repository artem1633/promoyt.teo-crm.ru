<?php

namespace app\controllers;

use app\models\AddressList;
use app\models\forms\InputValueForm;
use app\models\forms\SelectDateTime;
use app\models\forms\SelectEmployeesForm;
use app\models\RouteAddress;
use app\models\search\AddressListSearch;
use app\models\search\RouteAddressSearch;
use Yii;
use app\models\Routes;
use app\models\search\RoutesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RoutesController implements the CRUD actions for Routes model.
 */
class RoutesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Routes models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        $project_id =(int) $project_id;
        $searchModel = new RoutesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['project_id'=>$project_id]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
        ]);
    }


    /**
     * Displays a single Routes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Routes #".$model->id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Routes model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $request = Yii::$app->request;
        $model = new Routes();
        $model->project_id = (int)$project_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый Routes",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый Routes",
                    'content'=>'<span class="text-success">Создание прошло успешно</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый Routes",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionAddAddress($route_id)
    {
        $request = Yii::$app->request;
        $searchModel = new AddressListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('add-address', [
            'route_id' => $route_id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionSetPlan()
    {
        $request = Yii::$app->request;
        $model = new SelectDateTime();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('set-plan', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                $route = Routes::findOne($request->get('id'));
                $route->start_date = $model->dateTime;
                $route->status_route = Routes::ROUTE_PLAN;
                $route->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('set-plan', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('set-plan', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionSetNorm()
    {
        $request = Yii::$app->request;
        $model = new InputValueForm();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Введите норму",
                    'content'=>$this->renderAjax('set-norm', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                $routeAddress = RouteAddress::findOne($request->get('id'));
                $routeAddress->plane_work = $model->inputValue;
                $routeAddress->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('set-norm', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {
                $routeAddress = RouteAddress::findOne($request->get('id'));
                $routeAddress->plane_work = $model->inputValue;
                $routeAddress->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('set-norm', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionSetEmployee()
    {
        $request = Yii::$app->request;
        $model = new SelectEmployeesForm();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('set-employee', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                $route = Routes::findOne($request->get('id'));
                $route->employees_id = $model->employee;
                $route->status_route = Routes::ROUTE_WORK;
                $route->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('set-employee', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('set-plan', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEditAddress($address_id)
    {
        $request = Yii::$app->request;
        $model = AddressList::findOne($address_id);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('edit-address', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{
                print_r($model->errors);
                return [
                    'title'=> "Выберите дату",
                    'content'=>$this->renderAjax('edit-address', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {
                return $this->redirect(['edit-address', 'id' => $model->id]);
            } else {
                return $this->render('edit-address', [
                    'model' => $model,
                ]);
            }
        }
    }
    /**
     * Updates an existing Routes model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModelAddress= new RouteAddressSearch();
        $dataProviderAddress  = $searchModelAddress->search(Yii::$app->request->queryParams);
        $dataProviderAddress->query->andWhere(['route_id' => $id]);


        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index', 'project_id' => $model->project_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProviderAddress' => $dataProviderAddress,
                'searchModelAddress' => $searchModelAddress,
            ]);
        }
    }

    /**
     * @param int
     * @return mixed
     */
    public function actionApprove($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->video_approved = 1;
        $result = $model->save(false);

        return ['result' => $result];
    }

    /**
     * Delete an existing Routes model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Routes model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionBulkSelect()
    {
        $id = 1;
        $request = Yii::$app->request;
        foreach ( $request->post( 'pks' ) as $pk ) {
            $address = AddressList::findOne($pk);
            $route = new RouteAddress();
            $route->address_id=$pk;
            $route->route_id=$id;
            $route->save();
        }

        return $this->redirect(['update','id'=>$id]);
    }



    /**
     * Finds the Routes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Routes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Routes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
