<?php

namespace app\controllers;

use app\models\Employees;
use app\models\EmployeesMove;
use app\models\forms\SelectOnlineMapForm;
use app\models\Projects;
use app\models\Routes;
use app\models\Users;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;
use Yii;
use app\models\Fines;
use app\models\FinesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FinesController implements the CRUD actions for Fines model.
 */
class OnlineMapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SelectOnlineMapForm();
        $searchModel = new FinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $employees = Employees::find()->asArray()->all();

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'employees' => $employees,
        ]);
    }

    public function actionGetEmployeeLastPlace($type = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return EmployeesMove::getLastEmployeeMove();
    }

    public function actionGetEmployRoutes($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $date = null;
        $clientId = null;
        $projectId = null;

        if(isset($_GET['date'])){
            $date = $_GET['date'];
        }
        if(isset($_GET['clientId'])){
            $clientId = $_GET['clientId'];
        }
        if(isset($_GET['projectId'])){
            $projectId = $_GET['projectId'];
        }

        $projects = ArrayHelper::getColumn(Projects::find()->andFilterWhere(['client_id' => $clientId])->all(), 'id');

        if($projectId != null){
            $projects[] = $projectId;
        }

        $models = Routes::find()->andFilterWhere(['employees_id' => $id])->andWhere(['project_id' => $projects])->asArray()->all();

        for($i = 0; $i < count($models); $i++){
            $model = $models[$i];
            $route = Routes::findOne($model['id']);
            $moves = EmployeesMove::find()->where(['employee_id' => $id, 'route_id' => $model['id']]);
            if($date != null){
                $moves->andFilterWhere(['between', 'dateandtime', $date.' 00:00:00', $date.' 23:59:59']);
            }
            $moves = $moves->all();
            $datetimes = ArrayHelper::getColumn($moves, 'dateandtime');
            $coordsX = ArrayHelper::getColumn($moves, 'coord_x');
            $coordsY = ArrayHelper::getColumn($moves, 'coord_y');
            $coords = [];
            $timesteps = [];
            for($j = 0; $j < count($coordsY); $j++)
            {
                $coords[] = [$coordsX[$j], $coordsY[$j]];
                $timesteps[] = [$datetimes[$j] ,$coordsX[$j], $coordsY[$j]];
            }
            $models[$i]['coords'] = $coords;
            $models[$i]['datetimes'] = $datetimes;
            $models[$i]['timesteps'] = $timesteps;
            $models[$i]['videos'] = $route->getFiles();
        }

        return $models;
    }

    public function actionGetEmployees()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        $employees = Employees::find()->andFilterWhere(['id' => $employId]);

        if(Yii::$app->user->identity->role == Users::USER_TYPE_CLIENT){
            $clients = ArrayHelper::getColumn(Yii::$app->user->identity->clientsOrganisations, 'id');
            $projects = ArrayHelper::getColumn(Projects::find()->where(['client_id' => $clients])->all(), 'id');
            // var_dump(Routes::find()->where(['project_id' => $projects])->all());
            // exit;
            $employeesIds = ArrayHelper::getColumn(Routes::find()->where(['project_id' => $projects])->all(), 'employees_id');
            $employees->andWhere(['id' => $employeesIds]);
        }

        $employees = $employees->all();

        return $employees;
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Fines #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Fines model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Fines();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Fines",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Fines",
                    'content'=>'<span class="text-success">Create Fines success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new Fines",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Fines #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Fines #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Update Fines #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Fines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fines::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
