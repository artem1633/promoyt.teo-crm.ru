<?php

/** @var \app\models\Projects $model */
/** @var array $subjects */

$daysDuration = $model->getDaysDuration();

$lvlColors = [
    '#e5645e',
    '#a4aa34',
    '#cfd64a',
    '#ebf26d',
    '#f8fcae',
    '#e8fccf',
    '#f5fced',
];

?>


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Город</th>
            <th>Район</th>
            <?php for($i = 0; $i <= $daysDuration; $i++): ?>
                <th style="text-align: center;"><?=Yii::$app->formatter->asDate(date('d.m.Y', strtotime($model->date_start."+{$i} day")), 'php:j M')?></th>
            <?php endfor; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($subjects as $townname=>$town) :?>
            <?php foreach ($town as $regionname=>$region) :?>
                <tr>
                    <td>
                        <?=$townname?>
                    </td>
                    <td>
                        <?=$regionname?>
                    </td>
                    <?php for($i = 0; $i <= $daysDuration; $i++): ?>

                        <?php
                        if (isset($region[date('Y-m-d', strtotime($model->date_start."+{$i} day"))])) {
                            echo "<td style='text-align: center; background-color:red'>".$region[date('Y-m-d', strtotime($model->date_start."+{$i} day"))]."</td>";
                        } else {
                            echo "<td style='text-align: center; background-color:green'></td>";
                        }
                        ?>
                        </td>
                    <?php endfor; ?>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php

function renderSubjectsRecursive($subjects, $daysDuration, $lvlColors, $model, $lvl = 0)
{
    $baseLvl = $lvl;
    foreach ($subjects as $name => $other){
        $lvl = $baseLvl;

        if($name == ''){
            renderSubjectsRecursive($other, $daysDuration, $lvlColors, $model, $lvl);
            continue;
        }

        if(is_array($other)){
            echo "<tr style='background: ".$lvlColors[$lvl].";'>";
        }

        echo "<th>{$name}</th>";

        if(is_array($other)){
            for($i = 0; $i <= $daysDuration;){
                $i++;
                echo "<td></td>";
            }
        } else {
            for($i = 0; $i <= $daysDuration; $i++){

                $address = \app\models\AddressList::findOne($other);
                $adressesIds = \yii\helpers\ArrayHelper::getColumn(\app\models\AddressList::find()
                    ->all(),'id');
                $routesIds = \yii\helpers\ArrayHelper::getColumn(\app\models\RoutesAddressList::find()->where(['address_list_id' => $adressesIds])->all(), 'routes_id');
                $routes = \app\models\Routes::find()->where(['id' => $routesIds, 'project_id' => $model->id])->all();
                $routesNames = \yii\helpers\ArrayHelper::map($routes, 'id', 'name');

                $routeUsingIds = [];

                $date = strtotime($model->date_start."+{$i} day");


                foreach ($routes as $route){
                    /** @var \app\models\Routes $route */
                    $start = strtotime($route->start_date);
                    $end = strtotime($route->fact_date);
                    if($date >= $start && $date <= $end){
                        $routeUsingIds[] = $route->id;
                    }
                }

                if(count($routeUsingIds) == 1){
                    $str = $routeUsingIds[0];
                } else if(count($routeUsingIds) > 1) {
                    $str = implode(',', $routeUsingIds);
                } else {
                    $str = '';
                }

                if($str != ''){
                    $popoverContent = '<ul>';
                    foreach ($routeUsingIds as $id){
                        $popoverContent .= "<li>".$routesNames[$id]."</li>";
                    }
                    $popoverContent .= '</ul>';
                    echo "<td><span data-toggle=\"popover\" data-content=\"{$popoverContent}\" data-title=\"Маршруты\">{$str}</span></td>";
                } else {
                    echo "<td></td>";
                }
            }
        }


        echo "</tr>";

        if(is_array($other)){
            $lvl++;
            renderSubjectsRecursive($other, $daysDuration, $lvlColors, $model, $lvl);
        }
    }
}

$script = <<< JS
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'top',
    });
JS;

Yii::$app->view->registerJs($script, \yii\web\View::POS_READY);

?>