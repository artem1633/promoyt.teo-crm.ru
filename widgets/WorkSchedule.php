<?php

namespace app\widgets;

use app\models\AddressList;
use app\models\Projects;
use app\models\RouteAddress;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class WorkSchedule
 * @package app\widgets
 */
class WorkSchedule extends Widget
{
    /**
     * @var Projects
     */
    public $project;

    /**
     * @var array
     */
    private $subjects;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->project == null){
            throw new InvalidConfigException('Project must be required');
        }

        $routes =$this->project->getAllRoutes();

        $data = [];
        $minDate = null;
        $maxDate = null;
        $routeLast = '';
        foreach($routes as $route){

            $routeAddress = RouteAddress::find()->where(['route_id'=>$route->id])->all();
            foreach ($routeAddress as $address){
                if (isset($data[$route->town][$route->region][$address->plane_date])) {
                    if($routeLast != $route->name){
                        $routeLast = $route->name;
                        $data[$route->town][$route->region][$address->plane_date] .= ', '.$route->name;
                    }
                } else {
                    $routeLast = $route->name;
                    $data[$route->town][$route->region][$address->plane_date] = $route->name;
                }
            }
        }

        $this->subjects = $data;
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('work-schedule/index', [
            'model' => $this->project,
            'subjects' => $this->subjects,
        ]);
    }

    /**
     * @param array $array
     * @param object $value
     * @param array $attributes
     */
    private function addToSubjectsArray(&$array, $value, $attributes)
    {
        $attributes = array_filter($attributes, function($attribute) use($value) {
            return $value->$attribute != null;
        });

        foreach ($attributes as $key => $attribute){
            $attributes[$key] =  $value->$attribute;
        }

        if(ArrayHelper::keyExists($attributes, $array) == false){
            ArrayHelper::setValue($array, $attributes, []);
        }
    }
}