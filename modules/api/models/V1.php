<?php

namespace app\modules\api\models;

use app\models\AddressList;
use app\models\Comments;
use app\models\Employees;
use app\models\EmployeesCategory;
use app\models\Fines;
use app\models\PlacementMethod;
use app\models\Prices;
use app\models\Projects;
use app\models\ProjectsPlacement;
use app\models\RouteAddress;
use app\models\Routes;
use app\models\Zones;
use Yii;

class V1
{
    /**
     * Получает маршруты промоутера
     * @param int $id ID сотрудника
     * @return array|null
     */
    public static function getRoutesFromEmployee($id)
    {
        $routes = Routes::find()
                ->andWhere(['employees_id' => $id])
//                ->andWhere(['<>', 'status_route', Routes::ROUTE_CANCEL])
                ->andWhere(['OR', ['<>', 'status_route', Routes::ROUTE_CANCEL],['IS', 'status_route', null]])
                ->andWhere(['<>', 'files_uploads', 1])
                ->asArray()
                ->all() ?? null;

        if (!$routes) {
            return null;
        }

        $result = [];
        $i = 0;

        foreach ($routes as $route) {
            $category_id = $route['category_id'];
            $project_id = ($route['project_id']);
            $zone_id = ($route['zone_id']);
            $placement_id = ($route['placement_id']);

            unset($route['employees_id']);
            unset($route['video']);
            unset($route['duplicated']);
            unset($route['category_id']);
            unset($route['project_id']);
            unset($route['zone_id']);
            unset($route['placement_id']);

            $route['category'] = self::getCategoryName($category_id);
            $route['project'] = self::getProjectName($project_id);
            $route['project_circulation'] = ProjectsPlacement::find()
                    ->andWhere(['project_id' => $project_id])
                    ->andWhere(['placement_id' => $placement_id])
                    ->sum('circulation') ?? 0;

            $project = ProjectsPlacement::find()
                    ->andWhere(['project_id' => $project_id])
                    ->andWhere(['placement_id' => $placement_id])
                    ->one() ?? null;
            $sum = 0;
            if ($project) {
                $sum = $project->circulation * $project->price;

            }

            $route['project_payment'] = $sum;
            $route['zone'] = self::getZoneName($zone_id);
            $route['placement'] = self::getPlacementName($placement_id);

            //Добавляем дома маршрута
            $route['houses'] = self::getHousesFromRoute($route['id']);

            $result[$i] = $route;
            ++$i;
        }

        Yii::info($result, 'test');

        return $result;
    }

    /**
     * Получает наименование категории сотрудника
     * @param int $id ID Категории сотрудника
     * @return null|string
     */
    private static function getCategoryName($id)
    {
        return EmployeesCategory::findOne($id)->name ?? null;
    }

    /**
     * @param int $id ID проекта
     * @return null|string
     */
    private static function getProjectName($id)
    {
        return Projects::findOne($id)->name ?? null;
    }

    /**
     * @param int $id ID Зоны
     * @return null|string
     */
    private static function getZoneName($id)
    {
        return Zones::findOne($id)->name ?? null;
    }

    /**
     * @param int $id ID Способа размещения
     * @return null|string
     */
    private static function getPlacementName($id)
    {
        return PlacementMethod::findOne($id)->name ?? null;
    }

    /**
     * @param int $id ID маршрута
     * @return array|string
     */
    public static function getHousesFromRoute($id)
    {
        $houses = AddressList::find()
                ->joinWith(['routeAddresses ra'])
                ->select('address_list.*')
                ->andWhere(['ra.route_id' => $id])
                ->asArray()
                ->all() ?? null;

        if (!$houses) {
            return null;
        }

        $result = [];
        $i = 0;

        foreach ($houses as $house) {
            Yii::info($house, 'test');
            $house['status_api'] = $house['routeAddresses'][0]['status_api'];
            unset($house['routeAddresses']);
            $result[$i] = $house;
            ++$i;
        }

        Yii::info($result, 'test');

        return $result;
    }

    /**
     * @param int $id ID маршрута
     * @return array|string
     */
    public static function getAddress($id)
    {
        $house = AddressList::find()
                ->andWhere(['id' => $id])
                ->asArray()
                ->one() ?? null;

        if (!$house) {
            return null;
        }

        Yii::info($house, 'test');

        unset($house['routeAddresses']);

        return $house;
    }

    /**
     * Получает список штрафов промоутера
     * @param Employees $employee Модель сотрудника
     * @return array
     */
    public static function getFines($employee)
    {
        $routes = RouteAddress::find()
                ->joinWith(['route r'])
                ->andWhere(['r.employees_id' => $employee->id])
                ->andWhere(['NOT', ['fine_id' => null]])
                ->asArray()
                ->all() ?? null;

        if (!$routes) {
            return $routes;
        }

        $fines = [];
        foreach ($routes as $key => $route) {
            $fine_name = Fines::getName($route['fine_id']);
            $route['name'] = $fine_name;

            $fine_comment = Comments::findOne($route['fine_comment'])->message ?? null;
            $route['fine_comment'] = $fine_comment;

            //Удаляем ненужное из выдачи
            unset($route['plane_work']);
            unset($route['fact_work']);
            unset($route['confirm_work']);
            unset($route['comment']);
            unset($route['plane_date']);
            unset($route['status_api']);
            unset($route['route']);


            $fines[$key] = $route;
        }


        Yii::info($fines, 'test');

        return $fines;
    }

    /**
     * @param Employees $employee
     * @return array
     */
    public static function getIncomes(Employees $employee)
    {

        $incomes = [];

        //Получаем цену за распространение для марщрутов
        $routes = Routes::find()
            ->andWhere(['employees_id' => $employee->id]);

        foreach ($routes->each() as $key => $route) {

            Yii::info($route->toArray(), 'test');

            $query = RouteAddress::find()
                ->joinWith(['route r'])
                ->andWhere(['NOT', ['fact_work' => null, 'confirm_work' => null]])
                ->andWhere(['r.employees_id' => $employee->id])
                ->andWhere(['r.id' => $route->id]);


            $query_confirm_number = clone $query;
            $confirm_number = $query_confirm_number
                ->sum('confirm_work');
            Yii::info('Кол-во подтвержденных для маршрута ' . $route->id . ': ' . $confirm_number, 'test');

            $query_fact_number = clone $query;
            $fact_number = $query_fact_number
                ->sum('fact_work');
            Yii::info('Кол-во фактически выполненного для маршрута ' . $route->id . ': ' . $fact_number, 'test');

            $fact_work_date = $query->select('fact_work_date')->asArray()->one() ?? null;

            $placement_id = $route->placement_id;
            $project_id = $route->project->id;
            $price_id = ProjectsPlacement::find()
                    ->andWhere(['placement_id' => $placement_id, 'project_id' => $project_id])
                    ->one()
                    ->price_id ?? null;

            if (!$price_id) {
                return null;
            }

            $price = Prices::findOne($price_id)->price ?? null;

            if (!$price) {
                return null;
            }

            $incomes[$key]['route_id'] = $route->id;
            $incomes[$key]['date'] = $fact_work_date['fact_work_date'];
            $incomes[$key]['fact_sum'] = $fact_number * (int)$price;
            $incomes[$key]['confirm_sum'] = $confirm_number * (int)$price;
        }

        return $incomes;
    }

    /**
     * @param string $date_1 Date Should In YYYY-MM-DD Format
     * @param string $date_2 Date Should In YYYY-MM-DD Format
     * RESULT FORMAT:
     * '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
     * '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
     * '%m Month %d Day'                                            =>  3 Month 14 Day
     * '%d Day %h Hours'                                            =>  14 Day 11 Hours
     * '%d Day'                                                        =>  14 Days
     * '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
     * '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
     * '%h Hours                                                    =>  11 Hours
     * '%a Days                                                        =>  468 Days
     * @return integer
     */
    public static function dateDifference($date_1, $date_2)
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        $minutes = 0;
        $hours = $interval->format('%h');


        if ($hours > 0) {
            $minutes = (int)$hours * 60;
        }

        $minutes += (int)$interval->format('%i');

        return $minutes;

    }

}