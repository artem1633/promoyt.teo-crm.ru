<?php

namespace app\modules\api\controllers;

use app\models\AddressList;
use app\models\DoneWorks;
use app\models\Employees;
use app\models\EmployeesMove;
use app\models\EmployeesStatus;
use app\models\forms\UploadForm;
use app\models\RouteAddress;
use app\models\Routes;
use app\modules\api\models\V1;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

//use app\models\Company;
//use app\models\CategoryCompany;
//use app\models\CategoryProduct;
//use app\models\Product;
//use app\models\Orders;
//use app\models\OrdersSearch;
//use app\models\OrdersItem;
//use app\models\Params;
//use app\models\CompanyRating;


/**
 * Default controller for the `api` module
 */
class V1Controller extends Controller
{
    public $layout = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [
                    'login',
                    'register',
                    'get-promoyt-status',
                    'set-promoyt-status',
                    'get-routes',
                    'apply-route',
                    'cancel-route',
                    'test',
                    'get-promoyt-status-month',
                    'get-route-address',
                    'get-address-data',
                    'set-address-data',
                    'set-geo',
                    'get-route-tz',
                    'get-fines',
                    'get-income',
                    'fcm-push',
                    'test-rest',
                    'upload-file',
                    'set-route-status',
                    'upload-video',
                ],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
//                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function actions()
    {
        return [];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('docapi');
    }

    /**
     * Проверяет логин и пароль пользователя, в случае успеха возвращает токен доступа
     * Входные GET данные login, password, id смартфона
     * @return array
     */
    public function actionLogin()
    {
        $request = Yii::$app->request->get();
        // $request['device_token'] = 'c50q8pQkaIU:APA91bEaa5lFlADGnv98965xSrNhUw6P_e--FhY84UqhP0fC5Mi8FPPDjtQ6V1_ctSK-yAUw7q1vejPOxyXQTMV3OLT07xiX9qj6vkXq0lU2ZdmYCqImeUFclVQLvkuxvdkkSR7R21db';
        if (!isset($request['login'])) {
            return ['errors' => 'no login'];
        }
        if (!isset($request['password'])) {
            return ['errors' => 'no password'];
        }
        if (!isset($request['phone_id'])) {
            return ['errors' => 'no phone id'];
        }

        if (!isset($request['device_token'])) {
            return ['errors' => 'no device token'];
        }

        $token = Employees::login($request['login'], $request['password'], $request['phone_id']);
        // $token = '9Kok_-0BOIl9Ioy0rbaRU6n1r6W6op6K';
        if (!$token) {
            return ['errors' => 'error authorization'];
        }

        $device_token = $request['device_token'];
        //Сохраняем FCM токен смартфона
        /* @var Employees $model_employee */
        $model_employee = Employees::getEmployeeByToken($token);
        $model_employee->device_token = $device_token;
        if (!$model_employee->save()) {
            $errors = $model_employee->errors;
        } else {
            $errors = null;
        }

        return ['errors' => $errors, 'token' => $token];

    }

    /**
     * Регистрирует пользователя в системе
     * @return array
     */
    public function actionRegister()
    {
        $request = Yii::$app->request->post();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $login = $request['login'];
        if (!$login) {
            return ['errors' => 'no login'];
        }

        $password = $request['password'];
        if (!$password) {
            return ['errors' => 'no password'];
        }

        $phone_id = $request['phone_id'];
        if (!$phone_id) {
            return ['errors' => 'no phone id'];
        }

        $result = Employees::addEmployee($login, $password, $phone_id);

        if (!$result) {
            return ['errors' => null];
        }

        return ['errors' => $result];

    }

    /**
     * Получает статус промоутера
     * Входные GET данные token
     * @return array
     */
    public function actionGetPromoytStatus()
    {
        $request = Yii::$app->request->get();
        if (!isset($request['token'])) {
            return ['errors' => 'no token'];
        }
        $employee = Employees::getEmployeeByToken($request['token']);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }
        return ['errors' => null, 'status' => $employee->status];

    }

    /**
     * Возвращает список маршрутов для промоутера
     * Принимает параметры: token
     * @return array
     */
    public function actionGetRoutes()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        /** @var Employees $employee */
        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $routes = V1::getRoutesFromEmployee($employee->id);

        if (!$routes) {
            return ['errors' => 'no data'];
        }

        return ['errors' => null, 'data' => $routes];

    }

    /**
     * Изменяет статус маршрута на ROUTE_WORK
     * Принимает POST: token, route
     * @return array
     */
    public function actionApplyRoute()
    {
        $request = Yii::$app->request->post();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];

        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        $result = Routes::applyRoute($route_id);

        if (is_array($result)) {
            return ['errors' => $result];
        }

        return ['errors' => null];
    }

    /**
     * Изменяет статус маршрута на ROUTE_CANCEL
     * @return array
     */
    public function actionCancelRoute()
    {
        $request = Yii::$app->request->post();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];

        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        $comment = $request['comment'];

        if (!$comment) {
            return ['errors' => 'no comment'];
        }

        $result = Routes::cancelRoute($route_id, $comment);

        if (is_array($result)) {
            return ['errors' => $result];
        }

        return ['errors' => null];

    }

    /**
     * Возвращает статусы промоутера, если номер месяца не переедан возвращаются все статусы промоутера
     * @return array
     */
    public function actionGetPromoytStatusMonth()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $month = $request['month'];

        /** @var Employees $employee */
        $result = EmployeesStatus::getStatuses($employee, $month);

        if (!$result) {
            return ['errors' => 'internal error'];
        }

        Yii::info($result, 'test');

        return ['errors' => null, 'data' => $result];
    }

    /**
     * Сохраняет статус промоутера на указанную дату
     * @return array
     */
    public function actionSetPromoytStatus()
    {
        $request = Yii::$app->request->post();

        $token = $request['token'];

        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);

        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $status = $request['status'];

        if (!$status) {
            return ['errors' => 'no status'];
        }

        if (!Employees::getStatusName($status)) {
            return ['errors' => 'unknown status'];
        }

        $date = $request['date'];

        if (!$date) {
            return ['errors' => 'no date'];
        }

        $pattern = '/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/';

        if (!preg_match($pattern, $date)) {
            return ['errors' => 'wrong date format'];
        }

        $result = EmployeesStatus::setStatus($employee->id, $status, $date);

        if (!$result) {
            return ['errors' => null];
        }

        return ['errors' => $result];

    }

    /**
     * Возвращает адреса маршрута
     * @return array
     */
    public function actionGetRouteAddress()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];
        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        if (!Routes::findOne($route_id) ?? null) {
            return ['errors' => 'unknown route'];
        }

        $result = V1::getHousesFromRoute($route_id);

        return ['errors' => null, 'data' => $result];

    }

    /**
     * Возвращает данные по адресу
     * @return array
     */
    public function actionGetAddressData()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $address_id = $request['address'];
        if (!$address_id) {
            return ['errors' => 'no address'];
        }

        if (!AddressList::findOne($address_id) ?? null) {
            return ['errors' => 'unknown address'];
        }

        $result = V1::getAddress($address_id);

        return ['errors' => null, 'data' => $result];
    }

    /**
     * Сохраняет данные по адресу
     * Принимает
     * token,
     * route,
     * address,
     * comment,
     * entrances
     * [
     *      number,
     *      floor_count,
     *      apartment_count,
     *      porter,
     * ]
     *
     * @return array
     */
    public function actionSetAddressData()
    {
        $req = Yii::$app->request;
        if ($req->isPost) {
            Yii::info(Yii::$app->request->post(), 'test');
            $request = $req->post('data');
        } else {
            $request = Json::decode($req->get('data'));
        }

        Yii::info($request, 'test');

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];
        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        if (!Routes::findOne($route_id) ?? null) {
            return ['errors' => 'unknown route'];
        }

        $address_id = $request['address'];
        if (!$address_id) {
            return ['errors' => 'no address'];
        }

        if (!AddressList::findOne($address_id) ?? null) {
            return ['errors' => 'unknown address'];
        }

        /** @var RouteAddress $route_address */
        $route_address = RouteAddress::find()
                ->andWhere(['address_id' => $address_id, 'route_id' => $route_id])
                ->one() ?? null;

        if (!$route_address) {
            return ['errors' => 'address relation error'];
        }

        $entrances = $request['entrances'];
        if (!$entrances || count($entrances) == 0) {
            return ['errors' => 'no entrances'];
        }

        $done_errors = [];

        $factN = 0;
        $factPorch = 0;
        $factStore = 0;
        $factFlat = 0;
        $factEnter = 0;

        foreach ($entrances as $entrance) {

            Yii::info($entrance, 'test');

            if ($entrance['number'] < 0) {
                return ['errors' => 'no entrance number'];
            }
            if ($entrance['floor_count'] < 0) {
                return ['errors' => 'no floor count. Entrance #' . $entrance['number']];
            }
            if (!$entrance['apartment_count'] < 0) {
                return ['errors' => 'no apartment count. ' . $entrance['number']];
            }

            $done_model = DoneWorks::find()
                    ->andWhere([
                        'route_id' => $route_id,
                        'address_id' => $address_id,
                        'entrance_num' => $entrance['number']
                    ])
                    ->one() ?? null;

            if (!$done_model) {
                $done_model = new DoneWorks();
            }

            $done_model->route_id = $request['route'];
            $done_model->n = $entrance['n'];
            $done_model->address_id = $request['address'];
            $done_model->entrance_num = $entrance['number'];
            $done_model->floor_count = $entrance['floor_count'];
            $done_model->apartment_count = $entrance['apartment_count'];

            $factN += intval($entrance['n']);
            $factPorch += intval($entrance['porch']);
            $factStore += intval($entrance['floor_count']);
            $factFlat += intval($entrance['apartment_count']);
            $factEnter += intval($entrance['porter']);

            if (isset($entrance['porter'])) {
                if ($entrance['porter'] == 1) {
                    $done_model->porter = 1;
                } elseif ($entrance['porter'] == 0) {
                    $done_model->porter = 0;
                } else {
                    return ['errors' => 'incorrect porter format'];
                }
            }

            if (!$done_model->save()) {
                Yii::error('Ошибка сохранения данных по подъезду №' . $done_model->entrance_num, '_api_error');
                Yii::error($done_model->errors, '_api_error');
                array_push($done_errors, 'Ошбика сохранения данных по подъезду №' . $done_model->entrance_num);
            }

            //Пересчитываем число фактически отработанных адресов
            $route = Routes::findOne($route_address->route_id);
            $route->fact_count_adress = DoneWorks::find()
                    ->andWhere(['route_id' => $route->id])
                    ->distinct('address_id')
                    ->count() ?? 0;

            Yii::info($route->fact_count_adress, 'test');

            if (!$route->save()){
                Yii::error($route->errors, '_api_error');
            }
        }

        //Получаем общее кол-во квартир из DoneWorks
        $route_address->fact_work = DoneWorks::find()
                ->andWhere(['route_id' => $route_id])
                ->andWhere(['address_id' => $address_id])
                ->sum('apartment_count') ?? 0;

        $route_address->fact_n = $factN;
        $route_address->fact_porch = $factPorch;
        $route_address->fact_store = $factStore;
        $route_address->fact_flat = $factFlat;
        $route_address->fact_enter = $factEnter;
        $route_address->employee_comment = $request['comment'];

//        $route_address->comment = $request['comment'];
        $route_address->status_api = 1;

        if ($route_address->save()) {
            return ['errors' => null];
        }

        if (count($done_errors) > 0) {
            return ['errors' => $done_errors];
        }

        Yii::error($route_address->errors, '_api_error');
        return ['errors' => 'internal error'];


    }

    /**
     * Сохраняет геопозицию промоутера
     * @return array
     */
    public function actionSetGeo()
    {
        $req = Yii::$app->request;

        if ($req->isPost) {
            $request = Yii::$app->request->post();
        } else {
            $request = Yii::$app->request->get();
        }

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        /** @var Employees $employee */
        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route_id'];
        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        if (!Routes::findOne($route_id) ?? null) {
            return ['errors' => 'unknown route'];
        }

        $model = new EmployeesMove();
        $model->employee_id = $employee->id;
        $model->route_id = $route_id;
        $model->dateandtime = date('Y-m-d H:i:s', time());
        $model->timecode = $request['timecode'];
        $model->coord_x = $request['coord_x'];
        $model->coord_y = $request['coord_y'];

        $employee->coord_x = $request['coord_x'];
        $employee->coord_y = $request['coord_y'];
        $employee->save(false);

        if ($model->save()) {
            return ['errors' => null];
        }

        Yii::error($model->errors, '_api_error');
        return ['errors' => 'internal error'];
    }

    //Не реализовван
    public function actionGetRouteTz()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];
        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        $route = Routes::findOne($route_id) ?? null;
        if (!$route) {
            return ['errors' => 'unknown route'];
        }

        return ['errors' => null, 'data' => $route->project->routeTemplates->template];
    }

    /**
     * Получает список штрафов сотрудника
     * @return array
     */
    public function actionGetFines()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        /** @var Employees $employee Модель сотрудника */
        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $result = V1::getFines($employee);
        return ['errors' => null, 'data' => $result];
    }

    /**
     * Пока не понятно что за формула расчета и как она реализована в проекте и реализована ли вообще
     * Получает выплаты промоутера
     */
    public function actionGetIncome()
    {
        $request = Yii::$app->request->get();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        /** @var Employees $employee Модель сотрудника */
        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $result = V1::getIncomes($employee);

        return ['errors' => null, 'data' => $result];

    }

    public function actionUploadFile()
    {
        $req = Yii::$app->request;

        $uf = $req->post('UploadForm');

        if ($uf) {
            $request = $uf;
        } else {
            $request = $req->post();
        }

        if ($req->isPost) {
            Yii::info($req->post(), 'test');
            Yii::info($_FILES, 'test');

            $token = $request['token'];
            if (!$token) {
                return ['errors' => 'no token'];
            }

            /** @var Employees $employee Модель сотрудника */
            $employee = Employees::getEmployeeByToken($token);
            if (!$employee) {
                return ['errors' => 'unknown token'];
            }

            $type = $request['type'];
            if ($type == null) {
                return ['errors' => 'no type'];
            }

            $model = new UploadForm();
            $model->file = UploadedFile::getInstance($model, 'file');
            Yii::info($model->file, 'info');
//            $tmp_name = time() + rand(999, 99999999);
            switch ($type) {
                case 0:
                    $tmp_name = 'passport_' . $employee->id;
                    break;
                case 1:
                    $tmp_name = 'face_' . $employee->id;
                    break;
                case 2:
                    $tmp_name = 'other_' . $employee->id;
                    break;
                default:
                    $tmp_name = 'unknown_type_' . (int)time();
            };

            if ($model->employeeUpload($tmp_name, $employee)) {
                //Файл загружен
                $inputFileName = 'uploads/employees/' . $employee->id . '/' . $tmp_name . '.' . $model->file->extension;

                Yii::info('File path: ' . $inputFileName, 'test');
                Yii::info('File exist: ' . is_file($inputFileName), 'test');

                if (is_file($inputFileName)) {
                    if ($type == 1) {
                        $employee->foto = '/' . $inputFileName;
                        if (!$employee->save()) {
                            Yii::error($employee->errors, 'error');
                        }
                    }
                    return ['errors' => null, 'uploaded file' => $inputFileName];
                } else {
                    return ['errors' => 'error upload file'];
                }
            } else {
                return ['errors' => 'upload error'];
            }
        } else {
            Yii::info($req->get(), 'test');
            return ['errors' => 'To download a file, you must use the POST request'];
        }
    }

    /**
     * Получает
     * token,
     * date,
     * address_id,
     * route,
     *
     * @return array
     */
    public function actionUploadVideo()
    {
        Yii::info('Start upload-video script', 'test');
        Yii::info($_FILES, 'test');

        $req = Yii::$app->request;
//        ini_set('upload_max_filesize', '2038M');
//        ini_set('post_max_size', '2048M');
//        ini_set('max_execution_time', 60 * 10);
//        ini_set('max_input_time', 60 * 10);

        $uf = $req->post('UploadForm');

        if ($uf) {
            $request = $uf;
        } else {
            $request = $req->post();
        }

        $file = $_FILES['file'];
        // Проверим на ошибки загрузки.
        if (!empty($file['error']) || empty($file['tmp_name'])) {
            switch (@$file['error']) {
                case 1:
                    return ['errors' => 'Размер принятого файла превысил максимально допустимый размер, который задан директивой upload_max_filesize конфигурационного файла php.ini.'];
                    break;
                case 2:
                    return ['errors' => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE, указанное в HTML-форме.'];
                    break;
                case 3:
                    return ['errors' => 'Файл был получен только частично.'];
                    break;
                case 4:
                    return ['errors' => 'Файл не был загружен.'];
                    break;
                case 6:
                    return ['errors' => 'Файл не загружен - отсутствует временная директория.'];
                    break;
                case 7:
                    return ['errors' => 'Не удалось записать файл на диск.'];
                    break;
                case 8:
                    return ['errors' => 'PHP-расширение остановило загрузку файла.'];
                    break;
                case 9:
                    return ['errors' => 'Файл не был загружен - директория не существует.'];
                    break;
                case 10:
                    return ['errors' => 'Превышен максимально допустимый размер файла.'];
                    break;
                case 11:
                    return ['errors' => 'Данный тип файла запрещен.'];
                    break;
                case 12:
                    return ['errors' => 'Ошибка при копировании файла.'];
                    break;
                default:
                    Yii::error($file['error'], '_api_error');
                    return ['errors' => 'Файл не был загружен - неизвестная ошибка.'];
                    break;
            }
        }

        if ($req->isPost) {
            Yii::info('$_POST', 'test');
            Yii::info($req->post(), 'test');
            $token = $request['token'];
            if (!$token) {
                return ['errors' => 'no token'];
            }

            /** @var Employees $employee Модель сотрудника */
            $employee = Employees::getEmployeeByToken($token);
            if (!$employee) {
                return ['errors' => 'unknown token'];
            }

            $date = $request['date'];
            if (!$date) {
                return ['errors' => 'no date'];
            }

            $pattern = '/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/';

            if (!preg_match($pattern, $date)) {
                return ['errors' => 'wrong date format'];
            }

            $route_id = $request['route'];
            if (!$route_id) {
                return ['errors' => 'no route'];
            }

            if (!Routes::findOne($route_id) ?? null) {
                return ['errors' => 'unknown route'];
            }

            //Проверяем на отношение сотрудника к маршруту
            $exist = Routes::find()
                ->andWhere(['employees_id' => $employee->id, 'id' => $route_id])
                ->exists();
            if (!$exist) {
                return ['errors' => 'route relation error'];
            }

            $model = new UploadForm();

            if (!isset($_FILES['UploadForm'])) {
                $_FILES['UploadForm']['name']['file'] = $_FILES['file']['name'];
                $_FILES['UploadForm']['tmp_name']['file'] = $_FILES['file']['tmp_name'];
                $_FILES['UploadForm']['type']['file'] = $_FILES['file']['type'];
                $_FILES['UploadForm']['size']['file'] = $_FILES['file']['size'];
                $_FILES['UploadForm']['error']['file'] = $_FILES['file']['error'];
            }
            $model->file = UploadedFile::getInstance($model, 'file');

            Yii::info($_FILES, 'test');
            Yii::info($model->toArray(), 'test');

            $str_date = str_replace(':', '_', $date);
            $str_date = str_replace(' ', '__', $str_date);
            $file_name = 'video_' . $employee->id . '_' . $route_id . '_' . $str_date . '_' . $model->file->baseName;

            /** @var Routes $route_model */
            $route_model = Routes::findOne($route_id);
            $client = $route_model->project->client->name ?? null;
            Yii::info('Клиент: ' . $client, 'test');
            $project = $route_model->project->name ?? null;
            Yii::info('Проект: ' . $project, 'test');
            $date_folder = date('Y-m-d', strtotime($date));
            Yii::info('Папка-дата: ' . $date_folder, 'test');
            $region = $route_model->town . '. ' . $route_model->region;

            $path_dir = 'video/' . $client . '/' . $project . '/' . $date_folder . '/' . $employee->id . '/' . $region . '/' . $route_model->name;
            if ($model->videoUpload($path_dir, $file_name)) {
                //Файл загружен
                $inputFileName = $path_dir . '/' . $file_name . '.' . $model->file->extension;

                Yii::info('File path: ' . $inputFileName, 'test');
                Yii::info('File exist: ' . is_file($inputFileName), 'test');

                if (is_file($inputFileName)) {
                    $uploads_files = $request['uploads_files'];
                    if ($uploads_files) {
                        $route_model->files_uploads = $uploads_files;
                    }
                    $route_model->path_video = $path_dir;
                    if (!$route_model->save()) {
                        Yii::error($route_model->errors, '_api_error');
                        return [
                            'errors' => 'Файл загружен. Ошибка смены статуса загрузки файлов',
                            'uploaded file' => $inputFileName
                        ];
                    }

                    return ['errors' => null, 'uploaded file' => $inputFileName];
                } else {
                    return ['errors' => 'error upload file'];
                }
            } else {
                return ['errors' => 'upload error'];
            }
        } else {
            Yii::info($req->get(), 'test');
            return ['errors' => 'To download a file, you must use the POST request'];
        }
    }

    public function actionSetRouteStatus()
    {
        $request = Yii::$app->request->post();

        $token = $request['token'];
        if (!$token) {
            return ['errors' => 'no token'];
        }

        /** @var Employees $employee */
        $employee = Employees::getEmployeeByToken($token);
        if (!$employee) {
            return ['errors' => 'unknown token'];
        }

        $route_id = $request['route'];
        if (!$route_id) {
            return ['errors' => 'no route'];
        }

        if (!Routes::findOne($route_id) ?? null) {
            return ['errors' => 'unknown route'];
        }

        //Проверяем на отношение сотрудника к маршруту
        $exist = Routes::find()
            ->andWhere(['employees_id' => $employee->id, 'id' => $route_id])
            ->exists();
        if (!$exist) {
            return ['errors' => 'route relation error'];
        }

        $route_status_id = $request['status'];
        if ($route_status_id == null) {
            return ['errors' => 'no status'];
        }

        if (!(new Routes)->isExist($route_status_id)) {
            return ['errors' => 'unknown status'];
        }

        $model = Routes::findOne($route_id);

        switch ($route_status_id) {
            case Routes::ROUTE_START:
                //Если поле start_pause пустое, значит выставляем паузу на маршруте, а если НЕ пустое - значит снимаем паузу
                if (!Routes::findOne($route_id)->start_pause ?? null) {
                    $model->start_pause = date('Y-m-d H:i:s', time());
                } else {
                    //Снимаем паузу
                    //Вычисляем кол-во минут паузы
                    $start_pause = $model->start_pause;
                    if ($start_pause) {
                        $date_diff = V1::dateDifference(date('Y-m-d H:i', time()), $model->start_pause);
                        if ($date_diff) {
                            $model->length_of_pause += $date_diff; //Складываем с предыдущим значением
                            //Обнуляем поле start_pause
                            $model->start_pause = null;
                        }
                    } else {
                        Yii::error('Отсутствует значение времени начала паузы', '_api_error');
                        return ['errors' => 'internal error 1'];
                    }
                }
                $model->start_date = date('Y-m-d H:i:s', time());
                break;
            case Routes::ROUTE_WORK:
                $model->start_date = date('Y-m-d H:i:s', time());
                break;
            case Routes::ROUTE_DONE:
                $model->fact_date = date('Y-m-d H:i:s', time());
                break;
            case Routes::ROUTE_PAUSE:
                $model->start_pause = date('Y-m-d H:i:s', time());
        }

        $model->status_route = $route_status_id;
        if (!$model->save()) {
            Yii::error($model->errors, '_api_error');
            return ['errors' => 'internal error 2'];
        }
        return ['errors' => null];
    }

    /**
     * Отключение CSRF валидации
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionTest()
    {
        $data = [
            'token' => '123',
            'route' => 27,
            'status' => 0
        ];
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('http://promoyt/api/v1/set-route-status')
            ->setData($data)
            ->send();
        if ($response->isOk) {
//            $newUserId = $response->data['id'];
            Yii::info($response->data, 'test');
            return $response->data;
        }
//        VarDumper::dump($response, 10, true);
        return $response->data;
    }

    public function actionTestpush()
    {
        $message['type'] = 'order_status';
        if (!isset($_GET['status'])) {
            $status = 0;
        } else {
            $status = $_GET['status'];
        }
        $message['order_status'] = $status;
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = [
            'to' => 'H47qmwHSq0dA2gb31719lZt8mCK2',
            'notification' => ["message" => $message],
        ];

        $headers = [
            'Authorization: key=AAAASXJW1bY:APA91bHDVmYNAkcejJYtwAJmMlz9RNVvqSNzxPZNiAii7A83DDdUTX9eR9pPZsYNQCsKcoDktptw_JjCZLGU3NnbvhxAa0Ec0MNE7ZJ-5QLiMjiPirMmrXu8HE0b8LFrjr-j64OPdQ2r',
            'Content-Type: application/json',
        ];


        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);
        return $result;
    }

    /**
     * Отправляет пуш сообщения получателям
     * @param array $recipients Получатели сообщений. Содержит FCM токены получателей
     * @param string $message сообщение для отправки
     */
    public function actionFcmPush()
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $recipients = 'drXj7Jr7tC0:APA91bGYGeaISmialsBEEuliJoApeHGsbIjYPPHGaXJM-St4kZ7TEj7VI9xCrWDBnPcYT2P3LQoukXGJN9cucXQZEAEK5wkcmnC-HbyFf8b8mP-fJGhZIOgT2LMbumgWEITwXuiKIAuX';
        $message = 'Test message from Desh';

        $request_body = [
            'to' => $recipients,
            'notification' => [
                'title' => 'Сообщение сервиса http://promoyt.teo-crm.ru',
                'body' => $message,
//                'icon' => 'push-button.png',
//                'click_action' => 'https://sergdudko.tk',
            ],
        ];
        $fields = json_encode($request_body);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=AIzaSyAk3rFzajcWZWL_r99f5zR3-_DJY9QPB0E',
//            'Authorization: key=AIzaSyDy6UZ4XsOyQjTvopY5bTd6fOuRqR0ocG4',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
        echo $response;

    }

    public function actionTestCurl()
    {
        $url = 'http://promoyt/api/v1/set-address-data';
        $data = [
            'token' => 123,
            'route' => 27,
            'address' => 9626,
            'entrances' => [
                [
                    'apartment_count' => 0,
                    'floor_count' => 0,
                    'number' => 0,
                    'porter' => 0,
                ]
            ]
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['data' => $data]));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function actionTestRest()
    {
        return Routes::findOne(27);
    }
}
