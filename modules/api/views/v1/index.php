﻿
<div class="func-list-view">

    <td><div class="func-list-view">
            <hr>
            <h3>login</h3>
            <h3 style="color: green">Реализован: Да</h3>
            <p>Описание: <strong>Авторизація користувача</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/login">http://teo05.litin.vn.ua/api/v1/login</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>login</td>
                    <td>Строка</td>
                    <td>Логин пользователя</td>
                </tr>
                <tr>
                    <td>password</td>
                    <td>Строка</td>
                    <td>Пароль пользователя</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no login</td>
                    <td>Отсутствует Логин</td>
                </tr>
                <tr>
                    <td>no password</td>
                    <td>Отсутствует пароль</td>
                </tr>
                <tr>
                    <td>error autorization</td>
                    <td>Неверний логин или пароль</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>register</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Предварительная регистрация промоутера</strong></p>
            <p>Метод: <strong>POST</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/register">http://teo05.litin.vn.ua/api/v1/register</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-promoyt-status</h3>
            <h3 style="color: green">Реализован: Да</h3>
            <p>Описание: <strong>Возвращает статус промоутера</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-promoyt-status">http://teo05.litin.vn.ua/api/v1/get-promoyt-status</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-promoyt-category-history</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает историю присвоения категорий</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-promoyt-category-history">http://teo05.litin.vn.ua/api/v1/get-promoyt-category-history</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
                <tr>
                    <td>limit</td>
                    <td>Целое число</td>
                    <td>Количество возвращаемих записей</td>
                </tr>
                <tr>
                    <td>offset</td>
                    <td>Целое число</td>
                    <td>Начало возвращаемих записей</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>date</td>
                    <td>Массив</td>
                    <td>Дата</td>
                </tr>
                <tr>
                    <td>category</td>
                    <td>Целое число</td>
                    <td>Категория</td>
                </tr>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-promoyt-rate-history</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает историю рейтингов (Пока не понятно как будет начислятся рейтинг)</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-promoyt-rate-history">http://teo05.litin.vn.ua/api/v1/get-promoyt-rate-history</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-promoyt-status-month</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает данние по промоутеру за месяц</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-promoyt-status-month">http://teo05.litin.vn.ua/api/v1/get-promoyt-status-month</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>send-promoyt-status</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Принимает планируемий статус на определенную дату</strong></p>
            <p>Метод: <strong>POST</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/send-promoyt-status">http://teo05.litin.vn.ua/api/v1/send-promoyt-status</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>токен</td>
                </tr>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get_news</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список новостей</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get_news">http://teo05.litin.vn.ua/api/v1/get_news</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>date</td>
                    <td></td>
                    <td>Дата новости</td>
                </tr>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-routes</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список маршрутов</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-routes">http://teo05.litin.vn.ua/api/v1/get-routes</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>aplly-route</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Сообщает о принятии маршрута в работу</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/aplly-route">http://teo05.litin.vn.ua/api/v1/aplly-route</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>cancel-route</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Сообщает об отказе маршрута</strong></p>
            <p>Метод: <strong>POST</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/cancel-route">http://teo05.litin.vn.ua/api/v1/cancel-route</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-route-address</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список адресов маршрута</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-route-address">http://teo05.litin.vn.ua/api/v1/get-route-address</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>id</td>
                    <td>Целое число</td>
                    <td>ID маршрута</td>
                </tr>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-address-data</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает данние по указанному адресу</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-address-data">http://teo05.litin.vn.ua/api/v1/get-address-data</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>set-address-data</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Передает данние по указаному адресу</strong></p>
            <p>Метод: <strong>POST</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/set-address-data">http://teo05.litin.vn.ua/api/v1/set-address-data</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>send-geo</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Передает координати промоутера</strong></p>
            <p>Метод: <strong>POST</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/send-geo">http://teo05.litin.vn.ua/api/v1/send-geo</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>id</td>
                    <td>Целое число</td>
                    <td>ID маршрута</td>
                </tr>
                <tr>
                    <td>coord_x</td>
                    <td>Вещественное число</td>
                    <td>Координата X</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-route-tz</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает техническое задание по маршруту</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-route-tz">http://teo05.litin.vn.ua/api/v1/get-route-tz</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-income-stat</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает статистику доходов</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-income-stat">http://teo05.litin.vn.ua/api/v1/get-income-stat</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Набор значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>income</td>
                    <td>Вещественное число</td>
                    <td>Доход</td>
                </tr>
                <tr>
                    <td>fine</td>
                    <td>Вещественное число</td>
                    <td>Штраф</td>
                </tr>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-income</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список доходов</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-income">http://teo05.litin.vn.ua/api/v1/get-income</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-income-notconfirm</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список неподтвержденних доходов</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-income-notconfirm">http://teo05.litin.vn.ua/api/v1/get-income-notconfirm</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
                <tr>
                    <td>limit</td>
                    <td>Целое число</td>
                    <td>Количество записей</td>
                </tr>
                <tr>
                    <td>offset</td>
                    <td>Целое число</td>
                    <td>Начало отбора записей</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>date</td>
                    <td></td>
                    <td>Дата</td>
                </tr>
                <tr>
                    <td>id</td>
                    <td>Целое число</td>
                    <td>ID маршрута</td>
                </tr>
                <tr>
                    <td>summa</td>
                    <td>Вещественное число</td>
                    <td>Сумма</td>
                </tr>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-fine</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список штрафов</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-fine">http://teo05.litin.vn.ua/api/v1/get-fine</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>unknown token</td>
                    <td>Неизвестный токен</td>
                </tr>
            </table>




        </div>
    </td>
    <td><div class="func-list-view">
            <hr>
            <h3>get-notice</h3>
            <h3 style="color: red">Реализован: Нет</h3>
            <p>Описание: <strong>Возвращает список уведомлений</strong></p>
            <p>Метод: <strong>GET</strong></p>
            <p>Адрес: <a href="http://teo05.litin.vn.ua/api/v1/get-notice">http://teo05.litin.vn.ua/api/v1/get-notice</a></p>
            <h3>Принимает</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>token</td>
                    <td>Строка</td>
                    <td>Токен</td>
                </tr>
            </table>
            <h3>Возвращает <u>Масив значений</u></h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя параметра</th>
                    <th>Тип</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>errors </td>
                    <td>Строка </td>
                    <td>Имя ошибки или null</td>
                </tr>
            </table>
            <h3>Ошибки</h3>
            <table border="1">
                <thead>
                <tr>
                    <th>Имя ошибки</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tr>
                    <td>no token</td>
                    <td>Отсутствует токен</td>
                </tr>
            </table>




        </div>
    </td>
</div>
