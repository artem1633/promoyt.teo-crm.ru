<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prices */

?>
<div class="prices-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
