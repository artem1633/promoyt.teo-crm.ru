<?php

use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->clients = \yii\helpers\ArrayHelper::getColumn(\app\models\Clients::find()->where(['client_user_id' => $model->id])->all(), 'id');
}

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'role')->dropDownList(Users::getRoles(),['maxlength' => true]) ?>
        </div>
    </div>

    <div class="clients-wrapper" <?=$model->role != Users::USER_TYPE_CLIENT ? 'style="display: none;"' : ''?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'clients')->widget(\kartik\select2\Select2::class, [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Clients::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите организации',
                        'multiple' => true
                    ],
                ]) ?>
            </div>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$script = <<< JS
    $('#users-role').change(function(){
        var role = $(this).val();
        if(role == 5){
            $('.clients-wrapper').slideDown();
        } else {
            $('.clients-wrapper').slideUp();
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>