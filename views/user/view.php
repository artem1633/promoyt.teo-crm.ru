<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'fio',
            'email:email',
            'status',
            'created_at',
            'updated_at',
            'role',
        ],
    ]) ?>

</div>
