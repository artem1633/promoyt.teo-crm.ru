<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AddressList */
?>
<div class="address-list-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'town',
            'region',
            'street',
            'house',
            'coord_x',
            'coord_y',
            'floor',
            'apartament',
            'entrance',
            'CurrentType',
        ],
    ]) ?>

</div>
