<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model \app\models\forms\UploadForm */
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput()->label('Выберите файл для импорта'); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
