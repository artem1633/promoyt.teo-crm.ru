<?php

use app\models\AddressList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?=$form->field($model, 'town')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?=$form->field($model, 'region')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?=$form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?=$form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?=$form->field($model, 'housing')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?=$form->field($model, 'coord_x')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?=$form->field($model, 'coord_y')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-4"><?=$form->field($model, 'floor')->textInput() ?></div>
        <div class="col-md-4"><?=$form->field($model, 'apartament')->textInput() ?></div>
        <div class="col-md-4"><?=$form->field($model, 'entrance')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?=$form->field($model, 'type')->dropDownList(AddressList::getTypes()) ?>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
