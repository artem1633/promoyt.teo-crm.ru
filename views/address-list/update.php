<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddressList */
?>
<div class="address-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
