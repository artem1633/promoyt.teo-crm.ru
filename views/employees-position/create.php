<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmployeesPosition */

?>
<div class="employees-position-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
