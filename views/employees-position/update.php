<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeesPosition */
?>
<div class="employees-position-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
