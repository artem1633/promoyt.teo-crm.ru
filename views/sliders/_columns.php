<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "fone",
        'width' =>'60px',
        'content' => function ($data) {
            if($data->fone == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/noimage.png';
            else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/sliders/' . $data->fone;
            return '<img style="width: 45px; height:45px; border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'format'=>'html',
//        'attribute'=>'text',
//    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'view',
        'content' => function($data){
            if($data->view == 1) return 'Да';
            else return 'Нет';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'view_time',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   