<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeesCategory */
?>
<div class="employees-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
