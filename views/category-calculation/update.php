<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryCalculation */
?>
<div class="category-calculation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
