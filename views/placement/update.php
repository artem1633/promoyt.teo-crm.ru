<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlacementMethod */
?>
<div class="placement-method-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
