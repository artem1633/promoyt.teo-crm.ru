<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlacementMethod */

?>
<div class="placement-method-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
