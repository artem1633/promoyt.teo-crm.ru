<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_route',
        'value'=>'CurrentStatus',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'start_date',
        'value' => function($model){
            return $model->start_date  ? Yii::$app->formatter->asDatetime($model->start_date) : Html::a('Запланировать',['set-plan','id'=>$model->id],['role'=>'modal-remote']);
        },
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fact_date',
    ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'category_id',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'employees_id',
         'value' => function($model){
             switch ($model->status_route) {
                 case 0 : return '';
                 case 1: return Html::a('Назначить',['set-employee','id'=>$model->id],['role'=>'modal-remote']);;
                 case 2: return $model->employees->FIO;
                 case 3: return $model->employees->FIO.Html::a('<i class="glyphicon glyphicon-share"></i>',['duplicate-route','id'=>$model->id],['role'=>'modal-remote','class'=>'pull-right']);;;
                 default: return '';
             }

         },
         'format'=>'raw',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment_employees',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment_manager',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'duplicated',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'count_adress',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'fact_count_adress',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key,'project_id'=>$model->project_id]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   