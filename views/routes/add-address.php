<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Routes */
CrudAsset::register($this);
?>
<div class="routes-sdd-sddress">
    <div id="ajaxCrudDatatableAddress">
        <?=GridView::widget([
            'id'=>'crud-datatableAddress',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns-address.php'),
            'toolbar'=> [
                ['content'=>''
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i>Список Route Addresses ',
                'before'=>'',
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-send"></i>&nbsp; Вибрати',
                            "#" ,
                            [
                                "class"=>"btn btn-info btn-xs",
                                //'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                //'data-request-method'=>'post',
                                'data-confirm-title'=>'Are you sure?',
                                'data-confirm-message'=>'Are you sure want to delete this item',
                                'onClick' => 'addAddress()',
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<script>
    function addAddress() {

        var keys = $('#crud-datatableAddress').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые адреса',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Добавить выбранные адреса?');

        if (dialog == true) {
            $.ajax({
                type: "POST",
                url: 'bulk-select',
                data: {pks: keys},
            });
        }
    }
</script>