<?php

use app\models\AddressList;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Routes */
?>
<div class="address-edit-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'country')->textInput(['maxlength' => true,'readonly'=>true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'oblast')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'town')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'okrug')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'okrug_region')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'korpus')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'coord_x')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'coord_y')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'floor')->textInput() ?></div>
        <div class="col-md-4"><?= $form->field($model, 'apartament')->textInput() ?></div>
        <div class="col-md-4"><?= $form->field($model, 'entrance')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList(AddressList::getTypes()) ?>
        </div>

    </div>
    <?php ActiveForm::end()?>
</div>