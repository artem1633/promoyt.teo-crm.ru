<?php



/* @var $this yii\web\View */
/* @var $model app\models\Routes */

use app\models\Employees;
use kartik\datetime\DateTimePicker;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm; ?>
<div class="routes-set-plan">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model,'employee')->widget(Select2::className(), [
        'options' => ['placeholder' => 'Виберите сотрудника'],
        'data' => Employees::getEmloyeesList(),
        'pluginOptions' => [
            'autoclose' => true
        ],
    ])?>
    <?php ActiveForm::end()?>

</div>
