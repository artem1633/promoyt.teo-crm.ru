<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.oblast',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.region',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'address.town',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.street',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.house',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.korpus',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Етажей',
        'attribute'=>'address.floor',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Квартир',
        'attribute'=>'address.apartament',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Подьездов',
        'attribute'=>'address.entrance',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Тип',
        'attribute'=>'address.type',
        'value'=>function ($model) {
            switch ($model->address->type) {
                case 0 : return "<i class='fa fa-building'></i>";
                case 1 : return "<i class='fa fa-building-o'></i>";
                case 2 : return "<i class='fa fa-bank'></i>";
            }


        },
        'format'=>'raw',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type_work',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'plane_work',
        'value' => function($model){
            return $model->plane_work  ? $model->plane_work .  Html::a('Изменить',
                     ['set-norm','id'=>$model->id],['role'=>'modal-remote','class'=>'pull-right']): Html::a('Проставить',
                 ['set-norm','id'=>$model->id],['role'=>'modal-remote']);
        },
        'format'=>'raw',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'template' => '{view-address} {edit-address} {delete}',  // the default buttons + your custom button
        'buttons' => [
            'view-address' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>',
                    ['/routes/view-address', 'address_id' => $model->address_id], ['role' => 'modal-remote',]);
            },
            'edit-address' => function($url, $model, $key) {     // render your custom button
                    return Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
                    ['/routes/edit-address','address_id'=>$model->address_id],['role'=>'modal-remote']);
            },
        ],
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   