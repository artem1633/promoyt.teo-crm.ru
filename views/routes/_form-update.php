<?php


use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
CrudAsset::register($this);
$this->title = 'Адреса';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);
?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-info pull-right']) ?>
                </div>
            </div>
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProviderAddress,
                'filterModel' => $searchModelAddress,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => require(__DIR__ . '/_columns-route-address.php'),
                'toolbar'=> [
                    ['content'=>
                        '<div style="margin-top:10px;">' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-address','route_id'=>$model->id],
                            ['title'=> 'Добавить', 'class'=>'btn btn-info']).
                        '</div>'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список адресов ',
                    'before'=>'',
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
        <div class="col-md-4">
            <div id="map"></div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>
    
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'size'=>'modal-lg',
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;" type="text/javascript"></script>
<style>
    #map {
        width: 100%; height: 600px; padding: 0; margin: 0;
    }
</style>
<style>
    .YMaps-layer-container img{
        max-width: none;
    }
</style>

<script>
    function init () {
        // Создаем карту с добавленными на нее кнопками.
        var myMap = new ymaps.Map('map', {
            center: [55.750625, 37.626],
            zoom: 10,
        }, {
            buttonMaxWidth: 300
        });

        myMap.geoObjects
            .add(new ymaps.Placemark([55.684758, 37.738521], {
                balloonContent: '1'
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }))
            .add(new ymaps.Placemark([55.833436, 37.715175], {
                balloonContent: '2'
            }, {
                preset: 'islands#icon',
                iconColor: '#735184'
            }))
            .add(new ymaps.Placemark([55.687086, 37.529789], {
                balloonContent: '3'
            }, {
                preset: 'islands#icon',
                iconColor: '#3caa3c'
            }))
            .add(new ymaps.Placemark([55.782392, 37.614924], {
                balloonContent: '4'
            }, {
                preset: 'islands#icon',
                iconColor: 'yellow'
            }))
            .add(new ymaps.Placemark([55.642063, 37.656123], {
                balloonContent: '5'
            }, {
                preset: 'islands#icon',
            }))
            .add(new ymaps.Placemark([55.826479, 37.487208], {
                balloonContent: '6'
            }, {
                preset: 'islands#icon',
                iconColor: '#3b5998'
            }))
            .add(new ymaps.Placemark([55.694843, 37.435023], {
                balloonContent: '7',
            }, {
                preset: 'islands#greenDotIconWithCaption'
            }))
            .add(new ymaps.Placemark([55.790139, 37.814052], {
                balloonContent: '8',
            }, {
                preset: 'islands#icon',
                iconCaptionMaxWidth: '50'
            }));

    }

    ymaps.ready(init);
</script>