<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Routes */
?>
<div class="routes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'status_route',
            'start_date',
            'fact_date',
            'video',
            'duplicated',
            'category_id',
            'employees_id',
            'project_id',
            'comment_employees',
            'comment_manager',
            'count_adress',
            'fact_count_adress',
        ],
    ]) ?>

</div>
