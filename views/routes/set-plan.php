<?php



/* @var $this yii\web\View */
/* @var $model app\models\Routes */

use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm; ?>
<div class="routes-set-plan">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model,'dateTime')->widget(DateTimePicker::className(), [
        'options' => ['placeholder' => 'Виберите время'],
        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'autoclose' => true
        ],
    ])?>
    <?php ActiveForm::end()?>

</div>
