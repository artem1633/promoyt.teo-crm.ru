<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Routes */
?>
<div class="routes-update">

    <?= $this->render('_form-update', [
        'model' => $model,
        'dataProviderAddress' => $dataProviderAddress,
        'searchModelAddress' => $searchModelAddress,
    ]) ?>

</div>
