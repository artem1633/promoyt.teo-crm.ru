<?php

use app\models\Projects;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RoutesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Маршрути';
$this->params['breadcrumbs'][] = Projects::findOne($project_id)->name;
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>
<div class="routes-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'rowOptions'=>function ($model, $key, $index, $grid){
                switch ($model->status_route) {
                    case 0 : return [];
                    case 1: return ['style'=>'background-color:rgba(0,255,0,0.4) '];
                    case 2: return ['style'=>'background-color:rgba(255,255,0,0.4) '];
                    case 3: return ['style'=>'background-color:rgba(255,0,0,0.4) '];
                    default: return [];
                }
                },
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    '<div style="margin-top:10px;">' .
                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create','project_id'=>$project_id],
                        ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                    '</div>'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Список маршрутов ',
                'before'=>'',
                'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).                        
                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
