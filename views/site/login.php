<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Sliders;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход';

?>
<input type="hidden" name="time_reload" id="time_reload" value="3" >
<section id='avtorizatsiya'>
        <div class="avtorizatsiya_left">
            <!-- <a href="#"class="logo"><img src="/../register/img/logo.png" alt=""></a> -->
            <h2>Добро пожаловать!</h2>
            <p>Войдите в свою учетную запись</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal', 'enableClientValidation' => false]); ?>

                <div class="form_group">
                    <label for="">Логин</label>
                    <?= $form->field($model, 'username')->label(false)->textInput(['placeholder' => 'Логин']) ?>
                </div>
                <div class="form_group parol">
                    <label for="">Пароль</label>
                    <?= $form->field($model, 'password')->label(false)->textInput([ 'type' => 'password', 'placeholder' => 'Пароль']) ?>
                    <a href="#"></a>
                </div>
                <div class="form_group zap_menya clr" style="margin-bottom: 10px;">
                    <?= $form->field($model, 'rememberMe')->checkbox(['id'=>"aa"])->label(false) ?>
                    <label for="aa">Запомнить меня</label>
                </div>
                <?= Html::submitButton('Войти в аккаунт', [ 'class' => 'btn btn-info btn-block']) ?>

            <?php ActiveForm::end(); ?>
            <p class="no_acc">Нет аккаунта? <?= Html::a('Зарегистрируйтесь!', ['register'], []); ?></p>
        </div>
        <div class="my_carousel owl-carousel">
            <?php 
                $caruselBlock = Sliders::find()->orderBy(['order' => SORT_ASC])->all();
                $i = 0;
                foreach ($caruselBlock as $value) {
                    $i++;
                    $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/sliders/' . $value->fone;
            ?>
                    <div class="item" style="background-image: url('<?=$path?>');" >
                        <h1 style="color: #fff; margin-bottom: 20px;"><?=$value->title?></h1>
                        <p><?=$value->text?></p>
                    </div>
            <?php } ?>
        </div>
  
    </section>