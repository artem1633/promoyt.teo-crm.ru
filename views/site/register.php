<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Sliders;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-earphone form-control-feedback'></span>"
];

$fieldOptions5 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-send form-control-feedback'></span>"
];

?>

    <input type="hidden" name="time_reload" id="time_reload" value="3" >
    <section id='avtorizatsiya'>
        <div class="avtorizatsiya_left">
            <!-- <a href="#"class="logo"><img src="/../register/img/logo.png" alt=""></a> -->
            <h2>Добро пожаловать!</h2>
            <!-- <p>Введите в свою учетную запись</p> -->
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal', 'enableClientValidation' => false]); ?>

                <div class="form_group">
                    <label for="">Логин</label>
                    <?= $form->field($model, 'fio')->label(false)->textInput(['placeholder' => 'ФИО']) ?>
                </div>

                <div class="form_group">
                    <label for="">Телефон</label>
                    <?= $form->field($model, 'telephone')->label(false)->textInput(['placeholder' => 'Телефон']) ?>
                </div>

                <div class="form_group">
                    <label for="">Почта</label>
                    <?= $form->field($model, 'login')->label(false)->textInput(['placeholder' => 'Почта']) ?>
                </div>

                <div class="form_group parol">
                    <label for="">Пароль</label>
                    <?= $form->field($model, 'password')->label(false)->passwordInput(['placeholder' => 'Пароль']) ?>
                    <a href="#"></a>
                </div>

                <div class="form_group zap_menya clr" style="margin-bottom: 10px;">
                    <?= $form->field($model, 'agree')->checkbox(['id'=>"aa"])->label(false) ?>
                    <label for="aa">Политика в отношении обработки персональных данных</label>
                </div>

                <?= Html::submitButton('Регистрировать', [ 'style' => 'width:100%', 'class' => 'btn btn-info btn-block', 'name' => 'login-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="my_carousel owl-carousel">
            <?php 
                $caruselBlock = Sliders::find()->orderBy(['order' => SORT_ASC])->all();
                $i = 0;
                foreach ($caruselBlock as $value) {
                    $i++;
                    $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/sliders/' . $value->fone;
            ?>
                    <div class="item" style="background-image: url('<?=$path?>');" >
                        <h1 style="color: #fff; margin-bottom: 20px;"><?=$value->title?></h1>
                        <p><?=$value->text?></p>
                    </div>
            <?php } ?>
        </div>
  
    </section>