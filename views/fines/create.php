<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fines */

?>
<div class="fines-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
