<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->input('number') ?>

    <?= $form->field($model,'type')->dropDownList($model->getTypesList()) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'interval_before')->input('number') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'interval_after')->input('number') ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
