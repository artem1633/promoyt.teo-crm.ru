<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fines */
?>
<div class="fines-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'value',
        ],
    ]) ?>

</div>
