<?php


use app\models\Clients;
use app\models\Employees;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
/* @var $employees Employees[] */
CrudAsset::register($this);
$this->title = 'Онлайн карта ';

$this->params['breadcrumbs'][] = ['label' => $this->title];

$clients = Clients::getClientList();
$clientsPks = array_keys($clients);

?>


    <div class="routes-form">

        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs">Онлайн карта</span>
                    <span class="hidden-xs">Онлайн карта</span>
                </a>
            </li>
            <li class="">
                <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs">Архив</span>
                    <span class="hidden-xs">Архив</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="default-tab-1">


                <div class="row">
                    <!--                <div class="col-md-2">-->
                    <!--                    <div class="panel panel-primary">-->
                    <!--                        <div class="panel-heading">-->
                    <!--                            <h3 class="panel-title"> Промоутеры </h3>-->
                    <!--                        </div>-->
                    <!--                        <div class="panel-body">-->
                    <!--                            <span class="btn btn-primary" title="Все"><i class="glyphicon glyphicon-user" ></i></span>-->
                    <!--                            <span class="btn btn-success" title="Закончили"><i class="glyphicon glyphicon-user" ></i></span>-->
                    <!--                            <span class="btn btn-warning" title="В работе"><i class="glyphicon glyphicon-user" ></i></span>-->
                    <!--                            <span class="btn btn-danger" title="Не приступали"><i class="glyphicon glyphicon-user" ></i></span>-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <div class="col-md-12">
                        <div id="map"></div>
                    </div>
                </div>




            </div>
            <div class="tab-pane fade" id="default-tab-2">
                <?php $form = ActiveForm::begin(['id' => 'search-map']); ?>

                <div class="row">
                    <div class="col-md-2">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Фильтры </h3>

                            </div>
                            <div class="panel-body">
                                <?= $form->field($model, 'filterdate')->textInput(['type' => 'date'])?>
                                <?php if(Yii::$app->user->identity->role != \app\models\User::USER_TYPE_CLIENT): ?>
                                    <?= $form->field($model,'filterclient')->dropDownList($clients)?>
                                <?php endif; ?>
                                <?= $form->field($model,'filterproject')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Projects::find()->where(['client_id' => $clientsPks])->all(), 'id', 'name'))?>
                                <?= $form->field($model,'filteremployee')->dropDownList(Employees::getEmloyeesList())?>
                                <?= Html::a('Применить', ['#'], ['class' => 'btn btn-success btn-block', 'data-role' => 'map-filter']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="map-and-video-wrapper" style="position: relative;">
                            <input type="text" id="example_id" name="example_name" value="" />
                            <div class="video-wrapper" style="position: absolute; left: 50px; top: 114px; z-index: 99;">

                            </div>
                            <div id="map2"></div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div id="employeee-info-wrapper" style="margin-top: 30px;">
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'size'=>'modal-lg',
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

    <style>
        #map {
            width: 100%; height: 750px; padding: 0; margin: 0;
        }
        #map2 {
            width:  100%; height: 750px; padding: 0; margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img{
            max-width: none;
        }
    </style>

<?php
//$centermap = \app\models\RouteAddress::getCenterRouteAddress($model->id);
//$centerX = $centermap['centerX'];
//$centerY = $centermap['centerY'];
$script = <<<JS
var center_map = [55.7427,37.82];
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile("/js/online_map.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$employees = json_encode($employees);

$myScript = <<< JS
    // $(document).ready(function(){
       console.log(ymaps);
    
    // for(var i = 0; i <= employess.length; i++)
    //     {
    //         var employ = employess[i];
    //         var coords = [employ.coord_x, employ.coord_y];
    //        
    //         var myPlacemark = new ymaps.Placemark(coords,{
    //             balloonContent: employ.firstname,
    //             iconCaption: 'Очень длиннный, но невероятно интересный текст',
    //         });
    //     }
        // Создаем ломаную линию.
    // });
JS;

$this->registerJs($myScript, \yii\web\View::POS_END);

?>