<?php

use app\models\Fines;
use app\models\RouteAddress;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var \yii\data\ActiveDataProvider $dataProvider */
/* @var integer $route_id */

CrudAsset::register($this);


$route = \app\models\Routes::findOne($route_id);
$project = \app\models\Projects::findOne($route->project_id);
if($project->price_id != null){
    $price = \app\models\Prices::findOne($project->price_id);
    $priceType = $price->price_type;
    $price = $price->price;
} else {
    $price = 0;
    $priceType = null;
}

$this->title = "Адреса";

$this->params['breadcrumbs'][] = [
    'label' => 'Сводный отчет',
    'url' => ['main-report/index'],
];

$this->params['breadcrumbs'][] = $this->title;

?>

    <style>
        .kv-table-float {
            font-size: 10px;
        }
    </style>

    <div class="confirm-work" style="width: 100%;">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-confirm-work',
                'dataProvider' => $dataProvider,
                'resizableColumns' => false,
                'floatHeader' => true,
                'floatHeaderOptions'=>['top'=>'50'],
                'tableOptions' => [
                    'style' => 'font-size: 10px !important;',
                ],
                'options' => [
                    'style' => 'width: 150%;'
                ],
                'pjax' => true,
                'containerOptions' => [
                    // 'style' => 'max-height: 500px;',
                ],
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => [
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => '20px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => '#',
                        'attribute' => 'id',
                        'width' => '70px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Адрес',
                        'attribute' => 'address_id',
                        'value' => function (RouteAddress $model) {
                            return $model->getAddressString($model->address) ?? null;
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.region',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.housing',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.floor',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.apartament',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.entrance',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.porter',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'address.type',
                    ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'addressentrance',
//                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Отработано часов по видео',
                        'content' => function($model) use($route) {
                            $startDate = strtotime(date($route->start_date));
                            $endDate = strtotime(date($route->fact_date));

                            return round((($endDate - $startDate) / 60) / 60);
                        },
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Кол-во распределенной продукции',
                        'attribute' => 'fact_work',
                        'value' => function (RouteAddress $model) {
                            return round($model->fact_work, 2);
                        },
                        'visible' => !Yii::$app->user->identity->isClient(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'employee_comment',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
//                    [
//                        'class' => '\kartik\grid\DataColumn',
//                        'label' => 'Подтвержденный тираж',
//                        'attribute' => 'confirm_work',
//                        'value' => function (RouteAddress $model) {
//                            if(Yii::$app->user->identity->isClient() == false){
//                                return '<div class="input-group">' .
//                                    Html::input('text', 'confirm-work', round($model->confirm_work, 2), [
//                                        'data-confirm-for' => $model->id,
//                                        'class' => 'form-control'
//                                    ])
//                                    . '<span class="input-group-btn">'
//                                    . Html::button('<span class="glyphicon glyphicon-ok"></span>', [
//                                        'class' => 'btn btn-confirm',
//                                        'id' => $model->id,
//                                        'title' => 'Сохранить'
//                                    ])
//                                    . '</span>'
//                                    . '</div>'
//                                    . Html::tag('div', 'Сохранено', [
//                                        'style' => 'display: none;',
//                                        'comment-confirm-for' => $model->id
//                                    ]);
//                            } else {
//                                return $model->confirm_work;
//                            }
//                        },
//                        'format' => 'raw',
//                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'fact_n',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'fact_porch',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'fact_store',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'fact_flat',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'fact_enter',
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'confirm_n',
                        'editableOptions'=> ['formOptions' => ['action' => ['/main-report/edit']]],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'confirm_porch',
                        'editableOptions'=> ['formOptions' => ['action' => ['/main-report/edit']]],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'confirm_store',
                        'editableOptions'=> ['formOptions' => ['action' => ['/main-report/edit']]],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'confirm_flat',
                        'editableOptions'=> ['formOptions' => ['action' => ['/main-report/edit']]],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\EditableColumn',
                        'attribute' => 'confirm_enter',
                        'editableOptions'=> ['formOptions' => ['action' => ['/main-report/edit']]],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
//                    [
//                        'headerOptions' => [
//                            'colspan' => '2',
//                            'style' => 'padding:0px; width:1920px',
//                        ],
//                        'header' => "<table style = 'width: 192px;' ><tr style='border-bottom: 1px solid #ddd'><th colspan=2 style='text-align: center'>Оплата</th></tr>".
//                            "<tr>
//                                <th style = 'width:95px ;border-right: 1px solid #ddd; text-align: center'>Оплата Н</th>
//                                <th style='text-align: center'>Оплата Подъезда</th>
//                            </tr>
//                            </table>",
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'paymentN',
//                        'label' => 'Оплата',
//                        'options' => ['style' => 'width: 96px;'],
//                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'paymentN',
                        'label' => 'Оплата Н',
                        'format' => ['currency', 'rub'],
                        'options' => ['style' => 'width: 96px;'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'paymentPorch',
                        'label' => 'Оплата Подъезд',
                        'format' => ['currency', 'rub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'paymentStore',
                        'label' => 'Оплата Этаж',
                        'format' => ['currency', 'rub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'paymentFlat',
                        'label' => 'Оплата Квартира',
                        'format' => ['currency', 'rub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'paymentValue',
                        'label' => 'Итого',
                        'format' => ['currency', 'rub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
//                    [
//                        'class' => '\kartik\grid\DataColumn',
//                        'attribute' => 'paymentValue',
//                        'label' => 'Оплата',
//                        'content' => function(RouteAddress $model) use($priceType, $price){
//                            if($priceType !== null){
////                                if($priceType == \app\models\Prices::TYPE_COUNT){
//                                    return $model->confirm_work * $price;
////                                } else if($priceType == \app\models\Prices::TYPE_HOUR) {
////                                    return 0;
////                                }
//                            }
//                        },
//                        'visible' => !Yii::$app->user->identity->isClient(),
//                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Штрафы',
                        'attribute' => 'fine_id',
                        'value' => function (RouteAddress $model) {
                            return Html::dropDownList('fines', $model->fine_id, Fines::getFinesList(), [
                                'class' => 'form-control dropdown-fines',
                                'prompt' => 'Выберите тип штрафа',
                                'data-fines' => $model->id,
                            ]);
                        },
                        'format' => 'raw',
                        'visible' => !Yii::$app->user->identity->isClient(),
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Комментарий',
                        'attribute' => 'comment',
                        'value' => function (RouteAddress $model) {
                            if ($model->comment) {
                                $text = $model->comment;
                            } else {
                                $text = 'Добавить комментарий';
                            }
                            return Html::a($text, ['/route-address/comment', 'id' => $model->id],
                                ['role' => 'modal-remote']);
                        },
                        'format' => 'raw',
                        'visible' => !Yii::$app->user->identity->isClient(),
                    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'header' => "&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
//        'template' => '{report}',
//        'buttons' => [
//            'report' => function ($url, $model, $key) {     // render your custom button
//                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>',
//                    ['confirm-work2', 'route_id' => $model['id']], ['data-pjax' => '0', 'title' => 'Адреса']);
//            },
//        ],
//        'dropdown' => false,
//        'vAlign' => 'middle',
//        'urlCreator' => function ($action, $model, $key, $index) {
//            return Url::to([$action, 'id' => $model['id']]);
//        },
//        'viewOptions' => [
//            'label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>',
//            'role' => 'modal-remote'
//        ],
//        'updateOptions' => [
//            'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
//            'role' => 'modal-remote',
//            'title' => 'Изменить',
//            'data-toggle' => 'tooltip'
//        ],
//        'deleteOptions' => [
//            'role' => 'modal-remote',
//            'title' => 'Удалить',
//            'data-confirm' => false,
//            'data-method' => false,// for overide yii data api
//            'data-request-method' => 'post',
//            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
//            'data-toggle' => 'tooltip',
//            'data-confirm-title' => 'Вы уверенны?',
//            'data-confirm-message' => 'Вы действительно хотите удалить запись '
//        ],
//    ],

                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>&nbsp;Список адресов'  ,
                    'before' => '',
                    'after' => '',
                ]
            ]);
        } catch (Exception $e) {
            return new \yii\web\HttpException(500, 'Ошибка сервера');
        }
        ?>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$(document).ready(function(){
    $(document).on('click', '.btn-confirm', function() {
        var id = $(this).attr('id');
        var num = $("[data-confirm-for=" + id + "]").val();
        var info_block = $("[comment-confirm-for=" + id + "]");
        console.log('RouteAddress: ' + id);
        console.log('Num: ' + num);
      
      $.post(
          '/route-address/confirm-work',
          {
              'id': id,
              'num': num
          },
          function(response) {
            console.log(response);
            if (response['error'] === 1){
                info_block.removeClass('text-success');
                info_block.addClass('text-danger');
                info_block.text(response['data']);
                info_block.show();
            } else {
                info_block.removeClass('text-danger');
                info_block.addClass('text-success');
                info_block.text('Сохранено');
                info_block.fadeIn(1000).delay(3000).fadeOut(1000);
                
            }
          }
      )
    });
    
    $(document).on('click', '.btn-payment', function() {
        var id = $(this).attr('id');
        var num = $("[data-payment-for=" + id + "]").val();
        var info_block = $("[comment-payment-for=" + id + "]");
      
      $.post(
          '/route-address/payment',
          {
              'id': id,
              'num': num
          },
          function(response) {
            console.log(response);
            if (response['error'] === 1){
                info_block.removeClass('text-success');
                info_block.addClass('text-danger');
                info_block.text(response['data']);
                info_block.show();
            } else {
                info_block.removeClass('text-danger');
                info_block.addClass('text-success');
                info_block.text('Сохранено');
                info_block.fadeIn(1000).delay(3000).fadeOut(1000);
                
            }
          }
      )
    });
    
    $(document).on('change', '.dropdown-fines', function(){
        var ra_id = $(this).attr('data-fines');
        var fine_id = $(this).val();
        console.log(ra_id);
        console.log(fine_id);
        $.post(
            '/route-address/set-fine',
            {
                'id': ra_id,
                'fine': fine_id
            },
            function(response) {
                console.log(response);
            }
        );
    })
})
JS;

$this->registerJs($script);
