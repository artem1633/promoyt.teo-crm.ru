<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '#',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Клиент',
        'attribute'=>'clientname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '№ проекта',
        'attribute'=>'projectid',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Проект',
        'attribute'=>'projectname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Зона',
        'attribute'=>'zone_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Способ размещения',
        'attribute'=>'placementmethod',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Тираж',
        'attribute'=>'circulation',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Период работ',
        'attribute'=>'routeperiod',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'town',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'region',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'visible' => !Yii::$app->user->identity->isClient(),
        'template' => '{edit} {print}',
        'buttons' => [
            'print' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button>',
                    ['/projects/print-route-address','route_id'=>$model['id']],['data-pjax' => '0','target'=>'_blank','title'=>'Печатать']);
            },
            'edit' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-edit"></span></button>',
                    ['route-edit','id'=>$model['id']],['data-pjax' => '0','title'=>'Печатать']);
                http://teo05.litin.vn.ua/projects/print-route-address?route_id=9
            },
        ],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','data-pjax' => '0','title'=>'Изменить', 'data-toggle'=>'tooltip'],

],

];   