<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
///* @var string $path_file Путь к файлу видео */
/* @var array $files Наименования видеофайлов */

//Yii::info($path_file, 'test');
Yii::info($files, 'test');

?>
<div class="video-report-view">
    <?php
    //    if($path_file){
    //        try {
    //            echo VideoJsWidget::widget([
    //                'options' => [
    //                    'class' => 'video-js vjs-default-skin vjs-big-play-centered',
    ////                'poster' => "http://www.videojs.com/img/poster.jpg",
    //                    'controls' => true,
    //                    'preload' => 'auto',
    //                    'width' => '970',
    //                    'height' => '400',
    //                ],
    //                'tags' => [
    //                    'source' => [
    ////                    ['src' => 'http://vjs.zencdn.net/v/oceans.mp4', 'type' => 'video/mp4'],
    ////                    ['src' => 'http://vjs.zencdn.net/v/oceans.webm', 'type' => 'video/webm']
    ////                    ['src' => 'http://promoyt/video/Заказчик 1/Проект тест1/2019-06-02/1/Москва. Текстильщики/Текстильщики 1/WhatsApp Video 2019-08-07 at 14.07.34.mp4', 'type' => 'video/mp4']
    //                        ['src' => Url::to([$path_file], true), 'type' => 'video/mp4'],
    //                    ],
    //                ]
    //            ]);
    //        } catch (Exception $e) {
    //            Yii::error($e->getMessage(), '_error');
    //        }
    //    } else {
    //        echo '<h4>Неизвестно местоположение видеофайла</h4>';
    //    }

    if (count($files) > 0){
        if($model->video_approved === 1 || Yii::$app->user->identity->isSuperAdmin()){
            foreach ($files as $file){
                try {
                    echo \app\widgets\VideoJsWidget::widget([
                        'options' => [
                            'class' => 'video-js vjs-default-skin vjs-big-play-centered',
                            'controls' => true,
                            'preload' => 'auto',
                            'width' => '970',
                            'height' => '400',
                        ],
                        'tags' => [
                            'source' => [
                                ['src' => Url::to([$file], true), 'type' => 'video/mp4'],
                            ],
                        ],
                        'jsOptions' => 'function() {
                      
                      var player = this;
                      player.width = 970;
                      player.height = 400;
                        
                        player.on("timeupdate", function(){
                                console.log("tick");
                                      if(player.currentTime() == player.duration()){
                                          console.log("video is ended");
                                      }
                        });

                   }',
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                }
            }
        }
    }

    ?>

</div>
