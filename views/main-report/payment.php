<?php

use app\models\Employees;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rotes */

?>
<style>
    .form-group {
        margin-bottom: 0px;
    }
    .form-control {
        height: 26px;
        padding: 1px;
    }
    input {
        height: 22px;
    }
</style>
<div class="clients-create">
    <?php $form = ActiveForm::begin([


    ]); ?>
    <table class="table table-condensed">
        <tr>
            <th rowspan="2">План</th>
            <th rowspan="2">Факт</th>
            <th rowspan="2">Подтв.</th>
            <th colspan="3">Штраф</th>
            <th rowspan="2">Коментарий</th>
        </tr>
        <tr>
            <th>Тип</th>
            <th>Значение</th>
            <th>Коментарий</th>
        </tr>

        <?php foreach ($model as $item) :?>
            <tr>
                <td><?= $form->field($item, 'plane_work['.$item->id.']')->textInput()->label(false) ?></td>
                <td><?= $form->field($item, 'fact_work[]')->textInput()->label(false) ?></td>
                <td><?= $form->field($item, 'confirm_work[]')->textInput() ->label(false)?></td>
                <td><?= $form->field($item, 'fine_id[]')->dropDownList(\app\models\Fines::getFinesList()) ->label(false)?></td>
                <td><?= $form->field($item, 'fine_percent[]')->textInput() ->label(false)?></td>
                <td><?= $form->field($item, 'fine_comment[]')->textInput() ->label(false)?></td>
                <td><?= $form->field($item, 'comment[]')->textInput() ->label(false)?></td>

            </tr>
        <?php endforeach;?>
    </table>
    <?php ActiveForm::end(); ?>
</div>
