<?php

use app\models\Fines;
use app\models\RouteAddress;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => '#',
        'attribute' => 'id',
        'width' => '70px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Адрес',
        'attribute' => 'address_id',
        'value' => function (RouteAddress $model) {
            return $model->getAddressString($model->address) ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Кол-во распределенной продукции',
        'attribute' => 'fact_work',
        'value' => function (RouteAddress $model) {
            return round($model->fact_work, 2);
        },
        'visible' => !Yii::$app->user->identity->isClient(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Подтвержденный тираж',
        'attribute' => 'confirm_work',
        'value' => function (RouteAddress $model) {
            if(Yii::$app->user->identity->isClient() == false){
                return '<div class="input-group">' .
                    Html::input('text', 'confirm-work', round($model->confirm_work, 2), [
                        'data-confirm-for' => $model->id,
                        'class' => 'form-control'
                    ])
                    . '<span class="input-group-btn">'
                    . Html::button('<span class="glyphicon glyphicon-ok"></span>', [
                        'class' => 'btn btn-confirm',
                        'id' => $model->id,
                        'title' => 'Сохранить'
                    ])
                    . '</span>'
                    . '</div>'
                    . Html::tag('div', 'Сохранено', [
                        'style' => 'display: none;',
                        'comment-confirm-for' => $model->id
                    ]);
            } else {
                return $model->confirm_work;
            }
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'paymentValue',
        'label' => 'Оплата',
        'content' => function(RouteAddress $model){

        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Штрафы',
        'attribute' => 'fine_id',
        'value' => function (RouteAddress $model) {
            return Html::dropDownList('fines', $model->fine_id, Fines::getFinesList(), [
                'class' => 'form-control dropdown-fines',
                'prompt' => 'Выберите тип штрафа',
                'data-fines' => $model->id,
            ]);
        },
        'format' => 'raw',
        'visible' => !Yii::$app->user->identity->isClient(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Комментарий',
        'attribute' => 'comment',
        'value' => function (RouteAddress $model) {
            if ($model->comment) {
                $text = $model->comment;
            } else {
                $text = 'Добавить комментарий';
            }
            return Html::a($text, ['/route-address/comment', 'id' => $model->id],
                ['role' => 'modal-remote']);
        },
        'format' => 'raw',
        'visible' => !Yii::$app->user->identity->isClient(),
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'header' => "&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
//        'template' => '{report}',
//        'buttons' => [
//            'report' => function ($url, $model, $key) {     // render your custom button
//                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>',
//                    ['confirm-work2', 'route_id' => $model['id']], ['data-pjax' => '0', 'title' => 'Адреса']);
//            },
//        ],
//        'dropdown' => false,
//        'vAlign' => 'middle',
//        'urlCreator' => function ($action, $model, $key, $index) {
//            return Url::to([$action, 'id' => $model['id']]);
//        },
//        'viewOptions' => [
//            'label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>',
//            'role' => 'modal-remote'
//        ],
//        'updateOptions' => [
//            'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
//            'role' => 'modal-remote',
//            'title' => 'Изменить',
//            'data-toggle' => 'tooltip'
//        ],
//        'deleteOptions' => [
//            'role' => 'modal-remote',
//            'title' => 'Удалить',
//            'data-confirm' => false,
//            'data-method' => false,// for overide yii data api
//            'data-request-method' => 'post',
//            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
//            'data-toggle' => 'tooltip',
//            'data-confirm-title' => 'Вы уверенны?',
//            'data-confirm-message' => 'Вы действительно хотите удалить запись '
//        ],
//    ],

];   