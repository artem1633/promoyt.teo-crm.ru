<?php

use app\models\PlacementMethod;
use app\models\Zones;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
CrudAsset::register($this);
$this->title = 'Маршрут: '.$model->name;

$this->params['breadcrumbs'][] = ['label' => 'Сводный отчет','url'=>['/main-report']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="clients-update">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Основная информация</span>
                <span class="hidden-xs">Основная информация</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Адреса</span>
                <span class="hidden-xs">Адреса</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'town')->textInput() ?></div>
                <div class="col-md-6"><?= $form->field($model, 'region')->textInput() ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'name')->textInput() ?></div>
                <div class="col-md-6"><?= $form->field($model, 'placement_id')->dropDownList(PlacementMethod::getPlacementMethodList()) ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'maket')->textInput() ?></div>
                <div class="col-md-6"> <?= $form->field($model, 'normal')->textInput() ?></div>
            </div>
            <?= $form->field($model, 'comment_manager')->textarea(['rows' => 3]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>

        <div class="tab-pane fade" id="default-tab-2">
            <div class="row">
                <div class="col-md-8">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'options' => ['style' => 'font-size:12px;'],
                        'dataProvider' => $dataProviderAddress,
                        'filterModel' => $searchModelAddress,
                        'pjax'=>true,
                        'pjaxSettings' => [
                            'options' => [
                                'enablePushState' => false,
                            ],
                        ],
                        'columns' => require(__DIR__ . '/_columns-route-address.php'),
                        'toolbar'=> [
                            ['content'=>
                                '<div style="margin-top:10px;">' .
                                Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-address','route_id'=>$model->id],
                                    ['title'=> 'Добавить', 'class'=>'btn btn-info', 'role' => 'modal-remote']).
                                '</div>'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => '<i class="glyphicon glyphicon-list"></i>Список адресов ',
                            'before'=>'',
                            'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Are you sure?',
                                            'data-confirm-message'=>'Are you sure want to delete this item'
                                        ]),
                                ]).
                                '<div class="clearfix"></div>',
                        ]
                    ])?>
                </div>
                <div class="col-md-4">
                    <div id="map"></div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
