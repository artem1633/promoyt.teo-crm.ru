<?php

use app\models\Routes;
use kartik\grid\GridView;
use yii\helpers\Html;
use johnitvn\ajaxcrud\BulkButtonWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <div class="row">
        <div class="col-md-12">
            <?=GridView::widget([
                'id'=>'crud-address',
                'dataProvider' => $dataProviderReportFact,
                'filterModel' => $searchModelReportFact,
                'tableOptions' => ['style' => 'font-size: 11px;'],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['style' => 'background-color:'.Routes::$colorStatus[$model['status']]];
                },
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'options' => [ 'style' => 'table-layout:fixed;' ],
                'columns' => require(__DIR__ . '/_columns-report.php'),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список маршрутов ',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>

</div>
