<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сводный отчет';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="main-report-index">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">План работ</span>
                <span class="hidden-xs">План работ</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Назначено в график</span>
                <span class="hidden-xs">Назначено в график</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Отчет</span>
                <span class="hidden-xs">Отчет</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <?= $this->render('_view-plane_work', [
                'model' => $model,
                'searchModelPlaneWork' => $searchModelPlaneWork,
                'dataProviderPlaneWork' => $dataProviderPlaneWork,
            ]) ?>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <?= $this->render('_view-assigned_grafik', [
                'model' => $model,
                'searchModelAssignedGrafik' => $searchModelAssignedGrafik,
                'dataProviderAssignedGrafik' => $dataProviderAssignedGrafik,

            ]) ?>
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <?= $this->render('_view-report', [
                'model' => $model,
                'searchModelReportFact' => $searchModelReportFact,
                'dataProviderReportFact' => $dataProviderReportFact,

            ]) ?>
        </div>

    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
