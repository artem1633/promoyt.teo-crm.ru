<?php

use app\models\Routes;
use kartik\grid\GridView;
use yii\helpers\Html;
use johnitvn\ajaxcrud\BulkButtonWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <div class="row">
        <div class="col-md-12">
            <?=GridView::widget([
                'id'=>'crud-assigned-grafik',
                'dataProvider' => $dataProviderAssignedGrafik,
                'filterModel' => $searchModelAssignedGrafik,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['style' => 'background-color:'.Routes::$colorStatus[$model['status']]];
                },
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => require(__DIR__ . '/_columns-assigned_grafik.php'),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список маршрутов ',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>
    
</div>
