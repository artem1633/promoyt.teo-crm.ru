<?php

use app\models\Employees;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rotes */

?>
<div class="clients-create">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'employees_id')->dropDownList(Employees::getEmloyeesList()) ?></div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
