<?php

use wbraganca\videojs\VideoJsAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
///* @var string $path_file Путь к файлу видео */
/* @var array $files Наименования видеофайлов */
/* @var boolean $runViewJs */

//Yii::info($path_file, 'test');
Yii::info($files, 'test');

VideoJsAsset::register($this);

?>
<div class="video-report-view">
    <?php
    //    if($path_file){
    //        try {
    //            echo VideoJsWidget::widget([
    //                'options' => [
    //                    'class' => 'video-js vjs-default-skin vjs-big-play-centered',
    ////                'poster' => "http://www.videojs.com/img/poster.jpg",
    //                    'controls' => true,
    //                    'preload' => 'auto',
    //                    'width' => '970',
    //                    'height' => '400',
    //                ],
    //                'tags' => [
    //                    'source' => [
    ////                    ['src' => 'http://vjs.zencdn.net/v/oceans.mp4', 'type' => 'video/mp4'],
    ////                    ['src' => 'http://vjs.zencdn.net/v/oceans.webm', 'type' => 'video/webm']
    ////                    ['src' => 'http://promoyt/video/Заказчик 1/Проект тест1/2019-06-02/1/Москва. Текстильщики/Текстильщики 1/WhatsApp Video 2019-08-07 at 14.07.34.mp4', 'type' => 'video/mp4']
    //                        ['src' => Url::to([$path_file], true), 'type' => 'video/mp4'],
    //                    ],
    //                ]
    //            ]);
    //        } catch (Exception $e) {
    //            Yii::error($e->getMessage(), '_error');
    //        }
    //    } else {
    //        echo '<h4>Неизвестно местоположение видеофайла</h4>';
    //    }

    if (count($files) > 0){
        foreach ($files as $file){
            try {
//                echo VideoJsWidget::widget([
//                    'options' => [
//                        'class' => 'video-js vjs-default-skin vjs-big-play-centered',
//                        'controls' => false,
//                        'preload' => 'auto',
//                        'width' => '970',
//                        'height' => '400',
//                    ],
//                    'tags' => [
//                        'source' => [
//                            ['src' => Url::to([$file], true), 'type' => 'video/mp4'],
//                        ],
//                    ]
//                ]);
                echo '<video id="videojs-w0" class="video-js vjs-default-skin vjs-big-play-centered" width="970" height="400" preload="auto">
                        <source type="video/mp4" src="'.Url::to([$file], true).'">
                    </video>';

                break;
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            }
        }
    }

    ?>

</div>

<?php if($runViewJs): ?>

    <?php

    $this->registerJs("

    $('.video-wrapper').draggable();

    videoInstance = videojs('#videojs-w0', {
        width: 200,
        height: 300,
        controls: true,
    });
    // videoInstance.width(200);
    // videoInstance.height(300);
    // videoInstance.fullscreen();


", \yii\web\View::POS_READY);

    ?>

<?php endif; ?>
