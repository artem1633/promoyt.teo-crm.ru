<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => '#',
//        'attribute'=>'id',
//        'width' => '70px',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => '№ проекта',
//        'attribute'=>'projectid',
//        'group' => true,
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Проект',
        'attribute'=>'projectname',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Адрес',
//        'value' => function($data){
//            return $data['address_town'].' '.$data['address_street'];
//
//        },
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Н',
//        'value' => 'fact_n',
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Подъезд',
//        'value' => 'fact_porch',
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Этаж',
//        'value' => 'fact_store',
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Квартира',
//        'value' => 'fact_flat',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'routeName',
        'label' => 'Маршрут',
        'value' => function($data){
            return $data['route_name'];
        },
        'format' => 'raw',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Итого к оплате',
//        'value' => function($data){
//            if
//        },
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Клиент',
        'attribute'=>'clientname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Зона',
        'attribute'=>'zone_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Способ размещения',
        'attribute'=>'placement_method_name',
    ],
//    [
//        'headerOptions' => [
//            'colspan' => '2',
//            'style' => 'padding:0px; width:120px',
//
//        ],
//        'header' => "<table style = 'width: 120px;' ><tr style='border-bottom: 1px solid #ddd'><th colspan=2 style='text-align: center'>Тираж</th></tr>".
//            "<tr><th style = 'width:59px ;border-right: 1px solid #ddd; text-align: center'>Норма</th><th style='text-align: center'>Факт</th></tr></table>",
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => '<tr>vaxvfsad</tr>',
//        'attribute'=>'circulation',
//        'options' => ['style' => 'width: 60px; max-width: 60px;'],
//        'group' => true,
//    ],
//    [
//        'headerOptions' => [
//            'style' => 'display: none;',
//        ],
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => 'Тираж',
//        'attribute'=>'factcirculation',
//        'group' => true,
//        'contentOptions' => ['style' => 'width: 60px; max-width: 60px;'],
//    ],
//    [
//        'headerOptions' => [
//            'colspan' => '2',
//            'style' => 'padding:0px; width:1920px',
//        ],
//        'header' => "<table style = 'width: 192px;' ><tr style='border-bottom: 1px solid #ddd'><th colspan=2 style='text-align: center'>Период</th></tr>".
//            "<tr><th style = 'width:95px ;border-right: 1px solid #ddd; text-align: center'>Норма</th><th style='text-align: center'>Факт</th></tr></table>",
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'routeperiod',
//        'group' => true,
//        'options' => ['style' => 'width: 96px;'],
//    ],
   [
       'class'=>'\kartik\grid\DataColumn',
       'attribute'=>'routeperiod',
       'label' => 'Период',
       'contentOptions' => ['style' => 'width: 96px;'],
   ],
   [
       'class'=>'\kartik\grid\DataColumn',
       'attribute'=>'routeperiodfact',
       'label' => 'Период (Факт)',
       'contentOptions' => ['style' => 'width: 96px;'],
   ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'payment',
//        'group' => true,
//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
//        'group' => true,
        'attribute'=>'town',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'group' => true,
//        'attribute'=>'region',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
//        'group' => true,
        'label' => 'Промоутер',
        'attribute'=>'employees',
    ],
//    [
//        'group' => true,
//        'class'=>'\kartik\grid\DataColumn',
//        'label' => 'Зарплата',
//        'value' => function($model) {
//            $payment = 0;
//            $routesAddresses = \app\models\RouteAddress::find()->where(['route_id' => $model['id']])->all();
//
//            foreach ($routesAddresses as $address){
//                $payment += $address->paymentValue;
//            }
//
//            return $payment;
//        },
//        'format' => ['currency', 'rub'],
//        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'label' => 'Действия',
//        'value' => function($data){
//            return $data['route_id'];
//        },
//        'content' => function($data){
////            return $data['route_name'];
//            return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>',
//                    ['main-report/confirm-work2', 'route_id' => $data['route_id']], ['data-pjax' => '0', 'title' => 'Адреса']);
//        },
//        'format' => 'raw',
////        'group' => true,
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
//        'visible' => !Yii::$app->user->identity->isClient(),
        'template' => '{report}',
        'buttons' => [
            'report' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>',
                    ['main-report/confirm-work2', 'route_id' => $model['id']], ['data-pjax' => '0', 'title' => 'Адреса']);
            },
            // 'report2' => function($url, $model, $key) {     // render your custom button
            //     return Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-rub"></span></button>',
            //         ['payment', 'route_id' => $model['id']], ['data-pjax' => '0', 'title' => 'Зарплаты']);
            // },
        ],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(["main-report/{$action}",'id'=>$model['id']]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
    ],

];   