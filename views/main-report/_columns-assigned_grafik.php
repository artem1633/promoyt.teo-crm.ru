<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '#',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Клиент',
        'attribute'=>'clientname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '№ проекта',
        'attribute'=>'projectid',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Проект',
        'attribute'=>'projectname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Зона',
        'attribute'=>'zone_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Способ размещения',
        'attribute'=>'placementmethod',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Тираж',
        'attribute'=>'circulation',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Период работ',
        'attribute'=>'routeperiod',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'town',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'region',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Промоутер',
        'attribute'=>'employees',
        'visible' => !Yii::$app->user->identity->isClient(),
        'value' => function($model) {
            if ($model['employees']) {
                return $model['employees'];
            } else {
                return Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-search"></span>Назначить</button>',
                    ['assign-employee','route_id'=>$model['id']],['title'=>'Назначить','role'=>'modal-remote']);
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Отказ промоутера',
        'attribute'=>'comment_employees',
        'visible' => !Yii::$app->user->identity->isClient(),
        'value' => function($model) {
            if ($model['comment_employees']) {
                return Html::a('<button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span></button>',
                        ['main-report/comment-employee','route_id'=>$model['id']],['title'=>'Изменить','role'=>'modal-remote']).' '.$model['comment_employees'];
            } else {
                return Html::a('<button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Комментировать</button>',
                    ['main-report/comment-employee','route_id'=>$model['id']],['title'=>'Комментировать','role'=>'modal-remote']);
            }
        },
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Комментарий менеджера',
        'attribute'=>'comment_manager',
        'visible' => !Yii::$app->user->identity->isClient(),
        'value' => function($model) {
            if ($model['comment_manager']) {
                return Html::a('<button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span></button>',
                    ['main-report/comment-manager', 'route_id' => $model['id']], ['title' => 'Комментировать','role'=>'modal-remote']).' '.$model['comment_manager'];
            } elseif ($model['comment_employees']) {
                return Html::a('<button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Комментировать</button>',
                        ['main-report/comment-manager','route_id'=>$model['id']],['title'=>'Изменить','role'=>'modal-remote']);
            } else {
                return "";
            }
        },
        'format' => 'raw',
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'template' => '{view} {send} {new}',
//        'visible' => !Yii::$app->user->identity->isClient(),
        'buttons' => [
            'send' => function($url, $model, $key) {     // render your custom button
                if ($model['employees_id'] && $model['status'] == 0) {
                    return Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-phone"></span></button>',
                        ['send-employees','id'=>$model['id']],['data-pjax' => '0','title'=>'Адреса']);
                }

            },
            'new' => function($url, $model, $key) {     // render your custom button
                if ($model['comment_manager'] && $model['status'] == 3) {
                    return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-new-window"></span></button>',
                        ['duplicate', 'id' => $model['id']], ['data-pjax' => '0', 'title' => 'Адреса']);
                }
            },
        ],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$model['id']]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   