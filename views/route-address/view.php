<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RouteAddress */
?>
<div class="route-address-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address_id',
            'fine_id',
            'route_id',
            'fine_percent',
            'fine_comment',
            'plane_work',
            'fact_work',
            'confirm_work',
        ],
    ]) ?>

</div>
