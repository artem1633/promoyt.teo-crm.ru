<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RouteAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_id')->textInput() ?>

    <?= $form->field($model, 'fine_id')->textInput() ?>

    <?= $form->field($model, 'route_id')->textInput() ?>

    <?= $form->field($model, 'fine_percent')->textInput() ?>

    <?= $form->field($model, 'fine_comment')->textInput() ?>

    <?= $form->field($model, 'plane_work')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fact_work')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'confirm_work')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
