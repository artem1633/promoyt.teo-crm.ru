<?php
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RouteAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-address-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 10]) ?>

    <?php ActiveForm::end(); ?>
    
</div>
