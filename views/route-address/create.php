<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RouteAddress */

?>
<div class="route-address-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
