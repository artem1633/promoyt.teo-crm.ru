<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RouteTemplates */

?>
<div class="route-templates-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
