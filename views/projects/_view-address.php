<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isClient()){
    $content = '<div style="margin-top:10px;">' .
        Html::a('Обновить координаты', ['#'],
            ['title'=> 'Обновить координаты ', 'class'=>'btn btn-success', 'onclick' => '
                                event.preventDefault();
                                $.ajax({
                                    url: "'.Url::toRoute(['update-coordinates', 'id' => $model->id]).'",
                                    success: function(){
                                        $.pjax.reload("#crud-address-pjax");
                                    },
                                });
                            ', 'data-pjax' => '0']).
        Html::a('Экспорт', ['excel-export', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => 0]).
        '</div>';
} else {
    $content = '<div style="margin-top:10px;">' .
        Html::a('Обновить координаты', ['#'],
            ['title'=> 'Обновить координаты ', 'class'=>'btn btn-success', 'onclick' => '
                                event.preventDefault();
                                $.ajax({
                                    url: "'.Url::toRoute(['update-coordinates', 'id' => $model->id]).'",
                                    success: function(){
                                        $.pjax.reload("#crud-address-pjax");
                                    },
                                });
                            ', 'data-pjax' => '0']).
        Html::a('Экспорт', ['excel-export', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-pjax' => 0]).
        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-project-address','project_id'=>$model->id],
            ['title'=> 'Добавить', 'class'=>'btn btn-info', 'role' => 'modal-remote']).
        Html::a('Загрузить <i class="fa fa-excel"></i>', ['upload-excel-data','project_id'=>$model->id], [
            'role' => 'modal-remote',
            'title' => 'Загрузить',
            'class' => 'btn btn-default']).
        '</div>';
}

?>


<div class="projects-form ">
    <div class="row">
        <div class="col-md-12">
            <?=GridView::widget([
                'id'=>'crud-address',
                'dataProvider' => $dataProviderAddress,
                'filterModel' => $searchModelAddress,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => require(__DIR__ . '/_columns-address.php'),
                'toolbar'=> [
                    ['content'=> $content,
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список адресов ',
                    'before'=>'',
                    'after'=> Yii::$app->user->identity->isClient() ? null : BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["project-address-bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверенны?',
                                    'data-confirm-message'=>'Удалить выбранные элементы?'
                                ]),
                        ])."&nbsp".
                        Html::a('<i class="glyphicon glyphicon-trash"></i> Очистить', ['project-address-clear','project_id'=>$model->id],
                            ['title'=> 'Добавить', 'class'=>'btn btn-xs  btn-danger', 'role' => 'modal-remote' ,'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверенны?', 'data-confirm-message'=>'Очистить адресную базу?']).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
    </div>
</div>
