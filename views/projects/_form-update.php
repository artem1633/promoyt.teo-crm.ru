<?php

use app\models\Clients;
use app\models\PlacementMethod;
use app\models\Prices;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'client_id')->dropDownList(Clients::getClientList()) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'placements')->widget(MultipleInput::className(), [
                'columns' => [
                    [
                        'name'  => 'placement_id',
                        'title' => 'Способ доставки',
                        'type' => 'dropDownList',
                        'items' => PlacementMethod::getPlacementMethodList(),
                    ],
                    [
                        'name'  => 'price_id',
                        'title' => 'Тип цены',
                        'type' => 'dropDownList',
                        'items' => Prices::getPriceListFullName(),
                    ],
                    [
                        'name'  => 'circulation',
                        'title' => 'Тираж/К-во часов',
                        'defaultValue' => 1,

                    ],

                ],
                'min'               => 0, // should be at least 2 rows
                'allowEmptyList'    => false,
                'enableGuessTitle'  => true,
                'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
            ])->label(false);?>
        </div>
    </div>



  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
