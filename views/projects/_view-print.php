<div class="projects-form">
    <div class="row">
        <table class="table table-bordered " style="border: 1px solid black">
            <thead>
                <tr>
                    <th colspan="2" class="text-center" style="border: 1px solid black">
                        Маршрутный лист
                    </th>
                </tr>
            </thead>
            <tr>
                <td style="border: 1px solid black">
                    №/ФИО
                </td>
                <td style="border: 1px solid black">
                    Проект: <?=$model->name?>
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    План
                </td>
                <td style="border: 1px solid black">
                    Факт
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    Зона:
                </td>

                <td style="border: 1px solid black">
                    Зоны сделаны
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    Кол-во материалов (листовок) общее: <?=$model->normal?>
                </td>
                <td rowspan="2" style="border: 1px solid black">
                    Кол-во материалов (листовок) сделанное:
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    Кол-во домов: <?=$model->count_adress?><br>
                    Кол-во квартир: <?=$model->count_apartament?>
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    Кол-во фото общее <?=$model->count_entrance*4+$model->count_adress?><br>
                    Кол-во фото адресов <?=$model->count_adress?> <br>
                    Кол-во фото подъезда (снаружи/внутри) <?=($model->count_entrance*4+$model->count_adress)-$model->count_adress?>
                </td>
                <td style="border: 1px solid black">
                    Кол-во сделанных фото всего:
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="border: 1px solid black">
                    Условные обозначения: <br>
                    Кол-во сделанных листовок - поэтажная расклейка <br>
                    - не было распространения, обязательно указуем причину
                </td>
                <td style="border: 1px solid black">
                    Время начала работы:
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid black">
                    Время окончания работы:
                </td>
            </tr>
            <tr>
                <td width="50%" style="border: 1px solid black">
                    <?=$project->routeTemplates->template?>
                </td>
                <td width="50%" style="border: 1px solid black">
                    <br><br>
                    ОСТАТКИ кол-во _________________ подпись ________________
                    <br><br>
                    ОСТАТКИ принял _________________ подпись ________________

                </td>
            </tr>
        </table>
    </div>
</div>
