
<div class="projects-form">
    <div class="row">
        <table class="table table-bordered " cellpadding="0" cellspacing="0" style="border: 1px solid black">
            <thead>
                <tr>
                    <th class="text-center" style="border: 1px solid black">
                        Район
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Улица
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Дом
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Строение
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество подъездов
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество этажей
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество квартир
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Зона
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Фото
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        1п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        2п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        3п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        4п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        5п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        6п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        7п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        8п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        9п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        10п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        11п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        12п
                    </th>
                </tr>
            </thead>
            <tbody>
            <?php use app\models\RouteAddress;
            use app\models\Routes;
            use app\models\Zones;
            use app\services\Polygon;
            use yii\helpers\ArrayHelper;

            foreach ($address as $item) :?>
            <tr>
                <td style="border: 1px solid black">
                    <?=$model->region?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->street?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->house?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->housing?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->entrance?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->floor?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->apartament?>
                </td>
                <td style="border: 1px solid black">
                    <?=$model->zone->num_zone?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->entrance*4+1?>
                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
            </tr>
            <?php endforeach;?>
            </tbody>

        </table>
    </div>
</div>

<?php
$centermap = \app\models\RouteAddress::getCenterRouteAddress($model->id);
$centerX = $centermap['centerX'];
$centerY = $centermap['centerY'];

$route_id = $model->id;

$route = $model;
$routeAddres = ArrayHelper::map(RouteAddress::find()->where(['route_id'=>$route_id])->asArray()->all(),'id','address_id');
$coords = Zones::find()->where(['id'=>$route->zone_id])->one();
$coords = unserialize($coords->coorarr);
$polygon = Polygon::factory($coords[0]);
$address = $polygon->getObjects();
foreach ($address as $key => $addr){
    if (in_array($addr['id'],$routeAddres)) {
        $address[$key]['inroute'] = true;
    } else {
        $address[$key]['inroute'] = false;
    }
}

$points = [];

for ($i = 0; $i < count($address); $i++)
{
    $points[] = [
        $address[$i]['coord_y'],
        $address[$i]['coord_x'],
        'pm2',
        'bl',
        'm',
        '',
    ];
}

//$points = [
//    [
//        '37.756334625131',
//        '55.709347446395',
//        'pm2',
//        'bl',
//        'm',
//        ''
//    ],
//    [
//        '37.756334625131',
//        '55.709347446395',
//        'pm2',
//        'bl',
//        'm',
//        ''
//    ],
//];
$pointsStr = [];

foreach ($points as $point){
    $pointsStr[] = "{$point[0]},{$point[1]},{$point[2]}{$point[3]}{$point[4]}";
}

$points = 'pt='.implode('~', $pointsStr);


?>


<img src="https://static-maps.yandex.ru/1.x/?ll=<?=$centerY?>,<?=$centerX?>&size=500,450&spn=0.005,0.005&l=map&<?=$points?>" alt="">
<img src="https://static-maps.yandex.ru/1.x/?ll=<?=$centerY?>,<?=$centerX?>&size=500,450&spn=0.006,0.006&l=map&<?=$points?>" alt="">