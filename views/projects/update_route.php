<?php


/* @var $this yii\web\View */
/* @var $model app\models\Routes */

?>
<div class="routes-create">
    <?= $this->render('_form_route', [
        'model' => $model,
    ]) ?>
</div>
