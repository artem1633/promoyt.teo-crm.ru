<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isClient()){
    $content = '';
} else {
    $content = '<div style="margin-top:10px;">' .
        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-zone', 'project_id' => $model->id],
            ['title' => 'Добавить', 'class' => 'btn btn-info', 'role' => 'modal-remote']) .
        '</div>';
}

?>

<div class="projects-form">
    <div class="row">
        <ul class="nav pull-right">
            <a href="#" id="addZone" class="btn btn-info"
               onclick='PlaceAddressNoZone();'>Объекты вне зоны</a>
        </ul>
        <ul class="nav pull-right">
            <a href="#" id="addZone" class="btn btn-info"
               onclick='PlaceAddressList();'>Объекты в зоне</a>
        </ul>
        <?php if(Yii::$app->user->identity->isClient() == false): ?>
            <ul class="nav pull-right">
                <a href="#" id="addZone" class="btn btn-info"
                   onclick='needadd=4;AreaLineAdd();myMap.cursors.push("crosshair");'> Добавить зону </a>
            </ul>
            <ul class="nav pull-right">
                <a href="#" id="zoneEdit" class="btn btn-info"> Сохранить зону </a>
            </ul>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= GridView::widget([
                'id' => 'crud-zones',
                'options' => ['style' => 'font-size:12px;'],
                'dataProvider' => $dataProviderZone,
                'filterModel' => $searchModelZone,
                'pjax' => true,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'num_zone',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'name',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'objects_count',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'count_entrance',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'count_floor',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'count_apartament',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'template' => '{update}{delete}',
                        'visible' => Yii::$app->user->identity->isClient() == false,
                        'vAlign' => 'middle',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return Url::to(['zone-' . $action, 'zone_id' => $key]);
                        },

                        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
                        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
                        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
                            'data-confirm' => false, 'data-method' => false,// for overide yii data api
                            'data-request-method' => 'post',
                            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                            'data-toggle' => 'tooltip',
                            'data-confirm-title' => 'Вы уверенны?',
                            'data-confirm-message' => 'Вы действительно хотите удалить запись '],
                    ],

                ],
                'toolbar' => [
                    ['content' => $content,
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список зон ',
                    'before' => '',
                    'after' => Yii::$app->user->identity->isClient() ? null : BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"],
                                [
                                    "class" => "btn btn-danger btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Вы уверенны?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить запись '
                                ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ]) ?>
        </div>
        <div class="col-md-9">
            <div id="map"></div>
        </div>

    </div>

</div>

<style>
    #map {
        width: 100%;
        height: 600px;
        padding: 0;
        margin: 0;
    }
</style>
<style>
    .YMaps-layer-container img {
        max-width: none;
    }
</style>
<script>
    let cured = 'null';
    let needadd = 'null';
    let type_layout = 1;
    let billing_id = 1;
    let projectId = <?=$model->id?>;
</script>

<?php Modal::begin([
    "id" => "progectZoneModal",
    "options" => [
        "tabindex" => -1,
        "display:none",
    ],
    "header" => "<h3>Зона</h3>",
    "footer" => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"]),
]) ?>
<?php Modal::end(); ?>

<?php Modal::begin([
    "id" => "progectAddZoneModal",
    "options" => [
        "tabindex" => -1,
    ],

    "header" => "<h3>Зона</h3>",
    "footer" => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit", 'onclick' => "sendAjaxForm()"]),
]) ?>
<div class="zones-form">
    <form id="w0-add" >
        <div class="form-group field-zones-num_zone">
            <label class="control-label" for="zones-num_zone">№ зоны</label>
            <input type="text" id="add-zones-num_zone" class="form-control" name="Zones[num_zone]" value="">
            <div class="help-block"></div>
        </div>
        <div class="form-group field-zones-name">
            <label class="control-label" for="zones-name">Название</label>
            <input type="text" id="add-zones-name" class="form-control" name="Zones[name]" value="" maxlength="60">

            <div class="help-block"></div>
        </div>    </form></div>
<?php Modal::end(); ?>
<?php
$script = <<<JS
    var project_id = $model->id,
    center_map = [55.750625, 37.626];
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("/js/project_map.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>


