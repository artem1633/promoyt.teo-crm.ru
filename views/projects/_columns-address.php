<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'id',
     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'project_id',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address_town',
        'label' => 'Город'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address_region',
        'label' => 'Район',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address_street',
        'label' => 'Улица',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address_house',
        'label' => 'Дом',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address_housing',
        'label' => 'Строение'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'entrance',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apartament',
    ],
     [
     'class' => '\kartik\grid\DataColumn',
     'attribute' => 'floor',
     ],
     [
     'class' => '\kartik\grid\DataColumn',
     'attribute' => 'porter',
     ],
     [
     'class' => '\kartik\grid\DataColumn',
     'attribute' => 'type',
         'value'=>function ($model) {
             switch ($model->type) {
                 case 0 : return "<i class='fa fa-building'></i>";
                 case 1 : return "<i class='fa fa-building-o'></i>";
                 case 2 : return "<i class='fa fa-bank'></i>";
             }


         },
         'format'=>'raw',
     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Зона',
        'attribute' => 'zone_name',
        'value' => 'zone.name',
    ],

     [
     'class' => '\kartik\grid\DataColumn',
     'attribute' => 'comment',
     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'coord_x',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'coord_y',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update}{delete}',
        'visible' => !Yii::$app->user->identity->isClient(),
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['project-address-'.$action,'projectaddress_id'=>$model['id']]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
    ],

];   