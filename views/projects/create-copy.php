<?php

use app\models\Clients;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Projects */

?>
<div class="projects-create">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_id')->dropDownList(Clients::getClientList()) ?>

    <?php if($model->isNewRecord): ?>
        <?= $form->field($model, 'intervalDates')->widget(DateRangePicker::classname(), [
        ]) ?>
    <?php else: ?>
        <?= $form->field($model, 'intervalDates')->widget(DateRangePicker::classname(), [
            'pluginOptions' => [
                'startDate' => date('d.m.Y', strtotime($model->date_start)),
                'endDate' => date('d.m.Y', strtotime($model->date_end)),
            ],
        ]) ?>
    <?php endif; ?>

    <?= $form->field($frm, 'placementMethod')->checkbox() ?>
    <?= $form->field($frm, 'addresslist')->checkbox() ?>
    <?= $form->field($frm, 'zones')->checkbox() ?>
    <?= $form->field($frm, 'routes')->checkbox() ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>
