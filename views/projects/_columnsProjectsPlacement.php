<?php
use app\models\PlacementMethod;
use app\models\Prices;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'value'=>'placement.name',
        'attribute'=>'placement_id',
        'filter' => PlacementMethod::getPlacementMethodList(),
    ],
    [
        'attribute'  => 'price_id',
        'label' => 'Тип цены',
        'value' => function($model) {
            return Prices::getPriceListFullName()[$model->price_id];
        },
        'filter' => Prices::getPriceListFullName(),
    ],
    [
        'attribute'  => 'circulation',
        'label' => 'Тираж/К-во часов',
        'pageSummary' => true,
        'hAlign' => 'right',
    ],
    [
        'label' => 'Сумма',
        'format' => 'decimal',
        'value' => function($model) {
            return $model->circulation * $model->price;
        },
        'pageSummary' => true,
        'hAlign' => 'right',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'header' =>"Действия",
        'template'=> '{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to(['placement-'.$action,'id'=>$key,'project_id'=>$model->project_id]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','data-pjax'=>'0'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   