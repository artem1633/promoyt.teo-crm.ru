<?php

use app\services\DateUtil;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'value' => function($model) {
            if (strtotime($model->date_start)>time()) {
                return "Планируется";
            } elseif (strtotime($model->date_end)>time()) {
                return "Текущий";
            } else {
                return "Завершенный";
            }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'value' => 'client.name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Год',
        'value' => function($model) {
            return date("Y",strtotime($model->date_start));
        },

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Месяц',
        'value' => function($model) {
            return DateUtil::getMonth(date("n",strtotime($model->date_start)));
        },

    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_start',
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_end',
        'format' => ['date', 'php:d.m.Y'],
    ],
    [ 
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'header' =>"Действия",
        'template' => Yii::$app->user->identity->isClient() ? '{view}' : '{copy} {view} {delete}',  // the default buttons + your custom button
        'buttons' => [
            'copy' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-copy"></span></button>',
                    ['copy','project_id'=>$model->id],['title'=>'Копировать','role'=>'modal-remote']);
            }
        ],
        'dropdown' => false,
        'vAlign'=>'middle',

        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','data-pjax'=>'0'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   