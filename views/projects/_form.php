<?php

use app\models\Clients;
use app\models\RouteTemplates;
use kartik\widgets\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->prices = ArrayHelper::getColumn(\app\models\PricesProjects::find()->where(['projects_id' => $model->id])->all(), 'prices_id');
}

?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_id')->dropDownList(Clients::getClientList()) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'prices')->widget(\kartik\select2\Select2::className(), [
                        'data' => ArrayHelper::map(\app\models\Prices::find()->all(), 'id', 'name'),
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',']
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'date_start')->textInput(['type' => 'date']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'date_end')->textInput(['type' => 'date']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'normal_one')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'route_template_id')->dropDownList(RouteTemplates::getTemplates()) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'maket')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
            <?= $form->field($model, 'maketFile')->//fileInput()->label('Выберите файл для импорта');
            widget(FileInput::classname(), [
                'options'=>[
                    'multiple'=>false,
                    'accept' => 'image/*.pdf',
                ],
                'pluginOptions' => [
                    'overwriteInitial'=>true,
                    'showUpload' => false,
                    'browseLabel' => '',
                    'removeLabel' => '',
                    'maxFileSize'=>2800,
                    'initialPreview' => [
                        "<img src='" . $model->maket_file . "' class='kv-file-content additional_photos file-preview-image'>"
                    ],
                ]
            ])->label(false);
            ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'info_add')->textarea();?>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
