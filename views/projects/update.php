<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
?>
<div class="projects-update">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">Общая информация</span>
                <span class="hidden-xs">Общая информация</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">Маршруты</span>
                <span class="hidden-xs">Маршруты</span>
            </a>
        </li>
        <li class="">
            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">График</span>
                <span class="hidden-xs">График</span>
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1">
            <div class="row">
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'base-pjax']) ?>
                <div class="col-md-7 col-sm-12">
                    <h3>
                        Общие элементы
                        <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>

                        <a class="btn btn-warning pull-right" data-pjax="0" href="<?=Url::toRoute(['set-lid', 'id' => $model->id])?>">Перевести клиент в лида</a>

                        <div class="btn-group">
                            <button type="button" class="btn btn-success"> <b><i class="fa fa-print"></i> </b></button>
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu">
                                <?php
                                $templates = Templates::find()->where(['project_id' => $model->project_id])->all();
                                foreach ($templates as $value) { ?>
                                    <li><?= Html::a( $value->name, ['/clients/print', 'template_id' => $value->id, 'client_id' => $model->id ], ['data-pjax'=>'0','title'=>'', 'target' => '_blank', 'data-toggle'=>'tooltip'])?></li>
                                <?php } ?>
                            </ul>

                        </div>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-hint2">
                            <i class="fa fa-question"></i>
                        </button>
                    </h3>
                    <div style="margin-top: 10px" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('fio')?></b></td>
                                <td><?=Html::encode($model->fio)?></td>
                                <td><b><?=$model->getAttributeLabel('date_cr')?></b></td>
                                <td><?=\Yii::$app->formatter->asDate($model->date_cr, 'php:d.m.Y')?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('city')?></b></td>
                                <td><?=Html::encode($model->city)?></td>
                                <td><b><?=$model->getAttributeLabel('project_id')?></b></td>
                                <td><?= $model->project->name?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('planned_date')?></b></td>
                                <td><?=Html::encode($model->planned_date)?></td>
                                <td><b><?=$model->getAttributeLabel('menejer')?></b></td>
                                <td><?= $model->menejer0->fio?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('status')?></b></td>
                                <td><?=Html::encode($model->status0->name)?></td>
                                <td><b><?=$model->getAttributeLabel('client')?></b></td>
                                <td><?= $model->client == 1 ? 'Да' : 'Нет'?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('source_id')?></b></td>
                                <td><?= $model->source->name?></td>
                                <td><b><?=$model->getAttributeLabel('utm')?></b></td>
                                <td><?= $model->utm?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('ip')?></b></td>
                                <td><?=$model->link?></td>
                                <td><b><?=$model->getAttributeLabel('link')?></b></td>
                                <td><?=$model->ip?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('comment')?></b></td>
                                <td colspan="3"><?=Html::encode($model->comment)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <h3>Контакты <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['/contacts/add', 'client_id' => $model->id])?>"><i class="fa fa-plus"></i></a>
                    </h3>
                    <?=GridView::widget([
                        'dataProvider' => $contactsdataProvider,
                        //'filterModel' => $contactssearchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_contacts_columns.php'),
                        'panelBeforeTemplate' => '',
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'showPageSummary' => false,
                    ])?>

                    <?php if ($model->id_send) :?>
                        <?php $myHistory = json_decode(file_get_contents("https://app.vkrassilka.ru/api/client/chatsls?id={$model->id_send}"));?>
                        <div>
                            <!-- DIRECT CHAT -->
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                        <?= Html::a('Переейти в диалог', 'https://app.vkrassilka.ru/dispatch-status/dialog?id='.$model->id_send, [ 'target' => '_blank',  'data-pjax'=>0]) ?>
                                    </h3>

                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <!-- Conversations are loaded here -->
                                    <div class="direct-chat-messages">

                                        <?php foreach ($myHistory->history as $msg):?>
                                            <!-- Message. Default to the left -->
                                            <?php if($msg->from != 'client'):?>
                                                <div class="direct-chat-msg">
                                                    <div class="direct-chat-info clearfix">
                                                        <div class="direct-chat-text">
                                                            <?= $msg->text ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php else:?>
                                                <div class="direct-chat-msg right">
                                                    <div class="direct-chat-text">
                                                        <?= $msg->text ?>
                                                    </div>
                                                </div>
                                            <?php endif;?>

                                        <?php endforeach;?>
                                    </div>
                                    <!--/.direct-chat-messages-->

                                </div>
                                <!-- /.box-body -->
                                <!--/.direct-chat -->
                            </div>
                        </div>

                    <?php endif; ?>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <div class="row">

                <?php Pjax::begin(['enablePushState' => false, 'id' => 'cash-pjax']) ?>
                <div class="col-md-11" style="margin-left: 40px;">
                    <h3>Добавить
                        <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['add-cash', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                    </h3>
                    <br>
                    <?php $pays =\app\models\Pays::find()->where(['client_id' => $model->id])->all();
                    if($pays == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет оплаты </b></center></span>';}
                    else {
                        ?>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th>Дата/Время</th>
                                <th>Сумма</th>
                                <th>Комментария</th>
                            </tr>
                            <?php
                            foreach ($pays as $change) {
                                $username = Users::findOne($change->user_id);
                                ?>
                                <tr>
                                    <td><?=\Yii::$app->formatter->asDate($change->data, 'php:H:i, d.m.Y')?></td>
                                    <td><?=$change->summa?></td>
                                    <td><?=$change->comment?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div>
        <div class="tab-pane fade" id="default-tab-3">
            <div class="row">
                <?php Pjax::begin(['enablePushState' => false, 'id' => 'three-pjax']) ?>
                <div class="col-md-11">
                    <h3>Создать / Изменить анкету
                        <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['/clients-field/add', 'client_id' => $model->id ])?>"><i class="fa fa-pencil"></i></a>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-hint1">
                            <i class="fa fa-question"></i>
                        </button>
                    </h3>
                    <br>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th>№</th>
                            <th>Наименование</th>
                            <th>Значение</th>
                        </tr>
                        <?php
                        $i = 0;
                        foreach ($fielddataProvider->getModels() as $field) {
                            $i++;
                            ?>
                            <tr>
                                <td><?=$i?></td>
                                <td><?=$field->field->label?></td>
                                <td><?=$field->value?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div>

    </div>
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>

</div>
