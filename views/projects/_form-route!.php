<?php

use app\models\PlacementMethod;
use app\models\Prices;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'placement_id')->dropDownList(PlacementMethod::getPlacementMethodList()) ?>
    <?= $form->field($model, 'price_id')->dropDownList(Prices::getPriceListFullName()) ?>
    <?= $form->field($model, 'circulation')->textInput()?>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
</div>
