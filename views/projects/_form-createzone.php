<?php

use yii\widgets\ActiveForm;

?>


<div class="zones-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'num_zone')->textInput() ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php ActiveForm::end(); ?>
</div>