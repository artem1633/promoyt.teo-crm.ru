<?php

use app\models\PlacementMethod;
use app\models\Prices;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;

CrudAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
$this->title = 'Проект: '.$model->name;

$this->params['breadcrumbs'][] = ['label' => 'Список проектов','url'=>['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="projects-view">


        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs">Общая информация</span>
                    <span class="hidden-xs">Общая информация</span>
                </a>
            </li>
            <li class="">
                <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs">Карта</span>
                    <span class="hidden-xs">Карта</span>
                </a>
            </li>
            <li class="">
                <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs">Адресная программа</span>
                    <span class="hidden-xs">Адресная программа</span>
                </a>
            </li>
            <li class="">
                <a href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs">Маршруты</span>
                    <span class="hidden-xs">Маршруты</span>
                </a>
            </li>
            <li class="">
                <a href="#default-tab-5" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs">График</span>
                    <span class="hidden-xs">График</span>
                </a>
            </li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="default-tab-1">
                <?= $this->render('_view-main', [
                    'model' => $model,
                    'searchModelProjectsPlacement' => $searchModelProjectsPlacement,
                    'dataProviderProjectsPlacement' => $dataProviderProjectsPlacement,
                ]) ?>
            </div>
            <div class="tab-pane fade" id="default-tab-2">
                <?= $this->render('_view-map', [
                    'model' => $model,
                    'searchModelZone' => $searchModelZone,
                    'dataProviderZone' => $dataProviderZone,

                ]) ?>
            </div>
            <div class="tab-pane fade" id="default-tab-3">
                <?= $this->render('_view-address', [
                    'model' => $model,
                    'searchModelAddress' => $searchModelAddress,
                    'dataProviderAddress' => $dataProviderAddress,

                ]) ?>
            </div>
            <div class="tab-pane fade" id="default-tab-4">
                <?= $this->render('_view-route', [
                    'model' => $model,
                    'searchModelRoute' => $searchModelRoute,
                    'dataProviderRoute' => $dataProviderRoute,

                ]) ?>
            </div>
            <div class="tab-pane fade" id="default-tab-5">
                <?= \app\widgets\WorkSchedule::widget(['project' => $model]) ?>
            </div>

        </div>


</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>