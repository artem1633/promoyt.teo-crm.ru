<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\RoutesAddressList */
/* @var $form yii\widgets\ActiveForm */

//$model->placementMethodsField = \yii\helpers\ArrayHelper::getColumn($model->getRoutesPlacementMethods()->all(), 'placement_method_id');

$model->fines = \yii\helpers\ArrayHelper::getColumn($model->getFinesRoutesAddressLists()->all(), 'fines_id');
$model->placementMethods = \yii\helpers\ArrayHelper::getColumn($model->getPlacementMethodRoutesAddressLists()->all(), 'placement_method_id');

?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_list_id')->widget(Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AddressList::find()->all(), 'id', 'fullAddress'),
        'pluginOptions' => [
            'placeholder' => 'Выберите улицу',
            'allowClear' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'fines')->widget(Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Fines::find()->all(), 'id', 'name'),
        'pluginOptions' => [
            'placeholder' => 'Выберите штрафы',
            'allowClear' => true,
            'multiple' => true
        ],
    ]) ?>

    <?= $form->field($model, 'placementMethods')->widget(\kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\PlacementMethod::find()->all(), 'id', 'name'),
        'pluginOptions' => [
            'placeholder' => 'Выберите типы',
            'allowClear' => true,
            'multiple' => true
        ],
    ]) ?>

    <?= $form->field($model, 'completed_edition')->input('number') ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>

    <div class="hidden">
        <?= $form->field($model, 'routes_id')->hiddenInput() ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
