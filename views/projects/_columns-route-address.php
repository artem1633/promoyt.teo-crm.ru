<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

$fines = \yii\helpers\ArrayHelper::map(\app\models\Fines::find()->all(), 'id', 'name');

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.street',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.house',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address.housing',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Етажей',
        'attribute'=>'address.floor',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Квартир',
        'attribute'=>'address.apartament',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Подьездов',
        'attribute'=>'address.entrance',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Тип',
        'attribute'=>'address.type',
        'value'=>function ($model) {
            switch ($model->address->type) {
                case 0 : return "<i class='fa fa-building'></i>";
                case 1 : return "<i class='fa fa-building-o'></i>";
                case 2 : return "<i class='fa fa-bank'></i>";
            }


        },
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'План.дата',
        'attribute'=>'plane_date',
    ],

    /*[
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Штраф',
        'attribute'=>'fine_id',
        'content' => function($model) use($fines) {
            return \kartik\editable\Editable::widget([
                'model' => $model,
                'formOptions' => [
                    'action' => Url::to(['index', 'id' => $model->id, 'attribute' => 'fine_id']),
                ],
                'attribute' => 'fine_id',
                'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                'options' => [
                    'value' => $model->fine->name,
                    'data' => $fines,
                    'pluginOptions'=>['allowClear'=>true],
                ],
            ]);
        },
    ],*/
//    [
//        'class'=>'\kartik\grid\EditableColumn',
//        'attribute'=>'completed_edition',
//        'content' => function($model) use($fines) {
//            return \kartik\editable\Editable::widget([
//                'model' => $model,
//                'attribute' => 'completed_edition',
//                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
//                'options' => [
//                    'value' => $model->completed_edition,
//                    'data' => $fines,
//                    'pluginOptions'=>['allowClear'=>true],
//                ],
//                'formOptions' => [
//                    'action' => Url::to(['index', 'id' => $model->id, 'attribute' => 'completed_edition']),
//                ],
//            ]);
//        },
//        'editableOptions' => [
//            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
//            'formOptions' => [
//                    'action' => Url::to(['index', 'id' => $model->id, 'attribute' => 'completed_edition']),
//                ],
//        ],
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'type_work',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'addressList.plane_work',
//        'value' => function($model){
//            return $model->plane_work  ? $model->plane_work .  Html::a('Изменить',
//                     ['set-norm','id'=>$model->id],['role'=>'modal-remote','class'=>'pull-right']): Html::a('Проставить',
//                 ['set-norm','id'=>$model->id],['role'=>'modal-remote']);
//        },
//        'format'=>'raw',
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'template' => '{edit-addressList} {delete}',  // the default buttons + your custom button"
        'visible' => !Yii::$app->user->identity->isClient(),
        'buttons' => [

            'edit-addressList' => function($url, $model, $key) {     // render your custom button
                    return Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
                        ['/projects/update-address','address_id'=>$model->id],['role'=>'modal-remote']);
            },
            'delete' => function($url, $model, $key) {
                return Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                    ['/projects/delete-address','address_id'=>$model->id],['role'=>'modal-remote']);
            }
        ],
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   