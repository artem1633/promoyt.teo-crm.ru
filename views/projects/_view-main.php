<?php

use app\models\Clients;
use app\models\RouteAddress;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isClient()){
    $content = '';
} else {
    $content = '<div style="margin-top:10px;">' .
        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['placement-create','project_id'=>$model->id],
            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
        '</div>';
}

?>
<style>
    #ajaxCrudModal .modal-dialog{
        width: 800px;
    }
</style>

<div class="projects-form">
    <div class="row">

        <div class="<?= Yii::$app->user->identity->isClient() ? 'col-md-12' : 'col-md-4' ?>">
            <?php Pjax::begin(['id'=>'progectMainModal'])?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Основная информация </h3>
                </div>
                <div class="panel-body">
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label"><?=$model->getAttributeLabel('id')?></span>
                            <span class="emp-value"><?=$model->id?></span>
                        </div>
                    </div>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label">Статус</span>
                            <span class="emp-value"><?php
                                if (strtotime($model->date_start)>time()) {
                                    echo "Планируется";
                                } elseif (strtotime($model->date_end)>time()) {
                                    echo "Текущий";
                                } else {
                                    echo "Завершенный";
                                }
                                ?></span>
                        </div>
                    </div>

                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label"><?=$model->getAttributeLabel('name')?></span>
                            <span class="emp-value"><?=$model->name?></span>
                        </div>
                    </div>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label"><?=$model->getAttributeLabel('client_id')?></span>
                            <span class="emp-value"><?=Clients::getClientList()[$model->client_id]?></span>
                        </div>
                    </div>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label">Период размещения</span>
                            <span class="emp-value"><?=$model->date_start.' по '.$model->date_end?></span>
                        </div>
                    </div>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label">Тираж факт</span>
                            <span class="emp-value">
                                <?php
                                    $routesPks = ArrayHelper::getColumn(\app\models\Routes::find()->where(['project_id' => $model->id])->all(), 'id');
                                    $confirmWorkSum = RouteAddress::find()->where(['route_id' => $routesPks])->sum('confirm_work');
                                    echo $confirmWorkSum;
                                ?>
                            </span>
                        </div>
                    </div>
                    <?php if(Yii::$app->user->identity->isClient() == false): ?>
                        <div class="row oglavl">
                            <div class="col-md-12">
                                <span class="emp-label">Норма на одного промоутера</span>
                                <span class="emp-value"><?=$model->normal_one?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label">Макет</span>
                            <span class="emp-value"><?=$model->maket?></span>
                        </div>
                    </div>
                    <div class="row oglavl">
                        <div class="col-md-12">
                            <span class="emp-label">Файл макета</span>
                            <span class="emp-value"><?=Html::a($model->maket_file,[$model->maket_file],['download'=>true,'data-pjax'=>'0','target'=>'_blank'])?></span>
                        </div>
                    </div>
                    <?php if(Yii::$app->user->identity->isClient() == false): ?>
                        <div class="row oglavl">
                            <div class="col-md-12">
                                <span class="emp-label">Шаблон маршрутного листа</span>
                                <span class="emp-value"><?=$model->routeTemplates->name?></span>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <strong class="">Дополнительная информация: </strong>
                                <span ><?=$model->info_add?></span>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
                <?php if(Yii::$app->user->identity->isClient() == false): ?>
                    <div class="form-group">
                        <?= Html::a('Изменить',['update','id'=>$model->id], ['class' => 'btn btn-info','role'=>'modal-remote']) ?>
                    </div>
                <?php endif; ?>
            <?php Pjax::end()?>

        </div>


        <?php if(Yii::$app->user->identity->isClient() == false): ?>
            <div class="col-md-8">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProviderProjectsPlacement,
                        'filterModel' => $searchModelProjectsPlacement,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columnsProjectsPlacement.php'),
                        'showPageSummary' => true,
                        'toolbar'=> [
                            ['content'=> $content,
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Список цен и количества работ ',
                            'before'=>'',
                            'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                        ["placement-bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Are you sure?',
                                            'data-confirm-message'=>'Are you sure want to delete this item'
                                        ]),
                                ]).
                                '<div class="clearfix"></div>',
                        ]
                    ])?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"progectMainModal",
    "options" => [
        "tabindex" => -1,

    ],
    'size'=>'modal-lg',
    "header"=>"<h3>Основная информация</h3>",
    "footer"=>Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit",'onclick'=>"sendAjaxForm()"]),
])?>
<?php Modal::end(); ?>

