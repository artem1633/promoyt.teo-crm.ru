<?php

use app\models\PlacementMethod;
use app\models\Zones;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Routes;

/* @var $this yii\web\View */
/* @var $model app\models\Routes */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'town')->textInput() ?>
    <?= $form->field($model, 'region')->textInput() ?>
    <?= $form->field($model, 'placement_id')->dropDownList(PlacementMethod::getPlacementMethodList()) ?>
    <?= $form->field($model, 'maket')->textInput() ?>
    <?= $form->field($model, 'normal')->textInput() ?>
    <?= $form->field($model, 'status_route')->dropDownList($model->getStatuses()) ?>
    <?= $form->field($model, 'zone_id')->dropDownList(Zones::getZoneByProject($model->project_id)) ?>
    <?= $form->field($model, 'comment_manager')->textarea(['rows' => 3]) ?>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<style>
    .routes-form label {
        font-size: 12px;
    }
</style>