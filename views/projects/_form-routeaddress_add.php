<?php

use app\models\Zones;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Routes */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_id')->widget(Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AddressList::find()->all(), 'id', 'fullAddress'),
        'pluginOptions' => [
            'placeholder' => 'Выберите объект',
            'allowClear' => true,
        ],
    ]) ?>
    <?= $form->field($model, 'plane_date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
        'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
        ]
    ]); ?>
    <?= $form->field($model, 'comment')->textInput() ?>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<style>
    .routes-form label {
        font-size: 12px;
    }
</style>