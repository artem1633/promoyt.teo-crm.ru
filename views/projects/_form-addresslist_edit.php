<?php

use app\models\AddressList;
use app\models\Zones;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\RoutesAddressList */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'entrance')->input('number') ?></div>
        <div class="col-md-6"><?= $form->field($model, 'floor')->input('number') ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'apartament')->input('number') ?></div>
        <div class="col-md-6"><?= $form->field($model, 'porter')->input('number') ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?=$form->field($model, 'type')->dropDownList(AddressList::getTypes()) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'zone_id')->dropDownList(Zones::getZoneByProject($model->project_id)) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?></div>
    </div>






    <?php ActiveForm::end(); ?>

</div>
