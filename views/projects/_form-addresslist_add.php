<?php

use app\models\AddressList;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\RoutesAddressList */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="routes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_id')->widget(Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AddressList::find()->all(), 'id', 'fullAddress'),
        'pluginOptions' => [
            'placeholder' => 'Выберите объект',
            'allowClear' => true,
        ],
    ]) ?>
    <?php ActiveForm::end(); ?>

</div>
