<?php


use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
CrudAsset::register($this);
$this->title = 'Маршрут: '.$model->name;

$this->params['breadcrumbs'][] = ['label' => 'Список проектов','url'=>['index']];
$this->params['breadcrumbs'][] = ['label' => 'Проект: '.$model->project->name,'url'=>['view','id'=>$model->project_id]];
$this->params['breadcrumbs'][] = ['label' => $this->title];

if(Yii::$app->user->identity->isClient()){
    $content = '';
} else {
    $content = '<div style="margin-top:10px;">' .
        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-address','route_id'=>$model->id],
            ['title'=> 'Добавить', 'class'=>'btn btn-info', 'role' => 'modal-remote']).
        '</div>';
}

?>

<div class="routes-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'options' => ['style' => 'font-size:12px;'],
                'dataProvider' => $dataProviderAddress,
                'filterModel' => $searchModelAddress,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => require(__DIR__ . '/_columns-route-address.php'),
                'toolbar'=> [
                    ['content'=>
                        $content
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список адресов ',
                    'before'=>'',
                    'after'=>Yii::$app->user->identity->isClient() ? null : BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
        <div class="col-md-4">
            <div id="map"></div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'size'=>'modal-lg',
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;" type="text/javascript"></script>
<style>
    #map {
        width: 100%; height: 600px; padding: 0; margin: 0;
    }
</style>
<style>
    .YMaps-layer-container img{
        max-width: none;
    }
</style>
<?php
$centermap = \app\models\RouteAddress::getCenterRouteAddress($model->id);
$centerX = $centermap['centerX'];
$centerY = $centermap['centerY'];


$script = <<<JS
//var employeeId = $model->id,
var center_map = [$centerX,$centerY];
route_id = $model->id;
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("/js/route_map.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>