<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isClient()){
    $content = '';
} else {
    $content = '<div style="margin-top:10px;">' .
        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['add-route','project_id'=>$model->id],
            ['title'=> 'Добавить', 'class'=>'btn btn-info', 'role' => 'modal-remote']).
        '</div>';
}

?>

<div class="projects-form">
    <div class="row">
        <div class="col-md-12">
            <?=GridView::widget([
                'id'=>'crud-routes',
                'dataProvider' => $dataProviderRoute,
                'filterModel' => $searchModelRoute,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'columns' => require(__DIR__ . '/_columns-route.php'),
                'toolbar'=> [
                    ['content'=>$content
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i>Список адресов ',
                    'before'=>'',
                    'after'=>Yii::$app->user->identity->isClient() ? null : BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
    </div>
</div>
