<?php

use app\models\Clients;
use app\models\PlacementMethod;
use app\models\Prices;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">


    <div class="row">

        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-4">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'client_id')->dropDownList(Clients::getClientList()) ?>
        </div>
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>
        <div class="col-md-8">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProviderProjectsPlacement,
                    'filterModel' => $searchModelProjectsPlacement,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columnsProjectsPlacement.php'),
                    'showPageSummary' => true,
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['placement-create'],
                                ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '</div>'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список цен и количества работ ',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                    ["bulk-delete"] ,
                                    [
                                        "class"=>"btn btn-danger btn-xs",
                                        'role'=>'modal-remote-bulk',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Are you sure?',
                                        'data-confirm-message'=>'Are you sure want to delete this item'
                                    ]),
                            ]).
                            '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>
