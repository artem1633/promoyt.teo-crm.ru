<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
        //Только для разработки
        $dev_item = [
            'label' => 'Разработка',
            'items' => [
                ['label' => 'Логи "test"', 'url' => ['/site/get-log', 'name' => 'test']],
                ['label' => 'Логи "_api_error"', 'url' => ['/site/get-log', 'name' => 'api']],
                ['label' => 'Логи приложения', 'url' => ['/site/get-log', 'name' => 'app']],
                ['label' => 'TEST', 'url' => ['/projects/test']],
                ['label' => 'PHP info', 'url' => ['/site/php-info']],
            ],
            'visible' => Yii::$app->user->identity->role != \app\models\Users::USER_TYPE_CLIENT,
        ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Проекты', 'url' => ['/projects/index']],
            ['label' => 'Онлайн карта', 'url' => ['/online-map/index']],
            ['label' => 'Сводный отчет', 'url' => ['/main-report/index']],
            [
                'label' => 'Справочники',
                'visible' => Yii::$app->user->identity->role != \app\models\Users::USER_TYPE_CLIENT,
                'items' => [
                    ['label' => 'Пользователи', 'url' => ['/user/index']],
                    ['label' => 'Страницы входа', 'url' => ['/sliders/index']],
                    ['label' => 'Сотрудники', 'url' => ['/employees/index']],
                    ['label' => 'Заказчики', 'url' => ['/clients/index']],
                    ['label' => 'Категории сотрудников', 'url' => ['/employees-category/index']],
                    ['label' => 'Должности сотрудников', 'url' => ['/employees-position/index']],
                    ['label' => 'Способы размещения', 'url' => ['/placement/index']],
                    ['label' => 'Категории расчета', 'url' => ['/category-calculation/index']],
                    ['label' => 'Цены', 'url' => ['/prices/index']],
                    ['label' => 'Адресса', 'url' => ['/address-list/index']],
                    ['label' => 'Штрафы', 'url' => ['/fines/index']],
                    ['label' => 'Шаблоны маршрутных листов', 'url' => ['/route-templates/index']],
                    ['label' => 'Документация АПИ', 'url' => ['/api/v1']],
                ],
            ],
            $dev_item,
            ['label' => 'Настройки', 'url' => ['/settings/index'], 'visible' => Yii::$app->user->identity->role != \app\models\Users::USER_TYPE_CLIENT,],
            Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'get')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container-fluid" style="margin-top: 55px">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2019 <a href="https://shop-crm.ru/portfolio?utm=teo_job">Разработчик TEO</a>.</strong>
        All rights
        reserved.
    </div>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
