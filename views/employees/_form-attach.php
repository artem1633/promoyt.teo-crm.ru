<?php

use kartik\widgets\FileInput;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form-attach">

    <?php $form = ActiveForm::begin([
        'options' => [
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            ]
    ]); ?>
    <?=$form->field($model, 'files[]')->widget(FileInput::classname(), [
        'options'=>[
            'multiple'=>true,
        ],
        'pluginOptions' => [
            //'hideThumbnailContent'=> true,
            'overwriteInitial'=>true,
            'showUpload' => false,
            'browseLabel' => '',
            'maxFileSize'=>2800,
            //'theme' => "explorer",
        ]
    ])->label(false);?>
    <?php ActiveForm::end(); ?>
    
</div>
