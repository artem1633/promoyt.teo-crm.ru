<?php

use yii\helpers\Html;

/* @var $model app\models\Employees */

?>

<div class="row no-gutters">
    <div class="col-md-2">
        <?=\yii\helpers\Html::img($model->foto,['style'=>'max-width: 100%'])?>
    </div>
    <div class="col-md-5 ">
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('status')?></span>
                <span class="emp-value"><?=$model->getCurrentStatus()?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('position.name')?></span>
                <span class="emp-value"><?=$model->position->name?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('FIO')?></span>
                <span class="emp-value"><?=$model->fio?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('birthday')?></span>
                <span class="emp-value"><?=Yii::$app->formatter->asDate($model->birthday,'dd.MM.yyyy')?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('age')?></span>
                <span class="emp-value"><?=$model->age?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('phone')?></span>
                <span class="emp-value"><?=$model->phone?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('phone_add')?></span>
                <span class="emp-value"><?=$model->phone_add?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('phone_guarantor')?></span>
                <span class="emp-value"><?=$model->phone_guarantor?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('FIO_guarantor')?></span>
                <span class="emp-value"><?=$model->FIO_guarantor?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('address')?></span>
                <span class="emp-value"><?=$model->address?></span>
            </div>
        </div>


        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('family_status')?></span>
                <span class="emp-value"><?=$model->getCurrentFamilyStatus()?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('children')?></span>
                <span class="emp-value"><?=$model->getAllChildren()?></span>
            </div>
        </div>

    </div>
    <div class="col-md-5">
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('category_id')?></span>
                <span class="emp-value"><?=$model->category->name?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('email')?></span>
                <span class="emp-value"><?=$model->email?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('phone_id')?></span>
                <span class="emp-value"><?=$model->phone_id?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('phone_model')?></span>
                <span class="emp-value"><?=$model->phone_model?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"></span>
                <span class="emp-value"></span>
            </div>
        </div>

        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('start_work')?></span>
                <span class="emp-value"><?=Yii::$app->formatter->asDate($model->start_work,'dd.MM.yyyy')?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('pasport_number')?></span>
                <span class="emp-value"><?=$model->pasport_number?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('pasport_date')?></span>
                <span class="emp-value"><?=Yii::$app->formatter->asDate($model->pasport_date,'dd.MM.yyyy')?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('pasport_publish')?></span>
                <span class="emp-value"><?=$model->pasport_publish?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('code_structure')?></span>
                <span class="emp-value"><?=$model->code_structure?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('other_data')?></span>
                <span class="emp-value"><?=$model->other_data?></span>
            </div>
        </div>
        <div class="row oglavl">
            <div class="col-md-12">
                <span class="emp-label"><?=$model->getAttributeLabel('specialization')?></span>
                <span class="emp-value"><?=$model->getAllSpecializaton()?></span>
            </div>
        </div>

    </div>
</div>