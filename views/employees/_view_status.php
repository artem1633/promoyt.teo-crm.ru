<?php

use app\helpers\StatusHelper;
use app\models\Employees;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */
?>
<style>


</style>
<div class="employees-view">

    <?php foreach ($statuses as $status) :?>
        <div class="row oglavl">
            <div class="col-md-6">
                <span class="emp-label"><?=Yii::$app->formatter->asDate($status->date_status,'dd.MM.yyyy')?></span>
                <span class="emp-value"><?=Employees::getStatuses()[$status->status]?></span>
            </div>
        </div>
    <?php endforeach;?>


</div>
