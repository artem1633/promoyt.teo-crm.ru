<?php



/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .modal-lg {
        width: 1200px;
    }
    .form-group {
        margin-bottom: 0px;
    }
    samp {
        display: none;
    }
    .file-thumbnail-footer {
        display: none;
    }
    .form-group-sm .form-control {
        height: 25px;
    }
    .form-group-sm select.form-control {
        height: 25px;
    }

    .help-block {
        margin: 0px;
    }
    label {
        font-size: 12px;
        margin-bottom: 0px;
    }
    .file-drop-zone-title {
        padding: 55px 10px;
    }
    .input-group-addon {
        padding: 0px 6px;
    }

</style>
<div class="employees-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            ]
    ]); ?>
    <div class="row">
        <div class="form-group">
            <?= Html::a( 'Отмена', \yii\helpers\Url::to(['/employees/index']), ['class' => 'btn btn-danger pull-right']) ?>
            <?= Html::submitButton('Создать', ['class' => 'btn btn-primary  pull-right']) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-8 bg-info">
            <h5>Персональные данные</h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Паспортные данные</h5>
        </div>

    </div>
    <div class="row form-group form-group-sm">
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(Employees::getStatuses()) ?>
            <?= $form->field($model, 'category_id')->dropDownList(EmployeesCategory::getCategoryList()) ?>
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'parentname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]); ?>
            <?= $form->field($model, 'address')->textarea(['rows'=>2, 'maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'family_status')->dropDownList(Employees::getFamilyStatuses()) ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'position_id')->dropDownList(EmployeesPosition::getPositionList())?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'camera')->checkbox() ?>
            <?= $form->field($model, 'internet')->checkbox() ?>
            <?= $form->field($model, 'office')->checkbox() ?>
            <?= $form->field($model, 'departure_area')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'prefers_work')->textInput() ?>
            <?= $form->field($model, 'start_work')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
            <?= $form->field($model, 'pasport_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'pasport_publish')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'pasport_date')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
            <?= $form->field($model, 'code_structure')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 bg-info">
            <h5>Фото </h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Сканы паспорта</h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Прочие файлы</h5>
        </div>

    </div>
<div class="row">

    <div class="col-md-4">
        <?= $form->field($model, 'main_photo')->widget(FileInput::classname(), [
            'options'=>[
                'multiple'=>false,
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'overwriteInitial'=>true,
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'maxFileSize'=>2800,
                'initialPreview' => [
                        "<img src='" . $model->foto . "' class='kv-file-content additional_photos file-preview-image'>"
                ],
            ]
        ])->label(false);?>
    </div>
    <div class="col-md-4">
        <?=GridView::widget([
            'id'=>'crud-datatable-passport',
            'dataProvider' => $dataProviderPassport,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'file',
                    ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'header' =>"",
                    'template' => '{delete}',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) {
                        return Url::to(['passport-attach-delete','id'=>$key]);
                    },
                    'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Вы уверенны?',
                        'data-confirm-message'=>'Вы действительно хотите удалить запись '],
                ],
                ],
            'toolbar'=> [
                ['content'=>''

                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'heading' => false,
                'footer' => false,
                'before' => false,
                'after'=>'<div style="margin-top:10px;">' .
                    Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['eployees/foto-add'],
                        ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                    '</div>',
            ]
        ])?>
    </div>
    <div class="col-md-4 ">
        <?= $form->field($model, 'other_files[]')->widget(FileInput::classname(), [
            'options'=>[
                'multiple'=>true,
            ],
            'pluginOptions' => [
                'hideThumbnailContent'=> true,
                'overwriteInitial'=>true,
                'showUpload' => false,
                'browseLabel' => '',
                'maxFileSize'=>2800,
                'theme' => "explorer",
            ]
        ])->label(false); ?>
    </div>

</div>
    <?php ActiveForm::end(); ?>
    
</div>
