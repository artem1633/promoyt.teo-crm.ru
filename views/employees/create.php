<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */

?>
<div class="employees-create">
    <?= $this->render('_form-create', [
        'model' => $model,
    ]) ?>
</div>
