<?php

use app\models\Employees;
use app\models\EmployeesPosition;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use unclead\multipleinput\MultipleInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

CrudAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $dataProviderPassport \yii\data\ActiveDataProvider */
/* @var $dataProviderOther \yii\data\ActiveDataProvider */

$this->title = 'Сотрудник: ' . $model->FIO;

$this->params['breadcrumbs'][] = ['label' => 'Справочник сотрудников', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];

if (!$model->login){
    try {
        $model->login = $model->getLogin();
    } catch (\yii\base\Exception $e) {
        Yii::error($e->getTraceAsString(), 'error');
    }
}
?>
    <div class="employees-update">

        <style>
            .modal-lg {
                width: 1200px;
            }

            .form-group {
                margin-bottom: 0px;
            }

            samp {
                display: none;
            }

            .file-thumbnail-footer {
                display: none;
            }

            .form-group-sm .form-control {
                height: 25px;
            }

            .form-group-sm select.form-control {
                height: 25px;
            }

            .help-block {
                margin: 0px;
            }

            label {
                font-size: 12px;
                margin-bottom: 0px;
            }

            .file-drop-zone-title {
                padding: 55px 10px;
            }

            .input-group-addon {
                padding: 0px 6px;
            }

            .multiple-input-list__btn {
                padding: 0px 6px;
            }

        </style>
        <div class="employees-form">

            <?php $form = ActiveForm::begin([
                'options' => [
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ]
            ]); ?>
            <div class="row">
                <div class="form-group">
                    <?= Html::a('Отмена', \yii\helpers\Url::to(['/employees/index']),
                        ['class' => 'btn btn-danger pull-right']) ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary  pull-right']) ?>

                </div>
            </div>

            <div class="row">
                <div class="col-md-8 bg-info">
                    <h5>Персональные данные</h5>
                </div>
                <div class="col-md-4 bg-info">
                    <h5>Паспортные данные</h5>
                </div>

            </div>
            <div class="row form-group form-group-sm">
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->dropDownList(Employees::getStatuses()) ?>
                    <?= $form->field($model, 'position_id')->dropDownList(EmployeesPosition::getPositionList()) ?>
                    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'parentname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ]
                    ]); ?>
                    <?= $form->field($model, 'family_status')->dropDownList(Employees::getFamilyStatuses()) ?>
                    <?= $form->field($model, 'children')->widget(MultipleInput::className(), [
                        'columns' => [
                            [
                                'name' => 'name',
                                'title' => 'Имя ребенка',
                            ],
                            [
                                'name' => 'birthday',
                                'type' => \kartik\date\DatePicker::className(),
                                'title' => 'День рождения',
                                'options' => [
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                                    'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                    ]
                                ]
                            ],
                            [
                                'name' => 'sex',
                                'type' => 'dropDownList',
                                'title' => 'Пол',
                                'defaultValue' => 1,
                                'items' => [
                                    1 => 'Мужской',
                                    2 => 'Женский'
                                ]
                            ],

                        ],
                        'min' => 0, // should be at least 2 rows
                        'allowEmptyList' => false,
                        'enableGuessTitle' => true,
                        'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
                    ])->label(false); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'address')->textarea(['rows' => 2, 'maxlength' => true]) ?>
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone_add')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone_guarantor')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'FIO_guarantor')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone_model')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone_id')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-4">

                    <?= $form->field($model, 'start_work')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                    <?= $form->field($model, 'pasport_number')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'pasport_publish')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'pasport_date')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                    <?= $form->field($model, 'code_structure')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'specializations')->widget(select2::classname(), [
                        'data' => Employees::$specializationList,
                        'options' => [
                            'placeholder' => 'Выберите специализации ...',
                            'multiple' => true
                        ],
                    ]); ?>
                    <?= $form->field($model, 'other_data')->textarea(['rows' => 2, 'maxlength' => true]) ?>

                    <div class="row">
                        <?php
                        $model->account ? $display_btn = 'none' : $display_btn = 'block';
                        $model->account ? $display_form = 'block' : $display_form = 'none';
                        ?>
                        <div id="activate-btn" class="col-md-12" style="display:<?= $display_btn ?>">
                            <?= Html::button('Активировать логин и пароль', [
//                                'id' => 'activate-btn',
                                'class' => 'btn btn-primary btn-block'
                            ]) ?>
                            <?php //echo $form->field($model, 'account')->checkbox(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6 account-form" style="display:<?= $display_form ?>">
                            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6 account-form" style="display:<?= $display_form ?>">
                            <?php
                            if ($model->password){
                               echo $form->field($model, 'new_password')->textInput(['maxlength' => true]);
                            } else {
                               echo $form->field($model, 'password')->textInput(['maxlength' => true]);
                            }

                            ?>
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-4 bg-info">
                    <h5>Фото </h5>
                </div>
                <div class="col-md-4 bg-info">
                    <h5>Сканы паспорта</h5>
                </div>
                <div class="col-md-4 bg-info">
                    <h5>Прочие файлы</h5>
                </div>

            </div>
            <div class="row">

                <div class="col-md-4">
                    <?= $form->field($model, 'main_photo')->widget(FileInput::classname(), [
                        'options' => [
                            'multiple' => false,
                            'accept' => 'image/*',
                        ],
                        'pluginOptions' => [
                            'overwriteInitial' => true,
                            'showUpload' => false,
                            'browseLabel' => '',
                            'removeLabel' => '',
                            'maxFileSize' => 2800,
                            'initialPreview' => [
                                "<img src='" . $model->foto . "' class='kv-file-content additional_photos file-preview-image'>"
                            ],
                        ]
                    ])->label(false); ?>
                </div>
                <div class="col-md-4">
                    <div id="ajaxCrudDatatable1">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable-passport',
                                'dataProvider' => $dataProviderPassport,
                                //'filterModel' => $searchModel,
                                'pjax' => true,
                                'columns' => [
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'attribute' => 'file',
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'header' => "",
                                        'template' => '{delete}',
                                        'dropdown' => false,
                                        'vAlign' => 'middle',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return Url::to(['attach-delete', 'category' => 1, 'id' => $key]);
                                        },
                                        'deleteOptions' => [
                                            'role' => 'modal-remote',
                                            'title' => 'Удалить',
                                            'data-confirm' => false,
                                            'data-method' => false,// for overide yii data api
                                            'data-request-method' => 'post',
                                            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                                            'data-toggle' => 'tooltip'
                                        ],
                                    ],
                                ],
                                'toolbar' => [
                                    [
                                        'content' => ''

                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'heading' => false,
                                    'footer' => false,
                                    'before' => false,
                                    'after' => '<div style="margin-top:10px;">' .
                                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>',
                                            ['employees/attach-add', 'category' => 1, 'id' => $model->id],
                                            [
                                                'role' => 'modal-remote',
                                                'title' => 'Добавить',
                                                'class' => 'btn btn-info'
                                            ]) .
                                        '</div>',
                                ]
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getTraceAsString(), 'error');
                        } ?>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div id="ajaxCrudDatatable2">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable-other',
                                'dataProvider' => $dataProviderOther,
                                //'filterModel' => $searchModel,
                                'pjax' => true,
                                'columns' => [
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'attribute' => 'file',
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'header' => "",
                                        'template' => '{delete}',
                                        'dropdown' => false,
                                        'vAlign' => 'middle',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return Url::to(['attach-delete', 'category' => 2, 'id' => $key]);
                                        },
                                        'deleteOptions' => [
                                            'role' => 'modal-remote',
                                            'title' => 'Удалить',
                                            'data-confirm' => false,
                                            'data-method' => false,// for overide yii data api
                                            'data-request-method' => 'post',
                                            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                                            'data-toggle' => 'tooltip'
                                        ],
                                    ],
                                ],
                                'toolbar' => [
                                    [
                                        'content' => ''

                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'heading' => false,
                                    'footer' => false,
                                    'before' => false,
                                    'after' => '<div style="margin-top:10px;">' .
                                        Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>',
                                            ['employees/attach-add', 'category' => 2, 'id' => $model->id],
                                            [
                                                'role' => 'modal-remote',
                                                'title' => 'Добавить',
                                                'class' => 'btn btn-info'
                                            ]) .
                                        '</div>',
                                ]
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getTraceAsString(), 'error');
                        } ?>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>

        </div>


    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
        'width' => '1200px',
    ],
    'size' => 'modal-lg',
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS
    $(document).ready(function() {
      $(document).on('click', '#activate-btn', function() {
            $('#activate-btn').hide();
            $('.account-form').show();
      })
    })
JS
);
