<?php

use app\models\Employees;
use app\models\EmployeesCategory;
use app\models\EmployeesPosition;
use kartik\date\DatePicker;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .modal-lg {
        width: 1200px;
    }
    .form-group {
        margin-bottom: 0px;
    }
    samp {
        display: none;
    }
    .file-thumbnail-footer {
        display: none;
    }
    .form-group-sm .form-control {
        height: 27px;
    }
    .form-group-sm select.form-control {
        height: 27px;
    }

    .help-block {
        margin: 0px;
    }
    label {
        font-size: 12px;
        margin-bottom: 0px;
    }
    .file-drop-zone-title {
        padding: 55px 10px;
    }
    .input-group-addon {
        padding: 0px 6px;
    }

</style>
<div class="employees-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            ]
    ]); ?>
    <div class="row">
        <div class="form-group">
            <?= Html::a( 'Отмена', \yii\helpers\Url::to(['/employees/index']), ['class' => 'btn btn-danger pull-right']) ?>
            <?= Html::submitButton('Создать', ['class' => 'btn btn-primary  pull-right']) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-8 bg-info">
            <h5>Персональные данные</h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Паспортные данные</h5>
        </div>

    </div>
    <div class="row form-group form-group-sm">
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(Employees::getStatuses()) ?>
            <?= $form->field($model, 'position_id')->dropDownList(EmployeesPosition::getPositionList())?>
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'parentname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]); ?>
            <?= $form->field($model, 'family_status')->dropDownList(Employees::getFamilyStatuses()) ?>
            <?= $form->field($model, 'children')->widget(MultipleInput::className(), [
                'columns' => [
                    [
                        'name'  => 'name',
                        'title' => 'Имя ребенка',
                    ],
                    [
                        'name'  => 'birthday',
                        'type'  => \kartik\date\DatePicker::className(),
                        'title' => 'День рождения',
                        'options' => [
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                            'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                            ]
                        ]
                    ],
                    [
                        'name'  => 'sex',
                        'type'  => 'dropDownList',
                        'title' => 'Пол',
                        'defaultValue' => 1,
                        'items' => [
                            1 => 'Мужской',
                            2 => 'Женский'
                        ]
                    ],

                ],
                'min'               => 0, // should be at least 2 rows
                'allowEmptyList'    => false,
                'enableGuessTitle'  => true,
                'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
            ])->label(false);?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'address')->textarea(['rows'=>2, 'maxlength' => true]) ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone_add')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone_guarantor')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'FIO_guarantor')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone_model')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone_id')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">

            <?= $form->field($model, 'start_work')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
            <?= $form->field($model, 'pasport_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'pasport_publish')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'pasport_date')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="glyphicon glyphicon-calendar text-primary"></i>',
                'removeIcon' => '<i class="glyphicon glyphicon-trash text-danger"></i>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
            <?= $form->field($model, 'code_structure')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'specializations')->widget(select2::classname(), [
                'data' => Employees::$specializationList,
                'options' => [
                    'placeholder' => 'Выберите специализации ...',
                    'multiple' => true
                ],
            ]); ?>
            <?= $form->field($model, 'other_data')->textarea(['rows'=>2, 'maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4 bg-info">
            <h5>Фото </h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Сканы паспорта</h5>
        </div>
        <div class="col-md-4 bg-info">
            <h5>Прочие файлы</h5>
        </div>

    </div>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'main_photo')->widget(FileInput::classname(), [
            'options'=>[
                'multiple'=>false,
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'overwriteInitial'=>true,
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'maxFileSize'=>2800,

            ]
        ])->label(false);?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'pasport_files[]')->widget(FileInput::classname(), [
            'options'=>[
                'multiple'=>true,
            ],
            'pluginOptions' => [
                'hideThumbnailContent'=> true,
                'overwriteInitial'=>true,
                'showUpload' => false,
                'browseLabel' => '',
                'maxFileSize'=>2800,
                'theme' => "explorer",
            ]
        ])->label(false);?>
    </div>
    <div class="col-md-4 ">
        <?= $form->field($model, 'other_files[]')->widget(FileInput::classname(), [
            'options'=>[
                'multiple'=>true,
            ],
            'pluginOptions' => [
                'hideThumbnailContent'=> true,
                'overwriteInitial'=>true,
                'showUpload' => false,
                'browseLabel' => '',
                'maxFileSize'=>2800,
                'theme' => "explorer",
            ]
        ])->label(false); ?>
    </div>

</div>
    <?php ActiveForm::end(); ?>
    
</div>
