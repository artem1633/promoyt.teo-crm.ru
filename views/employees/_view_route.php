<?php

use kartik\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $routeModel app\models\Employees */
?>
<div class="employees-view-route">

    <div class="row">
        <div class="col-md-2">
            <div class="row">
                <?php $form = ActiveForm::begin(['id' => 'employees-route'
                ]); ?>
                <div class="col-md-12 bg-info">
                    <h5>Фильтр</h5>
                    <?= $form->field($routeModel, 'start_date')->textInput(['type'=>'date','onChange'=>'selectRouteList()']); ?>
                    <?= $form->field($routeModel, 'end_date')->textInput(['type'=>'date']);?>
                    <?= $form->field($routeModel, 'route')->dropDownList(\app\models\Routes::getRouteFromEmployee($model->id)) ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?= \yii\helpers\Html::a('<span class="btn btn-primary">Сформировать карту</span>', "#",['onClick'=>'getEmployeeRoute()']) ?>

                        </div>
                    </div>

                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div class="col-md-10">
            <div id="playPanel" class="row" style="display: none">
                <div class="col-xs-1">
                    <?=Html::img('/images/yandex/start.png',['onClick'=>'play()','id'=>'play','style'=>'margin-top:20px']);?>
                </div>
                <div class="col-xs-11">
                    <input type="text" id="example_id" name="example_name" value="" />
                </div>
            </div>
            <div class="row">
                <div id="map">
                </div>
            </div>
        </div>
    </div>

</div>

<style>
    #map {
        width: 100%; height: 600px; padding: 0; margin: 0;
    }
</style>
<style>
    .YMaps-layer-container img{
        max-width: none;
    }
</style>
<?php

$script = <<<JS
var employeeId = $model->id,
center_map = [55.750625, 37.626];
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css");
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;", ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile("/js/employee_map.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
