<?php

use app\helpers\StatusHelper;
use app\models\Employees;
use app\models\EmployeesPosition;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'manager_id',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'value' => function ($model) {
            return $model->getCurrentStatus();
        },
        'filter' => Employees::getStatuses(),
        'contentOptions' => ['style' => 'width:150px; text-align:center'],


    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'position_id',
        'value' => function ($model) {
            return $model->position->name;
        },
        'filter' => EmployeesPosition::getPositionList(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fio',
        'label' => 'ФИО',
        'value' => function (Employees $model) {
            return Html::a($model->fio ?? null, ['/employees/view', 'id' => $model->id]);
        },
        'format' => 'raw',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'lastname',
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'firstname',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'family_status',
        'value' => function ($model) {
            return $model->getCurrentFamilyStatus();
        },
        'filter' => Employees::getFamilyStatuses(),
        'contentOptions' => ['style' => 'width:100px; text-align:center'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'code_structure',
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => "&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => [
            'label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>',
            'data-pjax' => 0,
        ],
        'updateOptions' => [
            'label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>',
            'data-pjax' => 0,
            'title' => 'Изменить',
            'data-toggle' => 'tooltip'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись '
        ],
    ],

];   