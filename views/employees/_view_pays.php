<?php

use kartik\grid\GridView;

/** @var $this \yii\web\View */
/** @var $searchModel \app\models\search\EmployeesPaysSearch */
/** @var $dataProvider \yii\data\ArrayDataProvider*/


$placements = \yii\helpers\ArrayHelper::map(\app\models\PlacementMethod::find()->all(), 'id', 'name');

$placementsColumns = [];

foreach ($placements as $id => $name){
    $placementsColumns[] = [
        'label' => "КР «{$name}»",
        'value' => "p-{$id}",
    ];
}


$columns = [
    [
        'attribute' => 'projectId',
        'label' => 'Проект',
        'value' => 'project_name'
    ],
    [
        'label' => 'Фактическое кол-во',
        'value' => 'fact_count'
    ],
    [
        'label' => 'Подтвержденное кол-во',
        'value' => 'completed_count',
    ],
    [
        'label' => 'Рабочих часов',
        'value' => 'workHours',
    ]
];

$columns = \yii\helpers\ArrayHelper::merge($columns, $placementsColumns);

$columns = \yii\helpers\ArrayHelper::merge($columns, [
    [
        'label' => 'Сумма штрафов',
        'value' => 'completed_count',
        'format' => ['currency', 'rub'],
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'label' => 'Оплата',
        'value' => 'payment_amount',
        'format' => ['currency', 'rub'],
        'hAlign' => GridView::ALIGN_CENTER,
    ],
]);

?>

<div class="row">
    <div class="col-md-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i>Отчет по зарплатам',
                'before'=>'',
                'after'=>'',
            ]
        ]) ?>
    </div>
</div>


