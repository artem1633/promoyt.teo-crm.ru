<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 23.04.2017
 * Time: 15:57
 */

namespace app\services;


use app\models\AddressList;
use app\models\RouteAddress;
use app\models\Settings;
use app\models\Zones;
use PhpOffice\PhpSpreadsheet\Style\Border;

class YandexStorage
{
    public static function getCenterRoute($route_id)
    {
        $route_address = RouteAddress::find()->where()->all();
    }

    public static function getCoordByAddress($addres)
    {
        $params = array(
            'geocode' => $addres, // адрес
            'format' => 'json',                          // формат ответа
            'results' => 1,                               // количество выводимых результатов
            'apikey' => Settings::findByKey('api_key')->value,                           // ваш api key
        );
        $adress = 'http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&');
        echo $adress.'<br>';

        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
            $res = explode(' ', $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
            $coords['x'] = $res[1];
            $coords['y'] = $res[0];
            return $coords;
        } else {
            return false;
        }

    }

    public static function getBorderZone($CoordArr)
    {
        $maxX = $CoordArr[0][0];
        $maxY = $CoordArr[0][1];
        $minX = $CoordArr[0][0];
        $minY = $CoordArr[0][1];
        foreach ($CoordArr as $item) {
            if (($maxX) < $item[0]) {
                $maxX = $item[0];
            }
            if (($minX) > $item[0]) {
                $minX = $item[0];
            }
            if (($maxY) < $item[1]) {
                $maxY = $item[1];
            }
            if (($minY) > $item[1]) {
                $minY = $item[1];
            }
        }
        return [
            'maxX' => $maxX,
            'maxY' => $maxY,
            'minX' => $minX,
            'minY' => $minY,
        ];
    }

    protected static function objectInPoly($oblect, $poly)
    {
        //int IsPointInsidePolygon (Point *p, int Number, int x, int y)

        //  int i1, i2, n, N, S, S1, S2, S3, flag;
        unset($poly[0]);
        $poly2 =[];
        $oblect['x'] = intval($oblect['x'] * 1000000);
        $oblect['y'] = intval($oblect['y'] * 1000000);
        foreach ($poly as $k=>$p) {
            $poly2[$k]['x'] = intval($poly[$k]['x'] * 1000000);
            $poly2[$k]['y'] = intval($poly[$k]['y'] * 1000000);
        }
        $poly = $poly2;
        $N = count($poly);
        for ($n = 0; $n < $N; $n++) {
            $flag = 0;
            $i1 = $n < $N - 1 ? $n + 1 : 0;
            while ($flag == 0) {
                $i2 = $i1 + 1;
                if ($i2 >= $N)
                    $i2 = 0;
                if ($i2 == ($n < $N - 1 ? $n + 1 : 0))
                    break;
                $S = abs($poly[$i1]['x'] * ($poly[$i2] . ['y'] - $poly[$n]['y']) +
                    $poly[$i2]['x'] * ($poly[$n]['y'] - $poly[$i1]['y']) +
                    $poly[$n]['x'] * ($poly[$i1]['y'] - $poly[$i2]['y']));
                $S1 = abs($poly[$i1]['x'] * ($poly[$i2]['y'] - $oblect['y']) +
                    $poly[$i2]['x'] * ($oblect['y'] - $poly[$i1]['y']) +
                    $oblect['x'] * ($poly[$i1]['y'] - $poly[$i2]['y']));
                $S2 = abs($poly[$n]['x'] * ($poly[$i2]['y'] - $oblect['y']) +
                    $poly[$i2]['x'] * ($oblect['y'] - $poly[$n]['y']) +
                    $oblect['x'] * ($poly[$n]['y'] - $poly[$i2]['y']));
                $S3 = abs($poly[$i1]['x'] * ($poly[$n]['y'] - $oblect['y']) +
                    $poly[$n]['x'] * ($oblect['y'] - $poly[$i1]['y']) +
                    $oblect['x'] * ($poly[$i1]['y'] - $poly[$n]['y']));

                if ($S == $S1 + $S2 + $S3) {
                    $flag = 1;
                    break;
                }
                var_dump($S);
                var_dump($S1+$S2+S3);
                $i1 = $i1 + 1;
                if ($i1 >= $N)
                    $i1 = 0;
            }
            if ($flag == 0)
                break;
        }
        return boolval(flag);
    }

    public static function getCountObjectZone($zone_id)
    {
        $zone = Zones::findOne($zone_id);
        $count = 0;
        $coorarr = unserialize($zone->coorarr);
        $coorarr = $coorarr[0];
        foreach ( $coorarr as $k=>$v )
        {
            $coorarr[$k] ['x'] = $coorarr[$k][0];
            unset($coorarr[$k][0]);
            $coorarr[$k] ['y'] = $coorarr[$k][1];
            unset($coorarr[$k][1]);
        }
        $border = unserialize($zone->border);

        $addressList = AddressList::find()
            ->where(['between', 'coord_x', $border['minX'], $border['maxX']])
            ->where(['between', 'coord_y', $border['minY'], $border['maxY']])
            ->asArray()->all();
        foreach ($addressList as $item){
            var_dump($item['id']);
            if (self::objectInPoly(['x'=>$item['coord_x'],'y'=>$item['coord_y']],$coorarr)) {
                echo "in";
            }

        }


    }

}