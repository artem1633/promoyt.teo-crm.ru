<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 23.04.2017
 * Time: 15:57
 */

namespace app\services;


use app\models\AddressList;
use app\models\ProjectAddress;
use app\models\Projects;
use app\models\RouteAddress;
use app\models\Zones;
use PhpOffice\PhpSpreadsheet\Style\Border;
use yii\helpers\ArrayHelper;

class Polygon
{

    protected $coords;

    public function __construct($coords)
    {
        foreach ($coords as $k => $v) {
            $coords[$k] ['x'] = $coords[$k][0];
            unset($coords[$k][0]);
            $coords[$k] ['y'] = $coords[$k][1];
            unset($coords[$k][1]);
        }
        $this->coords = $coords;
    }

    public static function factory($coords)
    {
        return new Polygon($coords);
    }

    public function getCoords()
    {
        return $this->coords;
    }


    public function getBorderZone()
    {
        $maxX = $this->coords[0]['x'];
        $maxY = $this->coords[0]['y'];
        $minX = $this->coords[0]['x'];
        $minY = $this->coords[0]['y'];
        foreach ($this->coords as $item) {
            if (($maxX) < $item['x']) {
                $maxX = $item['x'];
            }
            if (($minX) > $item['x']) {
                $minX = $item['x'];
            }
            if (($maxY) < $item['y']) {
                $maxY = $item['y'];
            }
            if (($minY) > $item['y']) {
                $minY = $item['y'];
            }
        }
        return [
            'maxX' => $maxX,
            'maxY' => $maxY,
            'minX' => $minX,
            'minY' => $minY,
        ];
    }

    public function getCenterZone()
    {
        $border = $this->getBorderZone();
        return [
            'centerX' => ($border['minX']+$border['maxX'])/2,
            'centerY' => ($border['minY']+$border['maxY'])/2,
        ];
    }

    public function objectInPoly($point)
    {

        if ($this->pointOnVertex($point) == true) {
            return "vertex";
        }

        $intersections = 0;
        $vertices_count = count($this->coords);
        $vertices = $this->coords;
        for ($i = 1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i - 1];
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'],
                    $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return "boundary";
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'],
                $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x'];
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return "boundary";
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++;
                }
            }
        }

        if ($intersections % 2 != 0) {
            return "inside";
        } else {
            return "outside";
        }
    }

    private function pointOnVertex($point)
    {
        foreach ($this->coords as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }

    }

    public function getCountObjects($project_id)
    {

        $count = ['count_objects'=>0,'count_entrance'=>0,'count_floor'=>0,'count_apartament'=>0];
        $border = $this->getBorderZone();
        $addresses = AddressList::find()
            ->where(['between', 'address_list.coord_x', $border['minX'], $border['maxX']])
            ->andWhere(['between', 'address_list.coord_y', $border['minY'], $border['maxY']])
            ->asArray()->all();

        $addressList = [];

        foreach ($addresses as $list)
        {
            $projectsAddresses = ProjectAddress::find()->where(['project_id' => $project_id, 'address_id' => $list['id']])->all();
            if(count($projectsAddresses) > 0){
                   $addressList[] = $list;
            }
        }

        foreach ($addressList as $item) {
            if ($this->objectInPoly(['x' => $item['coord_x'], 'y' => $item['coord_y']]) == 'inside') {
                $count['count_objects']++;
                $count['count_entrance'] +=$item['entrance'];
                $count['count_floor'] +=$item['floor'];
                $count['count_apartament'] +=$item['apartament'];

            }
        }
        return $count;
    }

    /**
     * @param integer|null $projectId
     * @return array
     */
    public function getObjects($projectId = null)
    {
        $objects = [];
        $border = $this->getBorderZone();
        $addressList = AddressList::find()
            ->where(['between', 'address_list.coord_x', $border['minX'], $border['maxX']])
            ->andWhere(['between', 'address_list.coord_y', $border['minY'], $border['maxY']]);

        if($projectId != null){
            $addressProject = ArrayHelper::getColumn(ProjectAddress::find()->where(['project_id' => $projectId])->all(), 'address_id');
            $addressList->andWhere(['id' => $addressProject]);
        }

        $addressList = $addressList->asArray()->all();


        foreach ($addressList as $item) {
            if ($this->objectInPoly(['x' => $item['coord_x'], 'y' => $item['coord_y']]) == 'inside') {
                $objects[] = $item;
            }
        }
        return $objects;
    }

    /*returns
      1 if there is one intersection point "c"
      0 if chunks ar on parallel lines
    -1 if there are no intersection points
      2 if a1 lies on [b1,b2]
    */

    protected function checkIntersection($a1, $a2, $b1, $b2)
    {
        $d = ($a1['x'] - $a2['x']) * ($b2['y'] - $b1['y']) - ($a1['y'] - $a2['y']) * ($b2['x'] - $b1['x']);
        $da = ($a1['x'] - $b1['x']) * ($b2['y'] - $b1['y']) - ($a1['y'] - $b1['y']) * ($b2['x'] - $b1['x']);
        $db = ($a1['x'] - $a2['x']) * ($a1['y'] - $b1['y']) - ($a1['y'] - $a2['y']) * ($a1['x'] - $b1['x']);

        if (abs($d) < 0.0000001) {
            return 0;
        } else {
            $ta = $da / $d;
            $tb = $db / $d;
            if ((abs($ta) < 0.0000001) and ((0 <= $tb) and ($tb <= 1))) {
                return 2;
            } elseif ((0 <= $ta) && (0 <= $tb) && ($tb <= 1)) {
                return 1;
            } else {
                return -1;
            }
        }
    }


}
