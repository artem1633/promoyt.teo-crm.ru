<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 23.04.2017
 * Time: 15:57
 */

namespace app\services;



class DateUtil
{
    public static function getMonth($month)
    {
        $arr = [
            'январь',
            'февраль',
            'март',
            'апрель',
            'май',
            'июнь',
            'июль',
            'август',
            'сентябрь',
            'октябрь',
            'ноябрь',
            'декабрь'
        ];
        return $arr[$month-1];
    }
}