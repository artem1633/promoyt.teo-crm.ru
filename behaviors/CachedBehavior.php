<?php
/**
 * Created by PhpStorm.
 * User: Максим
 * Date: 20.02.2019
 * Time: 22:11
 */

namespace app\behaviors;


class CachedBehavior
{
    public $cache_table;



    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'deleteCache',
            ActiveRecord::EVENT_AFTER_UPDATE => 'deleteCache',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteCache',
        ];
    }

    public function deleteCache()
    {
        Yii::$app->cache->delete($this->cache_table);
    }
}