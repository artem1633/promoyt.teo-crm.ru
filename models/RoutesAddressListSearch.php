<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RoutesAddressList;

/**
 * RoutesAddressListSearch represents the model behind the search form of `app\models\RoutesAddressList`.
 */
class RoutesAddressListSearch extends RoutesAddressList
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'routes_id', 'address_list_id'], 'integer'],
            [['completed_edition', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RoutesAddressList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('addressList');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'routes_id' => $this->routes_id,
            'address_list_id' => $this->address_list_id,
        ]);

        $query->andFilterWhere(['like', 'completed_edition', $this->completed_edition])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
