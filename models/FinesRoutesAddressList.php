<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fines_routes_address_list".
 *
 * @property int $fines_id
 * @property int $routes_address_list_id
 *
 * @property Fines $fines
 * @property RoutesAddressList $routesAddressList
 */
class FinesRoutesAddressList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fines_routes_address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fines_id', 'routes_address_list_id'], 'required'],
            [['fines_id', 'routes_address_list_id'], 'integer'],
            [['fines_id', 'routes_address_list_id'], 'unique', 'targetAttribute' => ['fines_id', 'routes_address_list_id']],
            [['fines_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fines::className(), 'targetAttribute' => ['fines_id' => 'id']],
            [['routes_address_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoutesAddressList::className(), 'targetAttribute' => ['routes_address_list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fines_id' => 'Fines ID',
            'routes_address_list_id' => 'Routes Address List ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFines()
    {
        return $this->hasOne(Fines::className(), ['id' => 'fines_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutesAddressList()
    {
        return $this->hasOne(RoutesAddressList::className(), ['id' => 'routes_address_list_id']);
    }
}
