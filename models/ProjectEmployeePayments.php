<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "project_employee_payments".
 *
 * @property int $id
 * @property int $project_id Проект
 * @property int $employee_id Промоутер
 * @property double $amount Сумма оплаты
 * @property string $payed_at Дата и время оплаты
 *
 * @property boolean $isPayed Оплачено
 *
 * @property Employees $employee
 * @property Projects $project
 */
class ProjectEmployeePayments extends \yii\db\ActiveRecord
{
    const EVENT_PAYED = 'payed';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_employee_payments';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_PAYED => ['payed_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'employee_id'], 'integer'],
            [['amount'], 'number'],
            [['payed_at'], 'safe'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'employee_id' => 'Промоутер',
            'amount' => 'Сумма оплаты',
            'payed_at' => 'Дата и время оплаты',
        ];
    }

    /**
     * @return boolean
     */
    public function getIsPayed()
    {
       return $this->payed_at != null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::class, ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::class, ['id' => 'project_id']);
    }

    /**
     * Помечает запись как оплаченую
     * @return boolean
     */
    public function pay()
    {
        self::trigger(self::EVENT_PAYED);
        return $this->save(false);
    }
}
