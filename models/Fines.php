<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fines".
 *
 * @property int $id
 * @property string $name Название
 * @property int $value Значение
 * @property int $type Тип
 * @property int $interval_before Интервал до
 * @property int $interval_after Интервал после
 *
 * @property FinesRoutesAddressList[] $finesRoutesAddressLists
 * @property RoutesAddressList[] $routesAddressLists
 */
class Fines extends ActiveRecord
{
    const TYPE_PERCENT = 0;
    const TYPE_SUM = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'type', 'interval_before', 'interval_after'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'value' => 'Значение',
            'type' => 'Тип',
            'interval_before' => 'Интервал до',
            'interval_after' => 'Интервал после',
        ];
    }

    /**
     * @return array
     */
    public function getTypesList()
    {
       return [
           self::TYPE_PERCENT => 'Процент',
           self::TYPE_SUM => 'Сумма',
       ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinesRoutesAddressLists()
    {
        return $this->hasMany(FinesRoutesAddressList::class, ['fines_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRoutesAddressLists()
    {
        return $this->hasMany(RoutesAddressList::class, ['id' => 'routes_address_list_id'])->viaTable('fines_routes_address_list', ['fines_id' => 'id']);
    }

    public static function getFinesList()
    {
        return ArrayHelper::map(Fines::find()->all(),'id','name');
    }

    /**
     * Получает наименование штрафа по ID
     * @param int $id ID Штрафа
     * @return null|string
     */
    public static function getName($id)
    {
        return self::findOne($id)->name ?? null;
    }
}
