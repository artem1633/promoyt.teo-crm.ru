<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "done_works".
 *
 * @property int $id
 * @property int $route_id ID маршрута
 * @property int $address_id ID адреса
 * @property int $entrance_num Номер подъезда
 * @property int $floor_count Количество этажей
 * @property int $apartment_count Количество квартир
 * @property int $porter Наличие вахты
 * @property string $created_at Дата и время добавления данных
 *
 * @property AddressList $address
 * @property Routes $route
 */
class DoneWorks extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'done_works';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['route_id', 'address_id', 'entrance_num', 'floor_count', 'apartment_count', 'porter'], 'integer'],
            [['created_at'], 'safe'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressList::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'ID маршрута',
            'address_id' => 'ID адреса',
            'entrance_num' => 'Номер подъезда',
            'floor_count' => 'Количество этажей',
            'apartment_count' => 'Количество квартир',
            'porter' => 'Наличие вахты',
            'n' => 'Н',
            'created_at' => 'Дата и время добавления данных',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(AddressList::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    /**
     * Соединяет путь к папке с названием файла
     * @param $path_dir
     * @param array $files
     * @return array
     */
    public static function getFullPathFiles($path_dir, array $files)
    {
        $result = [];
        foreach ($files as $file){
            array_push($result, $path_dir . '/' . $file);
        }

        return $result;
    }
}
