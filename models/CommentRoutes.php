<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "comment_routes".
 *
 * @property int $id
 * @property int $route_id Маршрут
 * @property int $comment_type Тип коментария
 * @property string $comment_date Дата коментария
 * @property int $comment_read Прочитан
 * @property int $comment_confirm Подтвержден
 * @property string $comment_text Текст коментария
 *
 * @property Routes $route
 */
class CommentRoutes extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment_routes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['route_id', 'comment_type', 'comment_read', 'comment_confirm'], 'integer'],
            [['comment_date'], 'safe'],
            [['comment_text'], 'string', 'max' => 255],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Маршрут',
            'comment_type' => 'Тип коментария',
            'comment_date' => 'Дата коментария',
            'comment_read' => 'Прочитан',
            'comment_confirm' => 'Подтвержден',
            'comment_text' => 'Текст коментария',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }
}
