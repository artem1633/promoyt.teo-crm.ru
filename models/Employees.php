<?php

namespace app\models;

use DateTime;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property int $manager_id Менеджер
 * @property int $status Статус
 * @property int $category_id Категория
 * @property string $firstname Фамилия
 * @property string $lastname Имя
 * @property string $parentname Отчество
 * @property string $birthday Дата рождения
 * @property string $address Адрес проживания
 * @property int $family_status Смейное положение
 * @property string $phone Телфон
 * @property string $phone2 Телефон поручителя
 * @property int $position_id Должность
 * @property string $email E-mail
 * @property int $camera Есть фотоапарат?
 * @property int $internet Есть интернет?
 * @property int $office Сотрудник офиса
 * @property int $departure_area Выезд в область
 * @property int $prefers work Предпочитает работу
 * @property string $start_work Дата приема на работу
 * @property string $pasport_number Серия и номер паспорта
 * @property string $pasport_publish Кем выдан
 * @property string $pasport_date Дата выдачи
 * @property string $code_structure Код подразделения
 * @property string $foto Фотография
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $new_password Новый Пароль
 * @property string $token Токен
 * @property string $account Наличие аккаунта
 * @property string $specialization Специализация
 * @property string $phone_id ID смартфона
 * @property string $device_token FCM токен смартофна
 *
 * @property string $fullName ФИО
 *
 * @property EmployeesPosition $category
 * @property EmployeesPosition $position
 * @property EmployeesAttach[] $employeesAttaches
 *
 * @property login
 * @property password
 * @property account
 */
class Employees extends ActiveRecord
{

    public $pasport_files;
    public $other_files;
    public $main_photo;

    const STATUS_IN_WORK = 0;
    const STATUS_ON_HOLIDAY = 1;
    const STATUS_ON_SICK_LEAVE = 2;
    const STATUS_FIRED = 3;

    const FAMILY_UNMARRIED = 0;
    const FAMILY_MARRIED = 1;
    const FAMILY_DIVORCED = 2;

    public $children;

    public static $specializationList = [
        'go_oblast' => 'Выезд вобласть',
        'go_country' => 'Выезд по России',
        'naruzhka' => 'Наружка',
        'poetazhka' => 'Поетажка',
        'pya' => 'П/я',
        'etazh1' => 'Первые етажи',
        'ras_car' => 'Раскладка по машинам',
        'montazh' => 'Монтажник',
        'have_car' => 'Наличие личного авто',
    ];

    public $specializations;
    public $new_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'status', 'category_id', 'family_status', 'position_id',], 'integer'],
            [['status'], 'filter', 'filter' => 'intval'],
            [['birthday', 'start_work', 'pasport_date', 'deposit', 'device_token'], 'safe'],
            [['firstname', 'lastname', 'parentname'], 'string', 'max' => 60],
            [['address'], 'string', 'max' => 120],
            [['category_id', 'specializations', 'login', 'password', 'account'], 'safe'],
            [['phone', 'phone_add', 'phone_guarantor'], 'string', 'max' => 15],
            [
                ['email', 'foto', 'FIO_guarantor', 'phone_model', 'phone_id', 'specialization', 'other_data'],
                'string',
                'max' => 255
            ],
            [['pasport_number', 'pasport_publish', 'code_structure'], 'string', 'max' => 20],
            [['category_id'], 'default', 'value' => 1],
            [
                ['category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EmployeesCategory::className(),
                'targetAttribute' => ['category_id' => 'id']
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EmployeesPosition::className(),
                'targetAttribute' => ['position_id' => 'id']
            ],
            [['phone_id'], 'required'],
            ['account', 'default', 'value' => 0],
            ['new_password', 'string'],
            [['login', 'password'], 'required', 'when' => function($model){
                return $model->account == 1;
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Менеджер',
            'status' => 'Статус',
            'category_id' => 'Категория',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'parentname' => 'Отчество',
            'birthday' => 'Дата рождения',
            'address' => 'Адрес проживания',
            'family_status' => 'Семейное положение',
            'phone' => 'Телефон',
            'phone_guarantor' => 'Телефон поручителя',
            'position_id' => 'Должность',
            'email' => 'E-mail',
            'camera' => 'Есть фотоапарат?',
            'internet' => 'Есть интернет?',
            'office' => 'Сотрудник офиса',
            'departure_area' => 'Выезд в область',
            'prefers_work' => 'Предпочитает работу',
            'start_work' => 'Дата приема на работу',
            'pasport_number' => 'Серия и номер паспорта',
            'pasport_publish' => 'Кем выдан',
            'pasport_date' => 'Дата выдачи',
            'code_structure' => 'Код подразделения',
            'foto' => 'Фотография',
            'FIO' => 'ФИО',
            'phone_add' => 'Дополнительный телефон',
            'FIO_guarantor' => 'ФИО поручителя',
            'phone_model' => 'Модель смартфона',
            'phone_id' => 'ИД смартфона',
            'deposit' => 'Депозит',
            'specialization' => 'Специализация',
            'specializations' => 'Специализация',
            'other_data' => 'Прочие данные',
            'children' => 'Дети',
            'age' => 'Возраст',
            'login' => 'Логин',
            'password' => 'Пароль',
            'account' => 'Аккаунт',
            'new_password' => 'Новый пароль',
            'token' => 'Токен',
            'device_token' => 'FCM токен смартфона',
        ];
    }

    /**
     * Возвращает ФИО
     * @return string
     */
    public function getFullName()
    {
        return $this->lastname . ' ' . $this->firstname . ' ' . $this->parentname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(EmployeesCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(EmployeesPosition::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeesAttaches()
    {
        return $this->hasMany(EmployeesAttach::className(), ['emloyees_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_ON_HOLIDAY => 'В отпуске',
            self::STATUS_ON_SICK_LEAVE => 'На больничном',
            self::STATUS_FIRED => 'Уволен',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    public static function getStatusName($status)
    {
        switch ($status) {
            case self::STATUS_IN_WORK :
                return 'В работе';
            case self::STATUS_ON_HOLIDAY:
                return 'В отпуске';
            case self::STATUS_ON_SICK_LEAVE:
                return 'На больничном';
            case self::STATUS_FIRED:
                return 'Уволен';
            default:
                return '';
        }
    }

    public function getCurrentStatus()
    {
        switch ($this->status) {
            case self::STATUS_IN_WORK :
                return 'В работе';
            case self::STATUS_ON_HOLIDAY:
                return 'В отпуске';
            case self::STATUS_ON_SICK_LEAVE:
                return 'На больничном';
            case self::STATUS_FIRED:
                return 'Уволен';
            default:
                return "Неизвестно";
        }
    }

    public static function getFamilyStatuses()
    {
        return [
            self::FAMILY_UNMARRIED => 'Холост(не замужем)',
            self::FAMILY_MARRIED => 'Женат(Замужем)',
            self::FAMILY_DIVORCED => 'Разведен(а)',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    public static function getFamilyStatusName($status)
    {
        switch ($status) {
            case self::FAMILY_UNMARRIED :
                return 'Холост(не замужем)';
            case self::FAMILY_MARRIED:
                return 'Женат(Замужем)';
            case self::FAMILY_DIVORCED:
                return 'Разведен(а)';
            default:
                return "Неизвестно";
        }
    }

    public function getCurrentFamilyStatus()
    {
        switch ($this->family_status) {
            case self::FAMILY_UNMARRIED :
                return 'Холост(Не замужем)';
            case self::FAMILY_MARRIED:
                return 'Женат(Замужем)';
            case self::FAMILY_DIVORCED:
                return 'Разведен(а)';
            default:
                return "Неизвестно";
        }
    }

    public function getFIO()
    {
        return $this->lastname . ' ' . $this->firstname . ' ' . $this->parentname;
    }

    /**
     * @param $property
     * @param $category
     * @throws \yii\base\Exception
     */
    public function saveAttach($property, $category)
    {
        $this->$property = UploadedFile::getInstances($this, $property);
        $path = $category == 1 ? "images/passports/" : "images/otherdoc/";
        if (!empty($this->$property)) {
            if (!file_exists(($path))) {
                mkdir($path, 0777, true);
            }
            foreach ($this->$property as $uploadedFile) {
                $current_name = $path . time() . Yii::$app->security->generateRandomString(5) . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs($current_name);
                $attach = new EmployeesAttach();
                $attach->employees_id = $this->id;
                $attach->category_file = $category;
                $attach->file = "/" . $current_name;
                $attach->save();
            }
        }
    }

    /**
     * @throws \yii\base\Exception
     */
    public function saveAttachPassport()
    {
        $this->saveAttach('pasport_files', 1);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function saveAttachOther()
    {
        $this->saveAttach('other_files', 2);
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function saveChildren()
    {
        $children = EmployeesChildren::find()->where(['employees_id' => $this->id])->all();
        foreach ($children as $child) {
            $child->delete();
        }
        foreach ($this->children as $child) {
            $model = new EmployeesChildren();
            $model->employees_id = $this->id;
            $model->name = $child['name'];
            $model->birthday = $child['birthday'];
            $model->sex = $child['sex'];
            $model->save();
        }

    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->category_id = 1;

        }
        $changeStatus = $this->isAttributeChanged('status');
        if ($this->token == '') {
            $this->token = Yii::$app->security->generateRandomString();

        }

        //Соеденить специализации в строку
        $this->specialization = implode(',', $this->specializations);

        $res = parent::beforeSave($insert);

        // Если статус менялся добавить в историю статусов
        if ($changeStatus) {
            $statues = new EmployeesStatus();
            $statues->employees_id = $this->id;
            $statues->status = $this->status;
            $statues->date_status = date('Y-m-d');
            $statues->save();
        }

        //При наличии нового пароля меняем старый на новый
        if ($this->new_password) $this->password = md5($this->new_password);

        //Если аккаунта нет, но есть пароль (пароль ввел пользователь) - шифруем пароль перед записью
        if ($this->password && !$this->account) {
            $this->password = md5($this->password);
        }

        If ($this->login && $this->password) {
            $this->account = 1;
        } else {
            $this->account = 0;
        }

        return $res;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->specializations = explode(',', $this->specialization);
    }

    public function beforeDelete()
    {
        EmployeesAttach::deleteAll(['employees_id' => $this->id]);
        EmployeesChildren::deleteAll(['employees_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function getAllChildren()
    {
        $children = EmployeesChildren::find()->where(['employees_id' => $this->id])->all();
        if (!$children) {
            return 'Нет';
        }
        $allChildren = '';
        foreach ($children as $child) {
            $allChildren .= $child->name . ',';
        }
        return $allChildren;
    }

    public function getAllSpecializaton()
    {
        $allSpecialization = '';
        if (!$this->specializations) {
            return 'Нет';
        }
        foreach ($this->specializations as $sp) {
            $allSpecialization .= static::$specializationList[$sp] . ',';
        }
        return $allSpecialization;
    }

    public function getAge()
    {
        $now = new DateTime();
        $date = DateTime::createFromFormat("Y-m-d", $this->birthday);
        $interval = $now->diff($date);
        return $interval->y;
    }

    public static function getEmloyeesList()
    {
        $employees = static::find()->all();
        $employeesList = [];
        foreach ($employees as $employee) {
            $employeesList[$employee->id] = $employee->lastname . ' ' . $employee->firstname;
        }
        return $employeesList;
    }

    public static function getRouteFromPeriod($employee, $startDate = null, $endDate = null)
    {

        if (!$endDate) {
            $endDate = date('Y-m-d', time());
        }

        if (!$startDate) {
            $startDate = date('Y-m-d', time());
        }

        return Routes::find()->select(['id', 'name'])->where(['employees_id' => $employee])->where([
            'between',
            'start_date',
            $startDate,
            $endDate
        ])
            ->asArray()->all();
    }

    public static function login($login, $password, $phone_id)
    {
        $e = Employees::find()->where([
            'login' => $login,
            'password' => md5($password),
            'phone_id' => $phone_id
        ])->one();
        return $e ? $e->token : false;
    }

    /**
     * Получает модель сотрудника по токену
     * @param string $token Токен сотрудник
     * @return array|bool|null|ActiveRecord
     */
    public static function getEmployeeByToken($token)
    {
        $e = Employees::find()->where(['token' => $token])->one();
        return $e ? $e : false;
    }

    /**
     * Генерирует логин для сотрудника
     * @param int $id ID сотрудника
     * @return string
     * @throws \yii\base\Exception
     */
    public function getLogin($id = 0)
    {

        if ($id) {
            $model = Employees::findOne($id);
        } else {
            $model = $this;
        }

        $n = mb_strtoupper(mb_substr($model->firstname, 0, 1)); //Получаем первую букву имени
        $mn = mb_strtoupper(mb_substr($model->parentname, 0, 1)); //Первую букву отчества
        $short_name = $model->lastname . $n . $mn;

        $login = Inflector::slug($short_name);
        if (!$login) {
            //Если не сработала транслитерация. (если на сервере не включен php_intl)
            $login = $model->id . '_' . Yii::$app->security->generateRandomString(6);
        }

        return $login;
    }

    /**
     * @param string $login Логин
     * @param string $pass Пароль
     * @param string $phone ID смартфона
     * @return null|string
     */
    public static function addEmployee($login, $pass, $phone)
    {
        $login_exist = Employees::find()
            ->andWhere(['login' => $login])
            ->exists();

        if ($login_exist) return 'login already in use';

        $phone_exist = Employees::find()
            ->andWhere(['phone_id' => $phone])
            ->exists();

        if ($phone_exist) return 'phone id already in use';

        $model = new Employees();
        $model->login = $login;
        $model->password = $pass;
        $model->phone_id = $phone;

        if ($model->save()){
            return null;
        } else {
            Yii::error($model->errors, '_api_error');
            return 'internal error';
        }
    }
}
