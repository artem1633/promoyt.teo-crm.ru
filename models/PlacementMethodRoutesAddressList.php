<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placement_method_routes_address_list".
 *
 * @property int $placement_method_id
 * @property int $routes_address_list_id
 *
 * @property PlacementMethod $placementMethod
 * @property RoutesAddressList $routesAddressList
 */
class PlacementMethodRoutesAddressList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placement_method_routes_address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['placement_method_id', 'routes_address_list_id'], 'required'],
            [['placement_method_id', 'routes_address_list_id'], 'integer'],
            [['placement_method_id', 'routes_address_list_id'], 'unique', 'targetAttribute' => ['placement_method_id', 'routes_address_list_id']],
            [['placement_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlacementMethod::class, 'targetAttribute' => ['placement_method_id' => 'id']],
            [['routes_address_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoutesAddressList::class, 'targetAttribute' => ['routes_address_list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'placement_method_id' => 'Placement Method ID',
            'routes_address_list_id' => 'Routes Address List ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacementMethod()
    {
        return $this->hasOne(PlacementMethod::class, ['id' => 'placement_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutesAddressList()
    {
        return $this->hasOne(RoutesAddressList::class, ['id' => 'routes_address_list_id']);
    }
}
