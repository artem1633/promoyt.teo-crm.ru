<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "project_address".
 *
 * @property int $id
 * @property int $project_id Проект
 * @property int $address_id Адрес
 * @property int $zone_id Зона
 * @property int $entrance К-во подъездов
 * @property int $apartament К-во квартир
 * @property int $floor К-во этажей
 * @property int $porter К-во вахт
 * @property int $type Тип
 * @property int $comment Комментарий
 *
 * @property AddressList $address
 * @property Projects $project
 * @property Zones $zone
 */
class ProjectAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'address_id', 'zone_id', 'entrance', 'apartament', 'floor', 'porter', 'type'], 'integer'],
            [['comment'],'string'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressList::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['zone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zones::className(), 'targetAttribute' => ['zone_id' => 'id']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'address_id' => 'Адрес',
            'zone_id' => 'Зона',
            'entrance' => 'К-во подъездов',
            'apartament' => 'К-во квартир',
            'floor' => 'К-во этажей',
            'porter' => 'К-во вахт',
            'type' => 'Тип',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        if($this->zone_id != null){
            $zone = Zones::findOne($this->zone_id);
            $zone->objects_count--;
            $zone->count_entrance = $zone->count_entrance - $this->entrance;
            $zone->count_floor = $zone->count_floor - $this->floor;
            $zone->count_apartament = $zone->count_apartament - $this->apartament;
            $zone->save(false);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(AddressList::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZone()
    {
        return $this->hasOne(Zones::className(), ['id' => 'zone_id']);
    }
}
