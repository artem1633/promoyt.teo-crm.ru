<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects_placement".
 *
 * @property int $id
 * @property int $placement_id Метод
 * @property int $price_id Тип цены
 * @property string $price Цена
 * @property int $circulation Тираж
 * @property int $project_id Проект
 *
 * @property PlacementMethod $placement
 * @property Projects $project
 */
class ProjectsPlacement extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects_placement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['placement_id', 'price_id', 'circulation', 'project_id'], 'integer'],
            [['price'], 'number'],
            [['placement_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlacementMethod::className(), 'targetAttribute' => ['placement_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'placement_id' => 'Метод',
            'price_id' => 'Тип цены',
            'price' => 'Цена',
            'circulation' => 'Тираж',
            'project_id' => 'Проект',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacement()
    {
        return $this->hasOne(PlacementMethod::className(), ['id' => 'placement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }
}
