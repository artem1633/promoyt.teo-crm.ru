<?php

namespace app\models;

use app\source\excelReaders\ActiveRecordExcelReader;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "address_list".
 *
 * @property int $id
 * @property string $country Страна
 * @property string $oblast Область
 * @property string $region Район
 * @property string $town Город
 * @property string $okrug Округ
 * @property string $okrug_region Район
 * @property string $street Улица
 * @property string $house Дом
 * @property string $korpus Корпус
 * @property string $coord_x Долгота
 * @property string $coord_y Широта
 * @property int $floor К-во етажей
 * @property int $apartament К-во квартир
 * @property int $entrance К-во подьездов
 * @property int $type Тип здания
 *
 * @property string $fullAddress Полный адрес
 *
 * @property RouteAddress[] $routeAddresses
 * @property RoutesAddressList[] $routesAddressLists
 * @property Routes[] $routes
 */
class AddressList extends ActiveRecord
{
    const TYPE_RESIDENTIAL = 0;
    const TYPE_MOMRESIDENTIAL = 1;
    const TYPE_COMMERCIAL = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coord_x', 'coord_y'], 'number'],
            [['floor', 'apartament', 'entrance', 'type','porter'], 'integer'],
            [['country','region', 'town', 'street', 'house','housing'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Страна',
            'region' => 'Район',
            'town' => 'Город',
            'street' => 'Улица',
            'house' => '№ Дома',
            'housing' => 'Строение',
            'coord_x' => 'Долгота',
            'coord_y' => 'Широта',
            'floor' => 'К-во этажей',
            'apartament' => 'К-во квартир',
            'entrance' => 'К-во подъездов',
            'type' => 'Тип здания',
            'porter' => 'К-во вахт',
        ];
    }

    /**
     * Создает записи текущей модели в БД из excel-файла
     * Возвращает кол-во созданых записей
     * @param string $file Путь до файла
     * @return array
     */
    public static function createFromExcelData($file)
    {
        return ActiveRecordExcelReader::importToMainAddress(get_called_class(), $file);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectAddresses()
    {
        return $this->hasMany(ProjectAddress::className(), ['address_id' => 'id']);
    }

    /**
     * Полный адрес
     * @return string
     */
    public function getFullAddress()
    {
        return "{$this->street}, {$this->house}, {$this->housing}, {$this->town}, {$this->region}";
    }

    public static function getTypes()
    {
        $types = [];
        $types[self::TYPE_RESIDENTIAL] = 'Жилой';
        $types[self::TYPE_MOMRESIDENTIAL] = 'Не жилой';
        $types[self::TYPE_COMMERCIAL] = 'Коммерческий';
        return $types;

    }

    /**
     * @param int $type
     * @return string
     */
    public function getTypeName($type)
    {
        switch ($type) {
            case self::TYPE_RESIDENTIAL: return "Жилой";
            case self::TYPE_MOMRESIDENTIAL: return "Не жилой";
            case self::TYPE_COMMERCIAL: return "Коммерческий";
            default: return "Неизвестно";
        }
    }
    
    /**
     * @return string
     */
    public function getCurrentType()
    {
        switch ($this->type) {
            case self::TYPE_RESIDENTIAL: return "Жилой";
            case self::TYPE_MOMRESIDENTIAL: return "Не жилой";
            case self::TYPE_COMMERCIAL: return "Коммерческий";
            default: return "Неизвестно";
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteAddresses()
    {
        return $this->hasMany(RouteAddress::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRoutes()
    {
        return $this->hasMany(Routes::className(), ['id' => 'routes_id'])->viaTable('routes_address_list', ['address_list_id' => 'id']);
    }
}
