<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RouteAddress;

/**
 * RouteAddressSearch represents the model behind the search form about `app\models\RouteAddressSearch`.
 */
class RouteAddressSearch extends RouteAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'address_id', 'fine_id', 'route_id', 'fine_percent', 'fine_comment'], 'integer'],
            [['plane_work', 'fact_work', 'confirm_work'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RouteAddress::find()->joinWith('address');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'address_id' => $this->address_id,
            'fine_id' => $this->fine_id,
            'route_id' => $this->route_id,
            'fine_percent' => $this->fine_percent,
            'fine_comment' => $this->fine_comment,
            'plane_work' => $this->plane_work,
            'fact_work' => $this->fact_work,
            'confirm_work' => $this->confirm_work,
        ]);

        return $dataProvider;
    }
}
