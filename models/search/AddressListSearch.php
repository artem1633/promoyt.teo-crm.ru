<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddressList;

/**
 * AddressList represents the model behind the search form about `app\models\AddressList`.
 */
class AddressListSearch extends AddressList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coord_x', 'coord_y'], 'number'],
            [['floor', 'apartament', 'entrance', 'type','porter'], 'integer'],
            [['country','region', 'town', 'street', 'house','housing'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressList::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'coord_x' => $this->coord_x,
            'coord_y' => $this->coord_y,
            'floor' => $this->floor,
            'apartament' => $this->apartament,
            'entrance' => $this->entrance,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'town', $this->town])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house]);

        return $dataProvider;
    }
}
