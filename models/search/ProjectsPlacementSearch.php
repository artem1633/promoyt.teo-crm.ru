<?php

namespace app\models\search;

use app\models\PlacementMethod;
use app\models\Projects;
use app\models\ProjectsPlacement;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PlacementSearch represents the model behind the search form about `app\models\PlacementMethod`.
 */
class ProjectsPlacementSearch extends ProjectsPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['placement_id', 'price_id', 'circulation', 'project_id'], 'integer'],
            [['price'], 'number'],
            [['placement_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlacementMethod::className(), 'targetAttribute' => ['placement_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectsPlacement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'placement_id' => $this->placement_id,
            'price_id' => $this->price_id,
        ]);

        $query->andFilterWhere(['>', 'circulation', $this->circulation]);

        return $dataProvider;
    }
}
