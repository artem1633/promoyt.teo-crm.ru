<?php
namespace app\models\search;

use app\models\PlacementMethod;
use app\models\Routes;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class EmployeesPaysSearch
 * @package app\models
 */
class EmployeesPaysSearch extends Model
{
    /**
     * @var int
     */
    public $projectId;

    /**
     * @var int
     */
    public $employeeId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['projectId', 'employeeId'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $placements = PlacementMethod::find()->all();
        $query = (new Query())
                    ->select('
                    projects.id as project_id,
                    projects.name as project_name,
                    SUM(routes.fact) as fact_count,
                    SUM(routes.completed_count) as completed_count,
                    SUM(routes.fines_sum) as fines_count,
                    SUM(project_employee_payments.amount) as payment_amount')
                    ->from('projects')
                    ->leftJoin('routes', 'routes.project_id=projects.id')
                    ->leftJoin('project_employee_payments', 'project_employee_payments.project_id=projects.id');

        $dataProvider = new ArrayDataProvider();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->load($params);

        $query->andFilterWhere(['projects.id' => $this->projectId]);
        $query->andFilterWhere(['routes.employees_id' => $this->employeeId]);
        $query->andFilterWhere(['project_employee_payments.employee_id' => $this->employeeId]);


        $data = $query->all();

//        try {
//
//            throw new \Exception('asdas');
//
//        } catch(\Exception $e){
//
//        }

        for ($i = 0; $i < count($data); $i++)
        {
            $routes = Routes::find()->where(['project_id' => $data[$i]['project_id'], 'employees_id' => $this->employeeId])->all();

            // Считаем сумму подтвержденного тиража по категориям размещения
            foreach ($placements as $placement)
            {
                $placementRoutes = array_filter($routes, function($route) use($placement){
                    return $route->placement_id === $placement->id;
                });

                $data[$i]['p-'.$placement->id] = array_sum(ArrayHelper::getColumn($placementRoutes, 'completed_count'));
            }

            // Считаем общее кол-во рабочих часов
            $workHours = 0;
            foreach ($routes as $route)
            {
                /** @var Routes $route */
                if($route->start_date == null || $route->fact_date == null){
                    continue;
                }
                $start = strtotime($route->start_date);
                $end = strtotime($route->fact_date);

                $diff = $end - $start;
                $diff = round($diff / 60) - $route->length_of_pause;
                $workHours += round($diff / 60, 1);
            }

            $data[$i]['workHours'] = $workHours;
        }

        $dataProvider->models = $data;

        return $dataProvider;
    }

}