<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProjectAddress;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * ProjectAddressSearch represents the model behind the search form about `app\models\ProjectAddress`.
 */
class ProjectAddressSearch extends ProjectAddress
{
    public $address_street;
    public $address_region;
    public $zone_name;
    public $address_town;
    public $address_house;
    public $address_housing;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'address_id', 'zone_id', 'entrance', 'apartament', 'floor', 'porter', 'type', 'comment'], 'integer'],
            [['address_street', 'address_region', 'address_house', 'zone_name', 'address_housing'], 'safe'],
            [['address_town'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->select([
                'address_list.town as address_town',
                'address_list.region as address_region',
                'address_list.house as address_house',
                'address_list.housing as address_housing',
                'project_address.*'
            ])->from('project_address');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->joinWith(['address', 'zone']);

        $query->leftJoin('address_list', 'project_address.address_id=address_list.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'project_address.project_id' => $this->project_id,
            'project_address.address_id' => $this->address_id,
            'project_address.zone_id' => $this->zone_id,
            'project_address.entrance' => $this->entrance,
            'project_address.apartament' => $this->apartament,
            'project_address.floor' => $this->floor,
            'project_address.porter' => $this->porter,
            'project_address.type' => $this->type,
            'project_address.comment' => $this->comment,
        ]);

        $query->andFilterWhere(['like', 'address_list.street', $this->address_street]);
        $query->andFilterWhere(['like', 'address_list.region', $this->address_region]);
        // $query->andFilterWhere(['like', 'address_list.region', $this->address_region]);
        $query->andFilterWhere(['like', 'address_list.town', $this->address_town]);
        $query->andFilterWhere(['like', 'address_list.house', $this->address_house]);
        $query->andFilterWhere(['like', 'address_list.housing', $this->address_housing]);
        $query->andFilterWhere(['like', 'zones.name', $this->zone_name]);

        return $dataProvider;
    }
}
