<?php

namespace app\models\search;

use app\models\Clients;
use app\models\Projects;
use app\models\Users;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Routes;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * RoutesSearch represents the model behind the search form about `app\models\Routes`.
 */
class RoutesSearch extends Routes
{

    public $clientname;
    public $projectid;
    public $projectname;
    public $placementmethod;
    public $circulation;
    public $routeperiod;
    public $zone_name;
    public $town;
    public $region;
    public $employees;
    public $routeName;
    public $placement_method_name;
    public $routeperiodfact;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_route', 'duplicated', 'category_id', 'employees_id', 'project_id', 'count_adress', 'fact_count_adress', 'projectid'], 'integer'],
            [['start_date', 'fact_date', 'video', 'comment_employees', 'comment_manager'], 'safe'],
            [['clientname', 'projectname', 'placementmethod', 'circulation', 'routeperiod', 'zone_name', 'town', 'region', 'employees', 'routeName', 'placement_method_name', 'routeperiodfact', 'maket'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->select([
                'routes.*',
                'projects.name as projectname',
                'clients.name as clientname',
                'zones.name as zone_name',
            ])->from('routes');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->leftJoin('projects', 'routes.project_id = projects.id');
        $query->leftJoin('clients', 'projects.client_id = clients.id');
        $query->leftJoin('zones','zones.id = routes.zone_id');

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'projects.name', $this->projectname])
            ->andFilterWhere(['like', 'clients.name', $this->clientname])
            ->andFilterWhere(['like', 'routes.maket', $this->maket])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        if(Yii::$app->user->identity->role == Users::USER_TYPE_CLIENT){
            $clients = ArrayHelper::getColumn(Yii::$app->user->identity->clientsOrganisations, 'id');
            $projects = ArrayHelper::getColumn(Projects::find()->where(['client_id' => $clients])->all(), 'id');
            $query->andWhere(['project_id' => $projects]);
        }

        return $dataProvider;
    }

    public function searchReportPlaneWork($params)
    {
        $query = (new Query())
            ->select(
                [
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'zones.name as zone_name',
                    'routes.status_route as status',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'projects.name as projectname',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'placement_method.name as placementmethod',
                    'routes.normal as circulation'

                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('zones','zones.id = routes.zone_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'routes.project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'zones.name', $this->zone_name])
            ->andFilterWhere(['like', 'routes.town', $this->town])
            ->andFilterWhere(['like', 'routes.region', $this->region])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        if(Yii::$app->user->identity->role == Users::USER_TYPE_CLIENT){
            $clients = ArrayHelper::getColumn(Yii::$app->user->identity->clientsOrganisations, 'id');
            $projects = ArrayHelper::getColumn(Projects::find()->where(['client_id' => $clients])->all(), 'id');
            $query->andWhere(['routes.project_id' => $projects]);
        }

        return $dataProvider;
    }

    public function searchReportAssignedGrafik($params)
    {
        $query = (new Query())
            ->select(
                [
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'routes.status_route as status',
                    'zones.name as zone_name',
                    'routes.comment_employees as comment_employees',
                    'routes.comment_manager as comment_manager',
                    'routes.employees_id as employees_id',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'projects.name as projectname',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'placement_method.name as placementmethod',
                    'routes.normal as circulation',
                    'employees.firstname as employees',



                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('employees','employees.id=routes.employees_id')
            ->leftJoin('zones','zones.id = routes.zone_id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'projects.id' => $this->projectid,
            'employees_id' => $this->employees_id,
            'routes.project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'clients.name', $this->clientname])
            ->andFilterWhere(['like', 'projects.name', $this->projectname])
            ->andFilterWhere(['like', 'placement_method.name', $this->placementmethod])
            ->andFilterWhere(['like', 'zones.name', $this->zone_name])
            ->andFilterWhere(['like', 'routes.normal', $this->circulation])
            ->andFilterWhere(['like', 'employees.firstname', $this->employees])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        if(Yii::$app->user->identity->role == Users::USER_TYPE_CLIENT){
            $clients = ArrayHelper::getColumn(Yii::$app->user->identity->clientsOrganisations, 'id');
            $projects = ArrayHelper::getColumn(Projects::find()->where(['client_id' => $clients])->all(), 'id');
            $query->andWhere(['routes.project_id' => $projects]);
        }

        return $dataProvider;
    }

    public function searchReportFact($params)
    {
        $query = (new Query())
            ->select(
                [
                    'route_address.fact_n as fact_n',
                    'route_address.fact_porch as fact_porch',
                    'route_address.fact_store as fact_store',
                    'route_address.fact_flat as fact_flat',
                    'route_address.fact_enter as fact_enter',
                    'route_address.confirm_n as confirm_n',
                    'route_address.confirm_porch as confirm_porch',
                    'route_address.confirm_store as confirm_store',
                    'route_address.confirm_flat as confirm_flat',
                    'route_address.confirm_enter as confirm_enter',
                    'address_list.country as address_country',
                    'address_list.town as address_town',
                    'address_list.street as address_street',
                    'address_list.house as address_house',
                    'address_list.housing as address_housing',
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'routes.status_route as status',
                    'routes.normal as circulation',
//                    'routes.fact as factcirculation',
                    'SUM(route_address.confirm_work) as factcirculation',
                    'SUM(route_address.payment) as payment',
                    'zones.name as zone_name',
                    'routes.fact_count_address as factcountaddress',
                    'routes.count_adress as countaddress',
                    'routes.name as route_name',
                    'routes.id as route_id',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'CONCAT(routes.start_date,"-",routes.fact_date) as routeperiodfact',
                    'projects.name as projectname',
                    'placement_method.name as placement_method_name',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'employees.firstname as employees',
                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('employees','employees.id=routes.employees_id')
            ->leftJoin('zones','zones.id = routes.zone_id')
            ->leftJoin('route_address','route_address.route_id = routes.id')
            ->leftJoin('address_list','route_address.address_id = address_list.id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $query->groupBy('routes.id');
//        $query->groupBy('route_address.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'routes.project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'zones.name', $this->zone_name])
            ->andFilterWhere(['like', 'routes.name', $this->routeName])
            ->andFilterWhere(['like', 'placement_method.name', $this->placement_method_name])
            ->andFilterWhere(['or', ['like', 'routes.start_date', $this->routeperiodfact], ['like', 'routes.fact_date', $this->routeperiodfact]])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        $query->orderBy('routes.id desc, routes.id desc');

        if(Yii::$app->user->identity->role == Users::USER_TYPE_CLIENT){
            $clients = ArrayHelper::getColumn(Yii::$app->user->identity->clientsOrganisations, 'id');
            $projects = ArrayHelper::getColumn(Projects::find()->where(['client_id' => $clients])->all(), 'id');
            $query->andWhere(['routes.project_id' => $projects]);
        }

        return $dataProvider;
    }
}
