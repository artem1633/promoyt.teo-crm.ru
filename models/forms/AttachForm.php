<?php

namespace app\models\forms;

use app\models\EmployeesAttach;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class AttachForm extends Model
{
    public $files;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
//        return [
//            ['files', 'file', 'maxSize => 4096000'],
//        ];
    }

    public function saveAttach($category,$id)
    {
        $this->files = UploadedFile::getInstances($this, 'files');
        $path = $category==1 ? "images/passports/" : "images/otherdoc/";
        if (!empty($this->files)) {
            if (!file_exists(($path))) {
                mkdir($path, 0777, true);
            }
            foreach ($this->files as $uploadedFile) {
                $current_name = $path . time() . Yii::$app->security->generateRandomString(5) . '.' . $uploadedFile->extension;
                $uploadedFile->saveAs($current_name);
                $attach = new EmployeesAttach();
                $attach->employees_id = $id;
                $attach->category_file = $category;
                $attach->file = "/" . $current_name;
                $attach->save();
            }

        }
    }
}
