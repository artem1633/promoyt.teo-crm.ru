<?php

namespace app\models\forms;

use yii\base\Model;


class SelectDateTime extends Model
{
    public $dateTime;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['dateTime', 'datetime'],
        ];
    }
}
