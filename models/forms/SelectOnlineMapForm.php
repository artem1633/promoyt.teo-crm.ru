<?php

namespace app\models\forms;

use yii\base\Model;


class SelectOnlineMapForm extends Model
{
    public $filteremployee;
    public $filterproject;
    public $filterclient;
    public $filterdate;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['filteremployee','filterproject','filterclient'], 'integer'],
            ['filterdate', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
        'filteremployee' => 'Сотрудник',
        'filterproject' => 'Проект',
        'filterclient' => 'Заказчик',
        'filterdate' => 'Дата',
        ];
    }
}
