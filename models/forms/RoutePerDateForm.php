<?php

namespace app\models\forms;

use app\models\EmployeesAttach;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class RoutePerDateForm extends Model
{
    public $start_date ;
    public $end_date;
    public $route;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
//        return [
//            ['files', 'file', 'maxSize => 4096000'],
//        ];
    }

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }

    function attributeLabels()
    {
     return [
         'start_date' =>'Начало периода',
         'end_date' =>'Конец периода',
         'route' => 'Маршрут',
     ];
    }
}
