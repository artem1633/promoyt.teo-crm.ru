<?php

namespace app\models\forms;

use app\models\Employees;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\models\forms
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'maxSize' => 1024*1024*1024*2],

        ];
    }

    /**
     * @param $tmp_name
     * @return bool
     */
    public function upload($tmp_name)
    {
        Yii::info('this UploadForm', 'test');
        if ($this->validate()) {
            if (!file_exists('uploads')) {
                mkdir('uploads');
            }
            if (!file_exists('uploads/excel')) {
                mkdir('uploads/excel');
            }
            $this->file->saveAs('uploads/excel/' . $tmp_name . '.' . $this->file->extension);
            return true;
        } else {
            Yii::error($this->errors, 'error');
            return false;
        }
    }

    /**
     * @param string $tmp_name Имя файла
     * @param Employees $employee
    //     * @param int $type Тип файла (0 - Паспорт, 1 - Сотрудник, 2 - Прочее)
     * @return bool
     */
    public function employeeUpload($tmp_name, $employee)
    {
        Yii::info('this UploadForm', 'test');
        if ($result_validate = $this->validate()) {
            Yii::info('Валидация прйодена: ' . $result_validate, 'test');

            $path_dir = 'uploads/employees/' . $employee->id;

            if (!file_exists($path_dir)) {
                $create_dir = mkdir($path_dir, 0777, true);
                Yii::info('Директория создана: ' . $create_dir, 'test');
                Yii::info('Права на созданную директорию: ' . substr(decoct(fileperms($path_dir)), -3), 'test');

            } else {
                Yii::info('Директория уже в наличии. ' . $path_dir, 'test');
            }

            $result_save_file = $this->file->saveAs($path_dir . '/' . $tmp_name . '.' . $this->file->extension);
            Yii::info('Файл сохранен: ' . $result_save_file, 'test');
            if ($this->hasErrors()) {
                Yii::error($this->errors, 'error');
            }
            return true;
        } else {
            Yii::error($this->errors, 'error');
            return false;
        }
    }

    /**
     * @param string $path_dir Путь к папке
     * @param string $file_name имя файла
     * @return bool
     */
    public function videoUpload($path_dir, $file_name)
    {
        Yii::info('this video UploadForm', 'test');
        Yii::info($_FILES, 'test');
        Yii::info($this->toArray(), 'test');

        if ($result_validate = $this->validate()) {
            Yii::info('Валидация пройдена', 'test');

            if (!file_exists($path_dir)) {
                $create_dir = mkdir($path_dir, 0777, true);
                Yii::info('Директория создана: ' . $create_dir, 'test');
                Yii::info('Права на созданную директорию: ' . substr(decoct(fileperms($path_dir)), -3), 'test');
            } else {
                Yii::info('Директория уже в наличии. ' . $path_dir, 'test');
            }

            $result_save_file = $this->file->saveAs($path_dir . '/' . $file_name . '.' . $this->file->extension);
            Yii::info('Файл сохранен: ' . $result_save_file, 'test');
            if ($this->hasErrors()) {
                Yii::error($this->errors, 'error');
            }
            return true;
        } else {
            Yii::info('Валидация не пройдена', 'test');
            Yii::error($this->errors, 'error');
            return false;
        }
    }
}