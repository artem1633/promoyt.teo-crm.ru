<?php

namespace app\models\forms;

use yii\base\Model;


class SelectEmployeesForm extends Model
{
    public $employee;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['employee', 'integer'],
        ];
    }
}
