<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Class ProgectCopy
 * @package app\models\forms
 */
class ProgectCopy extends Model
{

    public $placementMethod;
    public $addresslist;
    public $zones;
    public $routes;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['placementMethod','addresslist','zones','routes'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'placementMethod' => 'Копировать типы работ',
            'addresslist' => 'Копировать адреса',
            'zones' => 'Копировать зоны',
            'routes' => 'Копировать маршруты',
        ];
    }
}