<?php

namespace app\models\forms;

use yii\base\Model;


class InputValueForm extends Model
{
    public $inputValue;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['inputValue', 'required'],
        ];
    }
}
