<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "placement_method".
 *
 * @property int $id
 * @property string $name Название
 * @property Routes[] $routes
 *
 * @property PlacementMethodRoutesAddressList[] $placementMethodRoutesAddressLists
 * @property RoutesAddressList[] $routesAddressLists
 * @property ProjectsPlacement[] $projectsPlacements
 * @property RoutesPlacementMethod[] $routesPlacementMethods
 */
class PlacementMethod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placement_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutesPlacementMethods()
    {
        return $this->hasMany(RoutesPlacementMethod::class, ['placement_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacementMethodRoutesAddressLists()
    {
        return $this->hasMany(PlacementMethodRoutesAddressList::class, ['placement_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutesAddressLists()
    {
        return $this->hasMany(RoutesAddressList::class, ['id' => 'routes_address_list_id'])->viaTable('placement_method_routes_address_list', ['placement_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Routes::class, ['id' => 'routes_id'])->viaTable('routes_placement_method', ['placement_method_id' => 'id']);
    }


    public static function getPlacementMethodList()
    {
        return ArrayHelper::map(static::find()->all(),'id','name');
    }
}
