<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $name Клиент
 * @property string $phone Телефон
 *
 * @property Users $clientUser
 * @property Projects[] $projects
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Клиент',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return array
     */
    public static function getClientList()
    {
        if(Yii::$app->user->identity->role != User::USER_TYPE_CLIENT) {
            return ArrayHelper::map(static::find()->all(),'id','name');
        } else {

            return ArrayHelper::map(static::find()->where(['client_user_id' => Yii::$app->user->getId()])->all(),'id','name');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'client_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['client_id' => 'id']);
    }

}
