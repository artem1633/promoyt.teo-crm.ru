<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $name Название
 * @property int $client_id Заказчик
 * @property string $date_start Дата начала
 * @property string $date_end Дата окончания
 *
 * @property double $honestyPercent
 * @property int $daysDuration
 *
 * @property Clients $client
 * @property float $project_cost
 * @property ProjectsPlacement[] $projectsPlacements
 * @property Routes[] $routes
 */
class Projects extends ActiveRecord
{
    /**
     * @var string
     */
    public $intervalDates;

    public $placements;

    public $maketFile;

    public $prices;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'normal_one', 'price_id'], 'integer'],
            [['name', 'maket'], 'string', 'max' => 255],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['route_template_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => RouteTemplates::className(),
                'targetAttribute' => ['route_template_id' => 'id']
            ],
            [['intervalDates', 'info_add', 'maket_file', 'date_start', 'date_end', 'prices'], 'safe'],
            //[['maketFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();
    }

    /**
     * @return double
     */
    public function getHonestyPercent()
    {
        $routes = $this->routes;
        $completedCount = array_sum(ArrayHelper::getColumn($routes,'completed_count'));
        $factCountAddress = array_sum(ArrayHelper::getColumn($routes, 'fact_count_adress'));
        return round($completedCount*100/$factCountAddress, 2);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Проeкт',
            'client_id' => 'Заказчик',
            'project_cost' => 'Общая сумма',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'intervalDates' => 'Дата начала — Дата завершения',
            'normal_one' => 'Норма на одного промоутера',
            'maket' => 'Макет',
            'maket_file' => 'Файл макета',
            'info_add' => 'Доплнительная информация',
            'price_id' => 'Цена',
            'prices' => 'Цены',
            'route_template_id' => 'Шаблон маршрутного листа',
        ];
    }

    /**
     * Получить длительность выполнения в днях
     * @return int
     */
    public function getDaysDuration()
    {
        $startTimestamp = strtotime($this->date_start);
        $endTimestamp = strtotime($this->date_end);

        return round(($endTimestamp - $startTimestamp) / 86400);
    }

    /**
     * Получить длительность выполнения в днях
     * @return int
     */
    public function getDaysDurationPeriod($start, $end)
    {
        $startTimestamp = strtotime($start);
        $endTimestamp = strtotime($end);

        return round(($endTimestamp - $startTimestamp) / 86400);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectsPlacements()
    {
        return $this->hasMany(ProjectsPlacement::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Routes::className(), ['project_id' => 'id']);
    }


    /**
     * Связь шаблона маршрута с маршрутом
     * @return \yii\db\ActiveQuery
     */
    public function getRouteTemplates()
    {
        return $this->hasOne(RouteTemplates::className(), ['id' => 'route_template_id']);
    }

    public function savePlacements($id = null)
    {
        if ($id) {
            $model = ProjectsPlacement::find()->where(['id' => $id])->one();
            if (!$model) {
                $model = new ProjectsPlacement();
            }
        } else {
            $model = new ProjectsPlacement();
        }
        $prices = Prices::find()->indexBy('id')->all();
        $all_cost = 0;
        $model->project_id = $this->id;
        $model->placement_id = $this->placements->placement_id;
        $model->price_id = $this->placements->price_id;
        $model->price = $prices[$this->placements->price_id]->price;
        $model->circulation = $this->placements->circulation;
        $all_cost += $model->price * $this->placements->circulation;
        $model->save();
        //$this->project_cost = $all_cost;
        //$this->save();
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        $routes = Routes::find()->where(['project_id' => $this->id])->all();
        foreach ($routes as $route) {
            $route->delete();
        }
        ProjectsPlacement::deleteAll(['project_id' => $this->id]);
        ProjectAddress::deleteAll(['project_id' => $this->id]);
        Zones::deleteAll(['project_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->prices != null){
            PricesProjects::deleteAll(['projects_id' => $this->id]);
            foreach ($this->prices as $price)
            {
                (new PricesProjects(['projects_id' => $this->id, 'prices_id' => $price]))->save();
            }
        }
    }

    /**
     *
     */
    public function getAllAddresses()
    {
        $addresses = [];

        foreach ($this->routes as $route) {
            $adrs = $route->addressLists;
            $addresses = array_merge($addresses, $adrs);
        }

        return $addresses;
    }

    public function getAllRoutes()
    {
        return $this->routes;
    }

    public function createCopy($form, $oldProject)
    {
        if ($form['placementMethod'] == 1) {
            $placementMethods = ProjectsPlacement::find()->where(['project_id' => $oldProject])->all();
            foreach ($placementMethods as $placementMethod) {
                $new = new ProjectsPlacement();
                $new->attributes = $placementMethod->attributes;
                $new->project_id = $this->id;
                $new->save();
            }
        }
        if ($form['addresslist'] == 1) {
            $addresslists = ProjectAddress::find()->where(['project_id' => $oldProject])->all();
            foreach ($addresslists as $addresslist) {
                $new = new ProjectAddress();
                $new->attributes = $addresslist->attributes;
                $new->project_id = $this->id;
                $new->save();
            }
        }
        if ($form['zones'] == 1) {
            $zones = Zones::find()->where(['project_id' => $oldProject])->all();
            foreach ($zones as $zone) {
                $new = new Zones();
                $new->attributes = $zone->attributes;
                $new->project_id = $this->id;
                $new->save();
            }
        }
        if ($form['routes'] == 1 && $form['addresslist'] == 1) {
            $routes = Routes::find()->where(['project_id' => $oldProject])->all();
            foreach ($routes as $route) {
                $new = new Routes();
                $new->attributes = $route->attributes;
                $new->project_id = $this->id;
                $new->save();
                $routeAddress = RouteAddress::find()->where(['route_id' => $route->id])->all();
                foreach ($routeAddress as $address) {
                    $newItem = new RouteAddress();
                    $newItem->attributes = $address->attributes;
                    $newItem->route_id = $new->id;
                    $newItem->save();
                }
            }
        }
    }
}
