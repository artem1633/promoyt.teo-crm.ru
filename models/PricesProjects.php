<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prices_projects".
 *
 * @property int $id
 * @property int $prices_id
 * @property int $projects_id
 *
 * @property Prices $prices
 * @property Projects $projects
 */
class PricesProjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices_projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prices_id', 'projects_id'], 'integer'],
            [['prices_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prices::className(), 'targetAttribute' => ['prices_id' => 'id']],
            [['projects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['projects_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prices_id' => 'Prices ID',
            'projects_id' => 'Projects ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasOne(Prices::className(), ['id' => 'prices_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasOne(Projects::className(), ['id' => 'projects_id']);
    }
}
