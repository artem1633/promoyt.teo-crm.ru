<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees_children".
 *
 * @property int $id
 * @property int $employees_id
 * @property int $sex Пол
 * @property string $name Имя
 * @property string $birthday Дата рождения
 *
 * @property Employees $employees
 */
class EmployeesChildren extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_children';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employees_id', 'sex'], 'integer'],
            [['birthday'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['employees_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employees_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees_id' => 'Employees ID',
            'sex' => 'Пол',
            'name' => 'Имя',
            'birthday' => 'Дата рождения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employees_id']);
    }
}
