<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees_move".
 *
 * @property int $id
 * @property int $employee_id Промоутер
 * @property int $route_id Маршрут
 * @property string $dateandtime Дата и время
 * @property string $coord_x X
 * @property string $coord_y Y
 * @property int $status Статус
 * @property int $timecode
 *
 * @property Employees $employee
 * @property Routes $route
 */
class EmployeesMove extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_move';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'route_id', 'status', 'timecode'], 'integer'],
            [['dateandtime'], 'safe'],
            [['coord_x', 'coord_y'], 'number'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Промоутер',
            'route_id' => 'Маршрут',
            'dateandtime' => 'Дата и время',
            'coord_x' => 'X',
            'coord_y' => 'Y',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }
    public static function getEmployeeRoute($employee,$route)
    {
        return static::find()->where(['employee_id'=>$employee])
        ->where(['route_id'=>$route])->orderBy('dateandtime')->asArray()->all();

    }

    public static function getLastEmployeeMove()
    {
        $employees = Employees::find()->indexBy('id')->asArray()->all();
        $data = static::find()->select(['employee_id', 'MAX(dateandtime)','coord_x','coord_y'])
            ->groupBy(['employee_id'])->asArray()->all();
        foreach ($data as $key=>$item) {
            $data[$key]['status'] = $employees[$item['employee_id']]['status_work'];
        }
        return $data;

    }
}
