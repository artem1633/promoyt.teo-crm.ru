<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username Логин
 * @property string $fio ФИО
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email E-mail
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $role
 *
 * @property Clients[] $clientsOrganisations
 * @property Comments[] $comments
 * @property Comments[] $comments0
 */
class Users extends ActiveRecord
{
    /**
     * @var array
     */
    public $clients;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_MANAGER_PROJECT = 2;
    const USER_TYPE_MANAGER_QUALITY = 3;
    const USER_TYPE_LEADER = 4;
    const USER_TYPE_CLIENT = 5;
    public $new_password;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email'], 'required'],
            [['status', 'created_at', 'updated_at', 'role'], 'integer'],
            [['username', 'fio', 'password', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['clients'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'fio' => 'ФИО',
            'password' => 'Пароль',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'E-mail',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'role' => 'Права',
            'currentRole' => 'Права',
            'new_password' => 'Новий пароль',
            'clients' => 'Клиентские организации'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->password = md5($this->password);
        }

        if($this->new_password != null) $this->password = md5($this->new_password);
        return parent::beforeSave($insert);
    }

    public function isClient()
    {
        return $this->role == Users::USER_TYPE_CLIENT;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->clients){
            Clients::updateAll(['client_user_id' => null], ['client_user_id' => $this->id]);
            Clients::updateAll(['client_user_id' => $this->id], ['id' => $this->clients]);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsOrganisations()
    {
        return $this->hasMany(Clients::className(), ['client_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments0()
    {
        return $this->hasMany(Comments::className(), ['user_to' => 'id']);
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        $roles=[];
        $roles[self::USER_TYPE_ADMIN] = 'Администратор';
        $roles[self::USER_TYPE_MANAGER_PROJECT] = 'Менеджер проекта';
        $roles[self::USER_TYPE_MANAGER_QUALITY] = 'Менеджер по качеству';
        $roles[self::USER_TYPE_LEADER] = 'Руководитель';
        $roles[self::USER_TYPE_CLIENT] = 'Заказчик';
        return $roles;

    }

    /**
     * @param $role
     * @return string
     */
    public function getRoleName($role)
    {
        switch ($role) {
            case self::USER_TYPE_ADMIN: return "Администратор";
            case self::USER_TYPE_MANAGER_PROJECT: return "Менеджер проекта";
            case self::USER_TYPE_MANAGER_QUALITY: return "Менеджер по качеству";
            case self::USER_TYPE_LEADER: return "Руководитель";
            case self::USER_TYPE_CLIENT: return "Заказчик";
            default: return "Неизвестно";
        }
    }

    /**
     * @return string
     */
    public function getCurrentRole()
    {
        switch ($this->role) {
            case self::USER_TYPE_ADMIN: return "Администратор";
            case self::USER_TYPE_MANAGER_PROJECT: return "Менеджер проекта";
            case self::USER_TYPE_MANAGER_QUALITY: return "Менеджер по качеству";
            case self::USER_TYPE_LEADER: return "Руководитель";
            case self::USER_TYPE_CLIENT: return "Заказчик";
            default: return "Неизвестно";
        }
    }

}
