<?php

namespace app\models;

use app\services\Polygon;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "route_address".
 *
 * @property int $id ID
 * @property int $address_id адрес
 * @property int $fine_id Штраф
 * @property int $route_id Маршрут
 * @property int $fine_percent Процент штрафа
 * @property int $fine_comment Комментарий штрафа
 * @property string $plane_work План
 * @property string $fact_work Факт
 * @property integer $fact_n Факт Н
 * @property integer $fact_porch Факт Подъезд
 * @property integer $fact_store Факт Этаж
 * @property integer $fact_flat Факт Квартира
 * @property integer $fact_enter Факт Вахта
 * @property integer $confirm_n Подтвержденно Н
 * @property integer $confirm_porch Подтвержденно Подъезд
 * @property integer $confirm_store Подтвержденно Этаж
 * @property integer $confirm_flat Подтвержденно Квартира
 * @property integer $confirm_enter Подтвержденно Вахта
 * @property string $confirm_work Подтверждено
 * @property string $comment Комментарий
 * @property int $status_api Комментарий
 * @property string $fact_work_date Комментарий
 * @property int $floor Кол-во этажей (данные промоутера)
 * @property int $entrance Кол-во подъездов (данные промоутера)
 * @property int $apartment Кол-во квартир (данные промоутера)
 * @property string $entrances_with_porter Список подъездов с вахтами (данные промоутера)
 *
 * @property AddressList $address
 * @property Routes $route
 */
class RouteAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'route_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_id', 'fine_id', 'route_id', 'fine_percent', 'fine_comment', 'status_api',
                'fact_n', 'fact_porch', 'fact_store', 'fact_flat', 'fact_enter',
                'confirm_n', 'confirm_porch', 'confirm_store', 'confirm_flat', 'confirm_enter'], 'integer'],
            [['plane_date', 'comment', 'fact_work_date'], 'safe'],
            [['plane_work', 'fact_work', 'confirm_work', 'payment'], 'number'],
            [
                ['address_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => AddressList::className(),
                'targetAttribute' => ['address_id' => 'id']
            ],
            [
                ['route_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Routes::className(),
                'targetAttribute' => ['route_id' => 'id']
            ],
            [['employee_comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_id' => 'адрес',
            'fine_id' => 'Штраф',
            'route_id' => 'Маршрут',
            'fine_percent' => 'Процент штрафа',
            'fine_comment' => 'Комментарий штрафа',
            'plane_work' => 'План',
            'fact_work' => 'Факт',
            'fact_n' => 'Факт Н',
            'fact_porch' => 'Факт Подъезд',
            'fact_store' => 'Факт Этаж',
            'fact_flat' => 'Факт Квартира',
            'fact_enter' => 'Факт Вахта',
            'confirm_n' => 'Подтерженно Н',
            'confirm_porch' => 'Подтерженно Подъезд',
            'confirm_store' => 'Подтерженно Этаж',
            'confirm_flat' => 'Подтерженно Квартира',
            'confirm_enter' => 'Подтерженно Вахта',
            'payment' => 'Оплата',
            'confirm_work' => 'Подтверждено',
            'plane_date' => 'Планируемая дата размещения',
            'comment' => 'Комментарий',
            'employee_comment' => 'Комментарий сотрудника',
            'status_api' => 'Флаг записи через АПИ',
            'fact_work_date' => 'Дата внесения записи о фактически выполненном',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(AddressList::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    public function getPaymentN()
    {
        $payment = 0;
        $route = $this->route;
        $project = $route->project;

        $pricesProjectsIds = ArrayHelper::getColumn(PricesProjects::find()->where(['projects_id' => $project->id])->all(), 'prices_id');
        $prices = Prices::findAll($pricesProjectsIds);

        foreach ($prices as $price){
            if($price->count_for == Prices::COUNT_FOR_N){
                $payment += $price->price * $this->confirm_n;
            }
        }

        return $payment;
    }

    public function getPaymentPorch()
    {
        $payment = 0;
        $route = $this->route;
        $project = $route->project;

        $pricesProjectsIds = ArrayHelper::getColumn(PricesProjects::find()->where(['projects_id' => $project->id])->all(), 'prices_id');
        $prices = Prices::findAll($pricesProjectsIds);

        foreach ($prices as $price){
            if($price->count_for == Prices::COUNT_FOR_PORCH){
                $payment += $price->price * $this->confirm_porch;
            }
        }

        return $payment;
    }

    public function getPaymentStore()
    {
        $payment = 0;
        $route = $this->route;
        $project = $route->project;

        $pricesProjectsIds = ArrayHelper::getColumn(PricesProjects::find()->where(['projects_id' => $project->id])->all(), 'prices_id');
        $prices = Prices::findAll($pricesProjectsIds);

        foreach ($prices as $price){
            if($price->count_for == Prices::COUNT_FOR_STORE){
                $payment += $price->price * $this->confirm_store;
            }
        }

        return $payment;
    }

    public function getPaymentFlat()
    {
        $payment = 0;
        $route = $this->route;
        $project = $route->project;

        $pricesProjectsIds = ArrayHelper::getColumn(PricesProjects::find()->where(['projects_id' => $project->id])->all(), 'prices_id');
        $prices = Prices::findAll($pricesProjectsIds);

        foreach ($prices as $price){
            if($price->count_for == Prices::COUNT_FOR_FLAT){
                $payment += $price->price * $this->confirm_flat;
            }
        }

        return $payment;
    }

    /**
     * @return double
     */
    public function getPaymentValue()
    {
        $payment = 0;
        $route = $this->route;
        $project = $route->project;

        $pricesProjectsIds = ArrayHelper::getColumn(PricesProjects::find()->where(['projects_id' => $project->id])->all(), 'prices_id');
        $prices = Prices::findAll($pricesProjectsIds);

        foreach ($prices as $price){
            if($price->count_for == Prices::COUNT_FOR_N){
                $payment += $price->price * $this->confirm_n;
            }

            if($price->count_for == Prices::COUNT_FOR_PORCH){
                $payment += $price->price * $this->confirm_porch;
            }

            if($price->count_for == Prices::COUNT_FOR_STORE){
                $payment += $price->price * $this->confirm_store;
            }

            if($price->count_for == Prices::COUNT_FOR_FLAT){
                $payment += $price->price * $this->confirm_flat;
            }
        }

        return $payment;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        $route = Routes::findOne($this->route_id);
        $minDate = RouteAddress::find()->where(['route_id' => $this->route_id])->min('plane_date');
        $maxDate = RouteAddress::find()->where(['route_id' => $this->route_id])->max('plane_date');
        $countHouse = RouteAddress::find()->where(['route_id' => $this->route_id])->count('id');
        $address = RouteAddress::find()->where(['route_id' => $this->route_id])->all();
        $countApartament = 0;
        $countEntrance = 0;
        foreach ($address as $a) {
            $countApartament += ProjectAddress::find()->where(['address_id' => $a->address_id])->one()->apartament;
            $countEntrance += ProjectAddress::find()->where(['address_id' => $a->address_id])->one()->entrance;
        }
        $route->plane_start_date = $minDate;
        $route->plane_end_date = $maxDate;
        $route->count_adress = $countHouse;
        $route->count_apartament = $countApartament;
        $route->count_entrance = $countEntrance;
        $route->save();
    }

    /**
     * @param $route_id
     * @return array
     */
    public static function getCenterRouteAddress($route_id)
    {
        $route = Routes::findOne($route_id);
        $coords = Zones::find()->where(['id' => $route->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        return $polygon->getCenterZone();
    }

    public static function getRouteAddress($route_id)
    {
        $route = Routes::findOne($route_id);
        $routeAddres = ArrayHelper::map(RouteAddress::find()->where(['route_id' => $route_id])->asArray()->all(), 'id',
            'address_id');
        $coords = Zones::find()->where(['id' => $route->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        $address = $polygon->getObjects();
        foreach ($address as $key => $addr) {
            if (in_array($addr['id'], $routeAddres)) {
                $address[$key]['inroute'] = true;
            } else {
                $address[$key]['inroute'] = false;
            }
        }
        $center = $polygon->getCenterZone();
        return ['address' => $address, 'center' => $center];
    }

    /**
     * @param AddressList $model
     * @return string Возвращает строку адреса
     */
    public function getAddressString($model)
    {
        return $model->town . ', ' . $model->street . ', ' . $model->house;
    }

    /**
     * @param int $id ID записи в route_address
     * @param double $num Кол-во подтвержденных
     * @return array
     */
    public static function setConfirmWork($id, $num)
    {
        if (!$id || $num === null) return ['error' => 1, 'data' => 'Отсутствуют параметры'];

        $model = RouteAddress::findOne($id) ?? null;

        if (!$model) return ['error' => 1, 'data' => 'Параметры маршрута не найдены'];

        $model->confirm_work = $num;

        $price = Prices::find()->one();

        if($price != null){
            $model->payment = $price->price * $num;
        }

        if (!$model->save()){
            \Yii::error($model->errors, '_error');
            return ['error' => 1, 'data' => 'Ошибка сохранения данных'];
        }

        return ['error' => 0];
    }

    public static function setPayment($id, $num)
    {
        if (!$id || $num === null) return ['error' => 1, 'data' => 'Отсутствуют параметры'];

        $model = RouteAddress::findOne($id) ?? null;

        if (!$model) return ['error' => 1, 'data' => 'Параметры маршрута не найдены'];

        $model->payment = $num;
        if (!$model->save()){
            \Yii::error($model->errors, '_error');
            return ['error' => 1, 'data' => 'Ошибка сохранения данных'];
        }

        return ['error' => 0];
    }

    /**
     * Устанавливает тип штрафа
     * @param int $id ID записи в route_address
     * @param int $fine_id ID штрафа
     * @return array
     */
    public static function setFine($id, $fine_id = null)
    {
        \Yii::info('RA ID: ' . $id, 'test');
        \Yii::info('Fine ID: ' . $fine_id, 'test');
        if (!$id) return ['error' => 1, 'data' => 'Отсутствуют необходимые параметры'];

        $model = self::findOne($id) ?? null;

        if (!$model) return ['error' => 1, 'data' => 'Параметры не найдены'];

        $model->fine_id = $fine_id;

        \Yii::info($model->toArray(), 'test');

        if (!$model->save()){
            \Yii::error($model->errors, '_error');
            return ['error' => 1, 'data' => 'Ошибка сохранения штрафа'];
        }

        return ['error' => 0];
    }
}
