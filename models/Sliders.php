<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sliders".
 *
 * @property int $id
 * @property string $title Заголовок
 * @property resource $text Текст
 * @property string $fone Фон
 * @property int $order Сортировка
 * @property int $view Показ
 * @property int $view_time Время показа в секундах
 */
class Sliders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders';
    }

    public $poster23_file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','view_time'], 'required'],
            [['text'], 'string'],
            [['order', 'view', 'view_time'], 'integer'],
            [['title', 'fone'], 'string', 'max' => 255],
            [['poster23_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'fone' => 'Фон',
            'order' => 'Сортировка',
            'view' => 'Показать',
            'view_time' => 'Время показа в секундах',
            'poster23_file' => 'Фон',
        ];
    }


    public function getView()
    {
        if($this->view == 1) return 'Да';
        else return 'Нет';
    }
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $slider = Sliders::find()->orderBy(['order' => SORT_DESC ])->one();
            if($slider != null) $this->order = $slider->order + 1;
            else $this->order = 1;
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if ($this->fone != null && file_exists('uploads/sliders/' . $this->fone)) unlink(Yii::getAlias('uploads/sliders/' . $this->fone));
        return parent::beforeDelete();
    }

    public function getColumnsList()
    {
        $active = [];
        $sliders = Sliders::find()->orderBy(['order' => SORT_ASC])->all();
        $i = 0;
        foreach ($sliders as $slider) {
            $i++;
            $active += [
                $slider->id => ['content' => '<i class="glyphicon glyphicon-cog"></i> #' . $i . '&nbsp;' . $slider->title ],
            ];
        }

        return $active;
    }
}
