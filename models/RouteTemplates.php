<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "route_templates".
 *
 * @property int $id
 * @property string $name Имя шаблона
 * @property string $template
 *
 * @property Projects[] $projects
 */
class RouteTemplates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'route_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя шаблона',
            'template' => 'Текст шаблона',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['route_template_id' => 'id']);
    }

    public static function getTemplates()
    {
        return ArrayHelper::map(static::find()->all(),'id','name');
    }
}
