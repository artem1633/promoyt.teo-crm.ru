<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "routes_placement_method".
 *
 * @property int $routes_id
 * @property int $placement_method_id
 *
 * @property PlacementMethod $placementMethod
 * @property Routes $routes
 */
class RoutesPlacementMethod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'routes_placement_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['routes_id', 'placement_method_id'], 'required'],
            [['routes_id', 'placement_method_id'], 'integer'],
            [['routes_id', 'placement_method_id'], 'unique', 'targetAttribute' => ['routes_id', 'placement_method_id']],
            [['placement_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlacementMethod::class, 'targetAttribute' => ['placement_method_id' => 'id']],
            [['routes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::class, 'targetAttribute' => ['routes_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'routes_id' => 'Routes ID',
            'placement_method_id' => 'Placement Method ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacementMethod()
    {
        return $this->hasOne(PlacementMethod::class, ['id' => 'placement_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasOne(Routes::class, ['id' => 'routes_id']);
    }
}
