<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "employees_status".
 *
 * @property int $id
 * @property int $employees_id Сотрудник
 * @property int $status Статус
 * @property string $date_status Дата
 */
class EmployeesStatus extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employees_id', 'status'], 'integer'],
            [['date_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees_id' => 'Сотрудник',
            'status' => 'Статус',
            'date_status' => 'Дата',
        ];
    }

    /**
     * Возвращает список статусов промоутера
     * @param Employees $employee Модель промоутера
     * @param int $month Номер месяца
     * @return array
     */
    public static function getStatuses($employee, $month = 0)
    {
        $year = date('Y', time());

        $query = self::find()
            ->select('status, date_status')
            ->andWhere(['employees_id' => $employee->id])
            ->asArray();

        if ($month > 0) {
            $str_month = str_pad($month, 2, '0', STR_PAD_LEFT);
            /** @var int $number_day Количество дней в месяце */
            $number_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $start_date = $year . '-' . $str_month . '-01';
            $end_date = $year . '-' . $str_month . '-' . $number_day;

            $query->andWhere(['BETWEEN', 'date_status', $start_date, $end_date]);
        }

        $result = [];

        foreach ($query->each() as $status) {
            $status['status'] = Employees::getStatusName($status['status']);
            array_push($result, $status);
        }


        return $result;
    }

    /**
     * Сохраняет статус сотрудника
     * @param int $employee_id ID сотрудника
     * @param int $status_id ID Статуса
     * @param $date
     * @return string|null
     */
    public static function setStatus($employee_id, $status_id, $date)
    {
        $model = new EmployeesStatus();
        $model->employees_id = $employee_id;
        $model->status = $status_id;
        $model->date_status = $date;
        if (!$model->save()){
            Yii::error($model->errors, '_api_error');
            return 'internal error';
        }
        return null;
    }
}
