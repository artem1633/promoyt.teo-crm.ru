<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property int $id
 * @property string $name Назва
 * @property string $price Цена
 * @property string $price_type Тип цены
 */
class Prices extends \yii\db\ActiveRecord
{
    const TYPE_COUNT = 0;
    const TYPE_HOUR =1;

    const COUNT_FOR_HOUR = 0;
    const COUNT_FOR_N = 1;
    const COUNT_FOR_PORCH = 2;
    const COUNT_FOR_STORE = 3;
    const COUNT_FOR_FLAT = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count_for', 'name', 'price'], 'required'],
            [['price_type', 'count_for'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price' => 'Цена',
            'price_type' => 'Тип цены',
            'count_for' => 'Оплата за'
        ];
    }

    /**
     * @return array
     */
    public static function countForLabels()
    {
        return [
            self::COUNT_FOR_HOUR => 'За час',
            self::COUNT_FOR_N => 'Наружная реклама',
            self::COUNT_FOR_PORCH => 'За подъезд',
            self::COUNT_FOR_STORE => 'За этаж',
            self::COUNT_FOR_FLAT => 'За квартиру',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        $types = [];
        $types[self::TYPE_COUNT] = 'За шт';
        $types[self::TYPE_HOUR] = 'За час';
        return $types;

    }

    /**
     * @param $role
     * @return string
     */
    public function getTypeName($type)
    {
        switch ($type) {
            case self::TYPE_COUNT: return "За шт";
            case self::TYPE_HOUR: return "За час";
            default: return "Неизвестно";
        }
    }

    /**
     * @return string
     */
    public function getCurrentType()
    {
        switch ($this->price_type) {
            case self::TYPE_COUNT: return "За шт";
            case self::TYPE_HOUR: return "За час";
            default: return "Неизвестно";
        }
    }

    public static  function getPriceListFullName()
    {
        $prices = static::find()->all();
        $priceList = [];
        foreach ($prices as $price) {
            $priceList[$price->id] = $price->name. " : " .$price->price ."/".static::getTypeName($price->price_type);
        }
        return $priceList;
    }

    public static  function getPriceByID($id)
    {
        return  static::findOne($id)->price;
    }
}
