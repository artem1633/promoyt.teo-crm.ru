<?php

namespace app\models;


/**
 * This is the model class for table "routes_address_list".
 *
 * @property int $id
 * @property int $routes_id
 * @property int $address_list_id
 * @property string $completed_edition Подтвержденный тираж
 * @property string $comment Комментарий
 *
 * @property FinesRoutesAddressList[] $finesRoutesAddressLists
 * @property PlacementMethodRoutesAddressList[] $placementMethodRoutesAddressLists
 * @property PlacementMethod[] $placementMethods
 * @property Fines[] $fines
 * @property AddressList $addressList
 * @property Routes $routes
 */
class RoutesAddressList extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $fines;

    /**
     * @var array
     */
    public $placementMethods;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'routes_address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['routes_id', 'address_list_id'], 'required'],
            [['routes_id', 'address_list_id', 'completed_edition'], 'integer'],
            [['comment'], 'string'],
            [['address_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressList::class, 'targetAttribute' => ['address_list_id' => 'id']],
            [['routes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::class, 'targetAttribute' => ['routes_id' => 'id']],
            [['fines', 'placementMethods'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($this->fines != null){
            FinesRoutesAddressList::deleteAll(['routes_address_list_id' => $this->id]);

            foreach ($this->fines as $fine){
                (new FinesRoutesAddressList(['routes_address_list_id' => $this->id, 'fines_id' => $fine]))->save(false);
            }
        }

        if($this->placementMethods != null){
            PlacementMethodRoutesAddressList::deleteAll(['routes_address_list_id' => $this->id]);

            foreach ($this->placementMethods as $method){
                (new PlacementMethodRoutesAddressList(['routes_address_list_id' => $this->id, 'placement_method_id' => $method]))->save(false);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'routes_id' => 'Маршрут',
            'fines' => 'Штрафы',
            'placementMethods' => 'Типы работ',
            'address_list_id' => 'Адрес',
            'completed_edition' => 'Подтвержденный тираж',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinesRoutesAddressLists()
    {
        return $this->hasMany(FinesRoutesAddressList::class, ['routes_address_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacementMethodRoutesAddressLists()
    {
        return $this->hasMany(PlacementMethodRoutesAddressList::className(), ['routes_address_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getPlacementMethods()
    {
        return $this->hasMany(PlacementMethod::className(), ['id' => 'placement_method_id'])->viaTable('placement_method_routes_address_list', ['routes_address_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getFines()
    {
        return $this->hasMany(Fines::class, ['id' => 'fines_id'])->viaTable('fines_routes_address_list', ['routes_address_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressList()
    {
        return $this->hasOne(AddressList::class, ['id' => 'address_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasOne(Routes::class, ['id' => 'routes_id']);
    }
}
