<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_calculation".
 *
 * @property int $id
 * @property string $name
 * @property int $indicator Показатель
 */
class CategoryCalculation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_calculation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['indicator'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'indicator' => 'Показатель',
        ];
    }
}
