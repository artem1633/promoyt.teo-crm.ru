<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees_attach".
 *
 * @property int $id
 * @property int $employees_id
 * @property string $file Файл
 * @property int $category_file Категория файла
 *
 * @property Employees $emloyees
 */
class EmployeesAttach extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_attach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employees_id', 'category_file'], 'integer'],
            [['file'], 'string', 'max' => 255],
            [['employees_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employees_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees_id' => 'Emloyees ID',
            'file' => 'Файл',
            'category_file' => 'Категория файла',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmloyees()
    {
        return $this->hasOne(Employees::className(), ['id' => 'emlpoyees_id']);
    }
}
