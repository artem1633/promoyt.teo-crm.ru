<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employees_position".
 *
 * @property int $id
 * @property string $name Должность
 *
 * @property Employees[] $employees
 * @property Employees[] $employees0
 */
class EmployeesPosition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Должность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employees::className(), ['category_id' => 'id']);
    }

    static public function getPositionList()
    {
        return ArrayHelper::map(static::find()->asArray()->all(),'id','name');
    }
}
