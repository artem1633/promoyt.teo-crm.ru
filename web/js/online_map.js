var lang = "ru-RU";
var year = 2018;

function dateToTS (date) {
    return date.valueOf();
}

function tsToDate (ts) {
    var d = new Date(ts);

    return d.toLocaleTimeString(lang, {
        hour: 'numeric',
        minute: 'numeric',
    });
}


var myMap = null;
var myMap2 = null;
var array = null;
var myCollection = null;
var objectCollection = null;

var globalSteps = [];
var globalTimeCounter = 0;
var localTimeCounter = 0;

// var videoInstance = null;

var rangeSlider = null;

var videoStep = 1;

var videos = [];

var currentVideoDuration = 0;

function updateCurrentVideoPart()
{
    var stepsLength = globalSteps.length;
    var steps = globalTimeCounter / 30;

    videoDuration = Math.ceil(steps);

    if(videoDuration != currentVideoDuration){
        currentVideoDuration = videoDuration;

        console.log('current video duration', currentVideoDuration);

        videoInstance.src({type: 'video/mp4', src: '/'+videos[currentVideoDuration-1]});
        videoInstance.play();
    }
}

function getEmployeeRoute() {
    $.get("/online-map/get-employee-last-place",{type:0},function (data) {
        PlaceEmployees(data);
        console.log(data);
    })
}

function getEmployeeRouteFull(id, map, ymaps) {
    var date = $('#selectonlinemapform-filterdate').val();
    var client = $('#selectonlinemapform-filterclient').val();
    var project = $('#selectonlinemapform-filterproject').val();
    $.ajax({
        url: '/employees/view-main-ajax?id='+id,
        method: 'GET',
        success: function(response){
            console.log('view main response', response);
            $('#employeee-info-wrapper').html(response.content);
        }
    });
    $.get("/online-map/get-employ-routes",{'id': id, 'date': date, 'clientId': client, 'projectId': project},function (data) {
        console.log('routes and moves', data);
        var path = [];
        for(var i = 0; i < data.length; i++){
            var route = data[i];
            videos = route.videos;
            if(route.coords != []){
                path.push(route.coords);
            }
        }

        for(var i = 0; i < data.length; i++)
        {
            if(data[i].coords.length == 0){
                continue;
            }

            var polyline = new ymaps.Polyline(data[i].coords, {
                hintContent: data[i].name,
            }, {
                strokeColor: '#000000',
                strokeWidth: 4,
                // Первой цифрой задаем длину штриха. Второй цифрой задаем длину разрыва.
                strokeStyle: '1 0',

            });
            // Добавляем линию на карту.
            map.geoObjects.add(polyline);
            // Устанавливаем карте границы линии.
            map.setBounds(polyline.geometry.getBounds());
        }

        globalSteps = [];

        for (var i = 0; i < data.length; i++)
        {
            var route = data[i];
            for(var j = 0; j < route.timesteps.length; j++)
            {
                globalSteps.push(route.timesteps[j]);
            }
        }
        // for (var i = 0; i < data.length; i++)
        // {
        //     var route = data[i];
        //     for(var j = 0; j < route.timesteps.length; j++)
        //     {
        //         globalSteps.push(route.timesteps[j]);
        //     }
        // }

        myMap2.setZoom(12);

        rangeSlider.update({
            skin: "big",

            grid: true,
            min: 1,
            max: globalSteps.length,
            from: 1,
            onChange: function(scope){
                globalTimeCounter = scope.input.val()-1;
                updateCurrentVideoPart();
                videoInstance.currentTime(scope.input.val()*10);
            },
            // min: dateToTS(new Date(year, 10, 1,0,0)),
            // max: dateToTS(new Date(year, 10, 1,23,59)),
            // from: dateToTS(new Date(year, 10, 1,8,0)),
            // prettify: tsToDate
        });

        var timeMark = new ymaps.Placemark([0, 0], {}, {
            preset: 'islands#circleDotIcon',
            iconLayout: 'default#image',
            iconImageHref: '/images/yandex/promoyt2.png',
            iconImageSize: [30, 42],
            iconColor: '#0095b6',
        });
        map.geoObjects.add(timeMark);

        $.get('/main-report/ajax-view?id='+data[0].id+'&runViewJs=0', function(response){
            $('.video-wrapper').html(response);
            if(typeof videoInstance !== 'undefined'){
                videoInstance.dispose();
            }
            videoInstance = videojs('#videojs-w0', {
                width: 200,
                height: 300,
                controls: true,
            });
            videoInstance.play();
            currentVideoDuration = videoInstance.duration();
            console.log('video duration', videoInstance.duration());
        });

        setInterval(function(){
            if(globalTimeCounter < globalSteps.length){
                timeMark.geometry.setCoordinates([globalSteps[globalTimeCounter][1], globalSteps[globalTimeCounter][2]]);
                rangeSlider.update({ from: globalTimeCounter+1 });
                globalTimeCounter++;
                console.log('global time counter is', globalTimeCounter);
            }
            if(videoInstance.ended()){
                // localTimeCounter = 0;
                // rangeSlider.update({ from: localTimeCounter+1 });
                updateCurrentVideoPart();
            } else {
                console.log('current video time', videoInstance.currentTime());
            }
        }, 10000);
    });

}

function getEmployees(map, ymaps) {
    $.get("/online-map/get-employees",function (data) {
        map.geoObjects.each(function(context) {
            map.geoObjects.remove(context);
        });
        console.log('employees', data);
        for(var i = 0; i < data.length; i++)
        {
            var employ = data[i];
            console.log(employ);
            var coords = [employ.coord_x, employ.coord_y];

            map.geoObjects.add(new ymaps.Placemark(coords,{
                balloonContent: null,
                iconCaption: employ.firstname+' '+employ.lastname+' (id: '+employ.id+')',
            }));
        }
    })
}

function PlaceEmployees(arr) {

    for (step = 0; step < arr.length; step++){
        txtycoor = [arr[step]['coord_x'],arr[step]['coord_y']];
        var myPlacemark = new ymaps.Placemark(txtycoor,{
            hintContent: arr[step]['employee_id'],
        }, {
            preset: 'islands#circleDotIcon',
            iconLayout: 'default#image',

            iconImageHref: '/images/yandex/promoyt'+arr[step]['status']+'.png',

            iconImageSize: [30, 42],
            iconColor: '#0095b6',

        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
    }

};

var routesLoaded = false;

$('[data-role="map-filter"]').click(function(e){
    e.preventDefault();

    myMap2.geoObjects.removeAll();

    routesLoaded = false;

    $.get("/online-map/get-employees",function (data) {
        // map.geoObjects.each(function(context) {
        //     map.geoObjects.remove(context);
        // });
        console.log('employees', data);
        for(var i = 0; i < data.length; i++)
        {
            var employ = data[i];
            var employId = $('#selectonlinemapform-filteremployee').val();
            if(employId != null && employId != employ.id){
                console.log('filtered');
                continue;
            }
            if(routesLoaded == false){
                getEmployeeRouteFull(employ.id, myMap2, ymaps);
                routesLoaded = 1;
            }
        }
    });
});

function init () {
    $("#example_id").ionRangeSlider({
        skin: "big",

        grid: true,
        min: 1,
        max: 1,
        from: 1,
        onChange: function(scope){
            localTimeCounter = scope.input.val()-1;
        },
        // min: dateToTS(new Date(year, 10, 1,0,0)),
        // max: dateToTS(new Date(year, 10, 1,23,59)),
        // from: dateToTS(new Date(year, 10, 1,8,0)),
        // prettify: tsToDate
    });
    rangeSlider = $('#example_id').data('ionRangeSlider');
    //myCollection = new ymaps.GeoObjectCollection();
    myCollection = new ymaps.GeoObjectCollection();
    objectCollection = new ymaps.GeoObjectCollection();
    // Создаем карту с добавленными на нее кнопками.
    if (myMap) myMap.destroy();
    myMap = new ymaps.Map('map', {
        center: center_map,
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });
    myMap2 = new ymaps.Map('map2', {
        center: center_map,
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });
    getEmployeeRoute();
    getEmployees(myMap, ymaps);
    setInterval(function(){
        getEmployees(myMap, ymaps);
    }, 5000);


    $.get("/online-map/get-employees",function (data) {
        // map.geoObjects.each(function(context) {
        //     map.geoObjects.remove(context);
        // });
        console.log('employees', data);
        for(var i = 0; i < data.length; i++)
        {
            var employ = data[i];
            var employId = $('#selectonlinemapform-filteremployee').val();
            if(employId != null && employId != employ.id){
                console.log('filtered');
                continue;
            }
            if(routesLoaded == false){
                getEmployeeRouteFull(employ.id, myMap2, ymaps);
                routesLoaded = 1;
            }
        }
    });


    // var verticles = array.map(function (point) {
    //     return [point.coord_x,point.coord_y]}),
    // line = new ymaps.Polyline(verticles);
    // myMap.geoObjects.add(line);
    // array.forEach(function(item, i, array) {
    //     myMap.geoObjects
    //         .add(new ymaps.Placemark([item.coord_x, item.coord_y], {
    //             balloonContent: item.id,
    //             iconContent: item.id,
    //             hintContent: item.id
    //         }, {
    //             preset: 'islands#circleDotIcon',
    //             iconColor: '#0095b6',
    //             iconImageSize: [2, 2],
    //         }));
    // });
}
ymaps.ready(init);
