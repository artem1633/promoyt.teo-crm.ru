var lang = "ru-RU";
var currPlace = null;
var myMap = null;
var routeData = null;
var stopPlay = false;
var currPlay = 0;
var my_range = null;

function dateToTS(date) {
    return date.valueOf();
}

function tsToDate(ts) {
    var d = new Date(ts);

    return d.toLocaleDateString(lang, {
        hour: 'numeric',
        minute: 'numeric',
    });
}


function getEmployeeRoute() {
    let route = $("#routeperdateform-route").val();
    $.get("/employees/get-move-route", {route: route, employeeId: employeeId}, function (data) {
        console.log(data);
        routeData = data.movies;
        drawRoute(data.address, data.center);
    })
}

/**
 * Получить маршрути за период
 */

function selectRouteList() {
    let startDate = $('#routeperdateform-start_date').val();
    let endDate = $('#routeperdateform-end_date').val();
    $.get("/employees/get-routes-from-period", {
        employee: employeeId,
        startDate: startDate,
        endDate: endDate
    }, function (data) {
        var sel = $("#routeperdateform-route");
        sel.empty();
        for (var i = 0; i < data.length; i++) {
            sel.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
        }
    })
}

function placeEmloyee(index, active) {
    myMap.geoObjects.remove(currPlace);
    currPlace = new ymaps.Placemark([routeData[index].coord_x, routeData[index].coord_y], {
        balloonContent: routeData[index].dateandtime,
        iconContent: routeData[index].dateandtime,
        hintContent: routeData[index].dateandtime
    }, {
        preset: 'islands#circleDotIcon',
        iconLayout: 'default#image',
        iconImageHref: '/images/yandex/promoyt' + active + '.png',
        iconImageSize: [30, 42],
        iconColor: '#0095b6',
    });
    myMap.geoObjects.add(currPlace);
}

function stop() {
    stopPlay = true;
}

function play() {
    stopPlay = false;
    $("#play").attr("src", "/images/yandex/stop.png");
    $("#play").attr("onClick", "stop()");
    let i = findPlaceEmploee(currPlay);
    (function loops() {
        setTimeout(function () {
            placeEmloyee(i, 2);
            currPlay = new Date(routeData[i].dateandtime);
            my_range.update({
                from: currPlay.valueOf(),
            });
            if (i < routeData.length - 1 && !stopPlay) {
                i++;
                loops();
            } else {
                $("#play").attr("src", "/images/yandex/start.png");
                $("#play").attr("onClick", "play()");
                placeEmloyee(i, 0);
            }
        }, 1000);
    })();
}

function findPlaceEmploee(dateandtime) {
    let i = routeData.findIndex(function (element, index, routeData) {
        d = new Date(element.dateandtime);
        return d.valueOf() > dateandtime;
    });
    if (i == -1) {
        i = routeData.length - 1
    }
    return i;
}

function drawRoute(address, center) {
    myCollection.removeAll();
    if (myMap) myMap.destroy();
    myMap = new ymaps.Map('map', {
        center: [center.centerX, center.centerY],
        zoom: 15,
    });
    let verticles = routeData.map(function (point) {
        return [point.coord_x, point.coord_y]
    });
    let line = new ymaps.Polyline(verticles);
    myMap.geoObjects.add(line);
    address.forEach(function (item, i, routeData) {
        myMap.geoObjects
            .add(new ymaps.Placemark([item.coord_x, item.coord_y], {
                hintContent: item.street + ", " + item.house + ", " + item.housing,
                balloonContent: item.street + ", " + item.house + ", " + item.housing,
                iconContent: item.street + ", " + item.house + ", " + item.housing,
            }, {
                preset: 'islands#circleDotIcon',
                iconColor: '#0095b6',
                iconImageSize: [2, 2],
            }));
    });
    placeEmloyee(0, 0);
    my_range.update({
        min: dateToTS(new Date(routeData[0].dateandtime)),
        max: dateToTS(new Date(routeData[routeData.length - 1].dateandtime)),
        from: dateToTS(new Date(routeData[0].dateandtime)),
    });
    currPlay = 0;
    $("#playPanel").show();
}

function init() {
    $("#example_id").ionRangeSlider({
        onFinish: function (data) {
            currPlay = data.from;
            placeEmloyee(findPlaceEmploee(currPlay), 0);
        },
        skin: "big",
        grid: true,
        prettify: tsToDate
    });
    my_range = $("#example_id").data("ionRangeSlider");
    myCollection = new ymaps.GeoObjectCollection();
    if (myMap) myMap.destroy();
    myMap = new ymaps.Map('map', {
        center: center_map,
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });
}

ymaps.ready(init);