var myMap = null;
var array = null;
var newStrokeColor = '#feff18';
var newStrokeWidth = 3;
var changedStrokeColor = '#ff2ab0';
var changedStrokeWidth = 3;
var savedStrokeColor = '#2c38ff';
var savedStrokeWidth = 3;
var objectCollection = null;

function GoToCenter(xx, yy, zoom) {
    myMap.setCenter([xx, yy], zoom);
    RePaintObjects();
};

function AreaLineAdd(){
    arealine_add([[]],'new',true,'Новая зона',0,0,'Объект не сохранен');
};

function arealine_add(txtycoor, status, modeadd, namezone, idzone, numzone, countobject,countentrance,countfloor,countapartament) {
    if ((status) == 'new' ) {
        strokeColor = newStrokeColor;
        strokeWidth = newStrokeWidth;
    }
    if ((status) == 'change' ) {
        strokeColor = changedStrokeColor;
        strokeWidth = changedStrokeWidth;
    }
    if ((status) == 'saved' ) {
        strokeColor = savedStrokeColor;
        strokeWidth = savedStrokeWidth;
    }
    var myPolyline = new ymaps.Polygon(txtycoor, {
        hintContent: "<b>Название: </b>"+namezone+"<br><b>ID: </b>"+idzone+"</b>"+"<br><b>К-во объектов: </b>"+countobject+"</b>"+
        "<br><b>К-во подъездов: </b>"+countentrance+"</b>"+"<br><b>К-во етажей: </b>"+countfloor+"</b>"+"<br><b>К-во квартир: </b>"+countapartament+"</b>",
        id_zone: idzone,
        name_zone: namezone,
        num_zone: numzone,
        status_zone : status,
    }, {
        strokeColor: strokeColor,
        strokeWidth: strokeWidth,
        editorMaxPoints: 50,
        editorMenuManager: function (items) {
            items.push({
                title: "Удалить линию",
                onClick: function () {
                    myCollection.remove(cured);
                    myMap.geoObjects.remove(cured);
                }
            });
            return items;
        },
        draggable: true,
        tfig: "Area",
    });
    myPolyline.events.add('click', function (e) {
        if (cured != 'null') {
            cured.editor.stopEditing();
            cured.options.set({
                fillColor: '#1d74cf40'
            });
        }
        e.get('target').editor.startEditing();
        cured = e.get('target');
        e.get('target').options.set({
            fillColor: '#2fff2150'
        });

    });
    myPolyline.events.add('geometrychange', function (e) {

        if (e.get('target').properties.get('status_zone') != 'new'){
            namezone = e.get('target').properties.get('name_zone');
            idzone = e.get('target').properties.get('id_zone');
            status = 'changed'
            e.get('target').properties.set({
                status_zone : status,
                hintContent: "<b>Название: </b>"+namezone+"<br><b>ID: </b>"+idzone+"</b>"+"<br><b>К-во объектов: </b>Объект не сохранен</b>",
            });
            e.get('target').options.set({
                strokeColor : changedStrokeColor,
                strokeWidth : changedStrokeWidth,
            });
        }


    });
    myCollection.add(myPolyline);
    myMap.geoObjects.add(myCollection);
    if (modeadd == true) {
        if (cured != 'null') {
            cured.editor.stopEditing();
            cured.options.set({
                fillColor: '#1d74cf40'
            });
        }
        myPolyline.options.set({
            fillColor: '#2fff2150'
        });
        myPolyline.editor.startDrawing();
        cured = myPolyline;


    };
};

function RePaintObjects() {
    myMap.geoObjects.each(function (ob) {
        myMap.geoObjects.remove(ob);
    });
    myCollection.removeAll();
    $.get('/projects/load-zone?project_id='+project_id,
        {billing_id: 0},
        function (e) {
            PlaceObj(e);
        });
};

function PlaceObj(arr) {
    i = 0;
    for (i in arr) {
        txtycoor = arr[i]["coords"];
        namezone = arr[i]["name_zone"];
        idzone = arr[i]["id_zone"];
        numzone = arr[i]["num_zone"];
        count_objects = arr[i]["count_objects"];
        count_entrance = arr[i]["count_entrance"];
        count_floor = arr[i]["count_floor"];
        count_apartament = arr[i]["count_apartament"];
        arealine_add(txtycoor, 'saved', false, namezone, idzone, numzone,count_objects,count_entrance,count_floor,count_apartament);
    }
    ;
};

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function PlaceAddressList() {
    if (cured=="null") {
        alert("Не выбрана зона")
    } else {
        objectCollection.each(function (ob) {
            myMap.geoObjects.remove(ob);
        });
        objectCollection.removeAll();
        var projectId = getUrlParameter('id');
        cr=cured.geometry.getCoordinates(),
            $.post( "/projects/load-address?project_id="+projectId,
                {param: JSON.stringify({coords:cr})},
                function (e) {
                    PlaceAddress(e);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("error " + textStatus);
                console.log("incoming Text " + jqXHR.responseText);
            });
    }
};

function PlaceAddress(arr) {

    for (step = 0; step < arr.coord.length; step++){
        txtycoor = [arr.coord[step]['coord_x'],arr.coord[step]['coord_y']];
        var myPlacemark = new ymaps.Placemark(txtycoor,{
            hintContent: arr.coord[step]['street'] + ", "+ arr.coord[step]['house'] + ", "+ arr.coord[step]['housing'],
        }, {
            preset: 'islands#dotIcon',
            iconColor: '#0095b6',
            iconImageSize: [2, 2],
        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
        console.log(myPlacemark);
    }

};

function PlaceAddressNoZone() {
    objectCollection.each(function (ob) {
        myMap.geoObjects.remove(ob);
    });
    objectCollection.removeAll();
    $.post( "/projects/load-address-no-zone?project_id="+project_id,
        {},
        function (e) {
            console.log(e);
            PlaceAddress(e);
        }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("error " + textStatus);
        console.log("incoming Text " + jqXHR.responseText);
    });
}



function init() {

    myCollection = new ymaps.GeoObjectCollection();
    objectCollection = new ymaps.GeoObjectCollection();
    myMap = new ymaps.Map('map', {
        center: center_map,
        zoom: 12,
        controls: ['smallMapDefaultSet']
    });
    RePaintObjects();
};

function savePolygon() {
    var zone_id = "",
        id_zone = "",
        cr=cured.geometry.getCoordinates(),
        num_zone = cured.properties.get("num_zone"),
        name_zone = cured.properties.get("name_zone");
    if (typeof cured.properties.get("id_zone") != "undefined") {
        id_zone = cured.properties.get('id_zone');
        zone_id = "&zone_id="+id_zone;
    }
    $.post( "/projects/save-zone?project_id="+project_id+zone_id,
        {param: JSON.stringify({num_zone:num_zone,name_zone:name_zone,id_zone:id_zone,coords:cr})},
        function(response) {

            console.log(JSON.stringify({num_zone:num_zone,name_zone:name_zone,id_zone:id_zone,coords:cr}));
            namezone = cured.properties.get('name_zone');
            idzone = cured.properties.get('id_zone');
            status = 'saved';

            cured.options.set({
                strokeColor : savedStrokeColor,
                strokeWidth : savedStrokeWidth,
            });
            cured.properties.set({
                status_zone : status,
                hintContent: "<b>Название: </b>"+namezone+"<br><b>ID: </b>"+idzone+"</b>"+"<br><b>К-во объектов: </b>Объект не сохранен</b>",
            });

            $.pjax.reload({container: "#crud-zones-pjax"}).done(function() {
                $.pjax.reload({container: "#crud-address-pjax"});
            });


        }
    );
}

function sendAjaxForm() {

    cured.properties.set({
        num_zone : $("#add-zones-num_zone").val(),
        name_zone : $("#add-zones-name").val(),
    });
    savePolygon();

    $("#progectAddZoneModal").modal("hide");
};

$("#zoneEdit").on("click",function(){
    if (cured=="null") {
        alert("Не выбрана зона")
    } else {
        if (cured.properties.get("id_zone") == 0) {
            $("#progectAddZoneModal").modal("show");
        } else {
            savePolygon();
        }
    }
});

$('#crud-zones-pjax').on("pjax:success", function(event){
        RePaintObjects();
    }
);

ymaps.ready(init);