var myMap = null;
var array = null;
var objectCollection = null;

function PlaceAddressList() {
        objectCollection.each(function (ob) {
            myMap.geoObjects.remove(ob);
        });
        objectCollection.removeAll();
        $.post( "/projects/load-route-address?route_id="+route_id,
            function (e) {
                PlaceAddress(e);
            }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log("error " + textStatus);
            console.log("incoming Text " + jqXHR.responseText);
        });
};

function PlaceAddress(arr) {
    console.log(arr);
    for (step = 0; step < arr.length; step++){
        txtycoor = [arr[step]['coord_x'],arr[step]['coord_y']];


        if (arr[step]['inroute']) {
            iconColor = '#2c38ff';
        } else {
            iconColor = '#ff0a19';
        }
        var myPlacemark = new ymaps.Placemark(txtycoor,{
            address_id : arr[step]['id'],
            iconContent : arr[step]['street'] + ", "+ arr[step]['house'] + ", "+ arr[step]['housing'],
        }, {
            iconColor : iconColor,
            preset: 'islands#dotIcon',
            iconImageSize: [2, 2],
        });
        myPlacemark.events.add('click', function (e) {
            if (e.get('target').options.get('iconColor') == '#2c38ff') {
                e.get('target').options.set({'iconColor' : '#ff0a19'});
            } else {
                e.get('target').options.set({'iconColor' : '#2c38ff'});
            };
            $.post( "/projects/set-route-address?route_id="+route_id,{
                id : e.get('target').properties.get('address_id')
            }, function (e) {
                $.pjax.reload({container: "#crud-datatable-pjax"});
                console.log(e);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("error " + textStatus);
                console.log("incoming Text " + jqXHR.responseText);
            });
        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
    }

};

function init() {
    myCollection = new ymaps.GeoObjectCollection();
    objectCollection = new ymaps.GeoObjectCollection();
    myMap = new ymaps.Map('map', {
        center: center_map,
        zoom: 16,
        controls: ['smallMapDefaultSet']
    });
    PlaceAddressList();
};

ymaps.ready(init);